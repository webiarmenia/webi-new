export const environment = {
  production: false,
  apiUrl: 'http://webi.webi.am/api/',
  imageUrl: 'http://webi.webi.am/uploads/',
  clientUrl: 'http://webi.webi.am/client/'
};
