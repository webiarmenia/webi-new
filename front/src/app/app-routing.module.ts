import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./components/pages/home/home.module').then(m => m.HomeModule)
    },
    {
        path: 'services',
        loadChildren: () => import('./components/pages/services//services.module').then(m => m.ServicesModule)
    },
    {
        path: 'portfolio',
        loadChildren: () => import('./components/pages/portfolio//portfolio.module').then(m => m.PortfolioModule)
    },
    {
        path: 'ecommerce',
        loadChildren: () => import('./components/pages/ecomerce//ecomerce.module').then(m => m.EcomerceModule)
    },
    {
        path: 'cabinet',
        loadChildren: () => import('./client//client.module').then(m => m.ClientModule)
    }
];


@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forRoot(routes)],
})


export class AppRoutingModule {
}
