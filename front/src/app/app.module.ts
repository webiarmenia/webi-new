import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule, HttpClient, HTTP_INTERCEPTORS} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';

import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


import {SharedComponentsModule, SharedModule} from './components/_modules';
import {AppRoutingModule} from './app-routing.module';

import {Globals} from './app.globals';

import {AppComponent} from './app.component';
import {ContactComponent} from './components/pages/contact/contact.component';
import {AboutComponent} from './components/pages/about/about.component';
import {MessageComponent} from './components/partials/message/message.component';
import {ScrollSpyDirective} from './components/_directives/scroll-spy.directive';
import {AllTeamComponent} from './components/pages/home/team/all-team/all-team.component';
import {BlogComponent} from './components/pages/blog/blog.component';
import {BlogDetailesComponent} from './components/pages/blog/blog-detailes/blog-detailes.component';
import {InterceptorService} from './client/_services/interceptor.service';
import {AlertComponent, LoadingComponent} from './components/partials/alert&loading';
import {AlertService} from './components/_services/alert.service';
// import { FaqComponent } from './components/partials/faq/faq.component';


export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}

@NgModule({
    declarations: [
        AppComponent,
        AlertComponent,
        LoadingComponent,
        ContactComponent,
        AboutComponent,
        MessageComponent,
        ScrollSpyDirective,
        AllTeamComponent,
        BlogComponent,
        BlogDetailesComponent,
        // FaqComponent,
    ],
    imports: [
        SharedModule,
        SharedComponentsModule,
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        NgbModule,
        HttpClientModule,
        RouterModule.forRoot([]),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
    ],
    providers: [Globals,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: InterceptorService,
            multi: true
        },
        AlertService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
