import {Component, OnDestroy, OnInit} from '@angular/core';
import {MenuService} from '../../_services/menu.service';
import {Menu} from '../../_models/menu';
import {Subscription} from 'rxjs';
import {ScrollService} from '../../_services/scroll.service';


@Component({
    selector: 'app-top-menu',
    templateUrl: './top-menu.component.html',
    styleUrls: ['./top-menu.component.scss']
})
export class TopMenuComponent implements OnInit, OnDestroy {
    menuList: Menu[];
    done = false;
    hed;
    parent;
    colorsSubscribe: Subscription;
    changeColor = false;
    url = 'assets/images/Homepage/White/logo3x.png';
    urlButton = 'assets/images/menuToggle.png';
    check = false;

    constructor(private menuService: MenuService, private scrollService: ScrollService) {
        this.colorsSubscribe = this.scrollService.getColors()
            .subscribe(color => {
                this.changeColor = color;
                if (this.changeColor) {
                    this.url = 'assets/images/Homepage/Violet/logo3x.png';
                    this.urlButton = 'assets/images/blueMenuToggle.png';
                } else if (!this.changeColor) {
                    this.url = 'assets/images/Homepage/White/logo3x.png';
                    this.urlButton = 'assets/images/menuToggle.png';
                }
            });
    }

    ngOnInit() {
        this.hed = document.getElementById('headerScroll');
        this.parent = document.getElementById('parentDiv');
        this.getMenuList();
        this.changeMenuHeight();
        this.url = 'assets/images/Homepage/White/logo3x.png';
    }

    ngOnDestroy() {
        this.colorsSubscribe.unsubscribe();
    }


    openCollapse(e: string | object) {
        this.check = !this.check;
        if (typeof (e) === 'string') {
            scrollTo(0, document.getElementById(e).offsetTop);
        }
        if (this.check) {
            setTimeout(() => {
                this.url = 'assets/images/Homepage/Violet/logo3x.png';
                this.urlButton = 'assets/images/blueMenuToggle.png';
                document.getElementById('sidebar-icons-id').style.display = 'block';
                document.getElementById('html').style.overflowY = 'hidden';
            }, 300);
        } else if (!this.changeColor && !this.check) {
            this.url = 'assets/images/Homepage/White/logo3x.png';
            this.urlButton = 'assets/images/menuToggle.png';
            document.getElementById('sidebar-icons-id').style.display = 'none';
            document.getElementById('html').style.overflowY = 'auto';
        } else if (this.changeColor && !this.check) {
            this.url = 'assets/images/Homepage/Violet/logo3x.png';
            this.urlButton = 'assets/images/blueMenuToggle.png';
            document.getElementById('sidebar-icons-id').style.display = 'none';
            document.getElementById('html').style.overflowY = 'auto';
        }
    }

    getMenuList() {
        this.menuService.getAll().subscribe(
            data => {
                this.menuList = data;
                this.done = true;
            },
            err => console.log(err)
        );
    }

    changeMenuHeight() {
        if (this.parent) {
            this.parent.onscroll = e => {
                const scrollTop = e.target.scrollTop;
                if (scrollTop > 50) {
                    this.hed.classList.add('scroll-change-height');
                } else {
                    this.hed.classList.remove('scroll-change-height');
                }
            };
        }
    }

}
