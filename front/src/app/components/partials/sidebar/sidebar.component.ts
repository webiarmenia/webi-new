import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import {ScrollService} from '../../_services/scroll.service';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnDestroy {
    colorSubscribe: Subscription;
    changeColor = false;

    @Input() currentSection;
    @Input() disable;

    constructor(private scrollService: ScrollService) {
        this.colorSubscribe = this.scrollService.getColors().subscribe(color => this.changeColor = color);
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.colorSubscribe.unsubscribe();
    }

    scrollTo(section) {
        document.querySelector('#' + section)
            .scrollIntoView();
        this.currentSection = section;

        // console.log(this.currentSection);
    }

}
