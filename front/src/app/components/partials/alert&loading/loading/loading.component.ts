import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import {AlertService} from '../../../_services/alert.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {
  display = 'none';
  subscription: Subscription;

  constructor(private alertService: AlertService) {
    this.subscription = this.alertService.get_alert_loading().subscribe(display => { this.display = display; });
  }

  ngOnInit() {
  }

}
