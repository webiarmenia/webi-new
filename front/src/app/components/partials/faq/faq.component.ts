import {Component, OnInit} from '@angular/core';
import {ServicesService} from '../../_services/services.service';
import {ServerResponse} from '../../_models/Service/ServerResponse';
import {ServerResponseFAQ} from '../../_models/FAQ/ServerResponseFAQ';

@Component({
    selector: 'app-faq',
    templateUrl: './faq.component.html',
    styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {

    constructor(
        private servicesService: ServicesService,
    ) {
    }

    ngOnInit() {
        this.servicesService.getAllPages('faq')
            .subscribe(
                (pages: ServerResponseFAQ) => {
                    console.log(pages);
                }
            );
    }

}
