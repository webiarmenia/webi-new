import {AfterViewInit, Component, OnInit} from '@angular/core';
import {FormGroup, Validators, FormBuilder} from '@angular/forms';
import {ContactService} from '../../_services/contact.service';
import {SettingService} from '../../_services/setting.service';
import {AlertService} from '../../_services/alert.service';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, AfterViewInit {
    myForm: FormGroup;
    data;
    phone;
    Email;
    address;
    emailPattern = '^[a-z0-9._%+-]{5,15}@[a-z0-9.-]+\.[a-z]{2,4}$';
    emailForm: FormGroup;
    submitted = false;
    locationsQuery: any;

    location = {
        address: 'CANADA: Abbas Khatamian+1',
        phone: '(438) 580 8558'
    };
    locationSecond = {
        address: 'CANADA: Abbas Khatamian+1',
        phone: '(438) 580 8558'
    };
    locations = [
        {
            Canada: [
                {
                    address: 'CANADA: Abbas Khatamian+1',
                    phone: '(438) 580 8558'
                }
            ],
            Brazil: [
                {
                    address: 'Brazil: Rua Luiza Porcari Corassa 383',
                    phone: '(11) 5086-2869'
                }
            ],
            Russia: [
                {
                    address: 'Russia: Arbatskaya street house 71',
                    phone: '(499) 7777-568645'
                }
            ],
            India: [
                {
                    address: 'India: 73 /, Lehari Bldg, th Khetwadi Road, Khetwadi',
                    phone: '02223824661'
                }
            ],
        }
    ];

    constructor(
        private formBuilder: FormBuilder,
        private contact: ContactService,
        private settingsService: SettingService,
        private alertService: AlertService
    ) {
        this.myForm = formBuilder.group({
            firstName: ['', [Validators.required]],
            email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
            message: ['', [Validators.required, Validators.minLength(10)]],
        });
        this.emailForm = formBuilder.group({
            email: ['', [Validators.required, Validators.pattern(this.emailPattern)]]
        });
    }

    get f() { return this.emailForm.controls; }

    ngOnInit() {
        this.phone = this.settingsService.getValueByKeyLanguage('footer-phone', 'en').split('///');
        this.Email = this.settingsService.getValueByKeyLanguage('footer-email', 'en').split('///');
        this.address = this.settingsService.getValueByKeyLanguage('footer-address', 'en').split('///');
    }
    ngAfterViewInit() {
        this.locationsQuery = Array.from(document.getElementsByClassName('location-wrapper'));
    }
    enter(location: string) {
        if (location === 'Canada') {
            this.location = this.locations[0].Canada[0];
        } else if (location === 'Brazil') {
            this.location = this.locations[0].Brazil[0];
        } else if (location === 'Russia') {
            this.location = this.locations[0].Russia[0];
        } else if (location === 'India') {
            this.location = this.locations[0].India[0];
        }
    }
    leave() {
        this.location = this.locationSecond;
    }
    changeLocation(e) {
        if (e.classList.contains('not-active')) {
            this.locationsQuery.forEach( el => {
                el.classList.remove('active');
                el.classList.add('not-active');
            } );
            e.classList.remove('not-active');
            e.classList.add('active');
        }
        this.locationSecond = this.location;
    }

    onSubmit() {
        this.submitted = true;
        this.alertService.alert_loading('show');
        if (!this.emailForm.valid) {
            return false;
        }
        this.contact.subscribeEmail(this.emailForm.value);
        this.emailForm.reset();
    }

    submit() {
        this.contact.sendEmail(this.myForm.value).subscribe(
            data => {
                if (data.success) {
                    this.resetForm();
                }
            },
            e => console.log(e)
        );
    }

    resetForm() {
        this.myForm.reset();
    }
}
