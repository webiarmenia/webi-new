import {Details} from './details';

export class Portfolio {
    id: string;
    url: string;
    slug: string;
    sliderPosition: string;
    title: {
        en: string,
        ru: string,
        am: string
    };
    // description: {
    //     en: string,
    //     ru: string,
    //     am: string
    // };
    shortDescription: {
        en: string,
        ru: string,
        am: string
    };
    hover: {
        en: string,
        ru: string,
        am: string
    };
    image: string;
    details: Details[];
}
