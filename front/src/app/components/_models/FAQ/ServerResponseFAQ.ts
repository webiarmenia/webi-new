import {FAQ} from './FAQ';

export interface ServerResponseFAQ {
    success: boolean;
    faqs: Array<FAQ>;
}
