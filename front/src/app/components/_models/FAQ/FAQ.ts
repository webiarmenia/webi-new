import {Talk} from './Talk';

export interface FAQ {
    _id: string;
    shortDescription: {
        am: string,
        ru: string,
        en: string
    };
    talk: Array<Talk> | [];
}
