export interface Talk {
    amAnswer: string;
    ruAnswer: string;
    enAnswer: string;
    amQuestion: string;
    ruQuestion: string;
    enQuestion: string;
    order: number;
    show: boolean;
}
