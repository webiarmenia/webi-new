export class Details {
    type: string;
    imgURL: string;
    imgURL1: string;
    imgURL2: string;
    imagePosition: string;
    amText: string;
    amTitle: string;
    amText1: string;
    amText2: string;
    enText: string;
    enTitle: string;
    enText1: string;
    enText2: string;
    ruText: string;
    ruTitle: string;
    ruText1: string;
    ruText2: string;
}
