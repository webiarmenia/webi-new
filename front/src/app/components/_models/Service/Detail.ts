import {Text} from './Text';

export interface Detail {
    // // // Duplicates // // //
    type: string;
    // // // Image With Text // // //
    alignment: string;
    imageHeight: string;
    imageOrder: string;
    imagePosition: string;
    imgSize: string;
    imgURL: string;
    objectFit: string;
    text: Array<Text>;
    verticalAlignment: string;
    // Text
    amDescription: string;
    amTitle: string;
    descriptionAlign: string;
    enDescription: string;
    enTitle: string;
    ruDescription: string;
    ruTitle: string;
    titleAlign: string;
    titleHeight: string;
    // // // Image // // //
    img1: {
        imgURL: string;
        alignment: string;
        verticalAlignment: string;
        objectFit: string;
    };
    img2: {
        imgURL: string;
        alignment: string;
        verticalAlignment: string;
        objectFit: string;
    };
    // // // Card // // //
    backColor: boolean;
    cards: Array<Text>;
    count: string;
    hover: boolean;
    textAlign: string;
    size: string;
    // // // TextWithLogo // // //
    textWithLogo: Array<Text>;
    // // // Logo // // //
    logos: [
        {
            imgURL: string;
        }
        ];
    // // // Block // // //
    row1: {
        amDescription1: string;
        amDescription2: string;
        amTitle: string;
        amTitle2: string;
        enDescription1: string;
        enDescription2: string;
        enTitle1: string;
        enTitle2: string;
        img1URL: string;
        img2URL: string;
        ruDescription1: string;
        ruDescription2: string;
        ruTitle1: string;
        ruTitle2: string;
    };
    row2: {
        amDescription1: string;
        amDescription2: string;
        amTitle: string;
        amTitle2: string;
        enDescription1: string;
        enDescription2: string;
        enTitle1: string;
        enTitle2: string;
        img1URL: string;
        img2URL: string;
        ruDescription1: string;
        ruDescription2: string;
        ruTitle1: string;
        ruTitle2: string;
    };

    // // // Slider // // //
    sliders: [
        {
            alignment: string;
            imageHeight: string;
            imagesURL: [
                {
                    imgURL: string;
                }
                ];
            objectFit: string;
            verticalAlignment: string;
            watchList: string;
        }
        ];
}
