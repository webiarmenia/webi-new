export interface Language {
    am: string;
    ru: string;
    en: string;
}
