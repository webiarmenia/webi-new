export interface Text {
    amContent: string;
    amTitle: string;
    enContent: string;
    enTitle: string;
    ruContent: string;
    ruTitle: string;
    // // // TextWithLogo/Card // // //
    imgURL?: string;
}
