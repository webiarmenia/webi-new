import {Page} from './Page';

export interface ServerResponse {
    success: boolean;
    pages: Array<Page>;
    msg: string;
}
