import {Detail} from './Detail';
import {Language} from './Language';

export interface Page {
    category: string;
    created: string;
    description: Language;
    details: Array<Detail>;
    grEnd: string;
    grStart: string;
    image: string;
    slug: string;
    title: Language;
    updated: string;
    _id: string;
}
