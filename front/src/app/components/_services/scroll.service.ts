import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ScrollService {
    changePortfolioColors = new Subject<any>();
    changeColors = new Subject<any>();
    scrollAnimation = new Subject<any>();

    data = null;

    constructor() {
    }

    // // // Colors // // //

    getPortfolioColor() {
        return this.changePortfolioColors.asObservable();
    }

    setPortfolioColor(e: boolean) {
        this.changePortfolioColors.next(e);
    }

    getColors() {
        return this.changeColors.asObservable();
    }

    setColors(e) {
        this.changeColors.next(e);
    }

    // // // Scroll Animation // // //

    getScrollAnimation() {
        return this.scrollAnimation.asObservable();
    }

    setScrollAnimation(e) {
        const data = {
            introduction: e.introduction,
            slider: e.slider,
            process: e.process,
            portfolio: e.portfolio,
            technology: e.technology,
            suggest: e.suggest,
            team: e.team
        };
        this.scrollAnimation.next(data);
    }
}
