import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';

import {Alert, AlertType} from '../_models/alert';


@Injectable()
export class AlertService {
    loading: string;
    private subject = new Subject<Alert>();
    private keepAfterRouteChange = false;
    private loadingSubject = new Subject<any>();

    constructor(private router: Router) {
        router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if (this.keepAfterRouteChange) {
                    this.keepAfterRouteChange = false;
                } else {
                    this.clear();
                }
            }
        });
    }

    getAlert(alertId?: string): Observable<any> {
        return this.subject.asObservable().pipe(filter((x: Alert) => x && x.alertId === alertId));
    }

    success(message: string, info: any) {
        this.alert(new Alert({ message, info, type: AlertType.Success }));
        setTimeout(() => {
            this.clear(this.alert[0]);
        }, 2000);
    }

    error(message: string, info: any) {
        this.alert(new Alert({ message, info, type: AlertType.Error }));
        setTimeout(() => {
            this.clear(this.alert[0]);
        }, 2000);
    }

    alert(alert: Alert) {
        this.keepAfterRouteChange = alert.keepAfterRouteChange;
        this.subject.next(alert);
    }

    clear(alertId?: string) {
        this.subject.next(new Alert({ alertId }));
    }

    alert_loading(exp) {
        if (exp === 'show') {
            this.loading = 'block';
            this.loadingSubject.next(this.loading);
        } else if (exp === 'close') {
            this.loading = 'none';
            this.loadingSubject.next(this.loading);
        }
    }

    get_alert_loading(): Observable<any>  {
        return this.loadingSubject.asObservable();
    }
}
