import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../../app.globals';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ServicesService {
    queryUrl = this.global.queryUrl;

    constructor(private http: HttpClient, private global: Globals) {
    }

    getAllPages(type) {
        return this.http.get(`${this.queryUrl}/${type}`)
            .pipe(
                map(pages => pages),
                catchError(err => throwError(err))
            );
    }

    getPageById(slug) {
        return this.http.get(`${this.queryUrl}service`, {params: slug})
            .pipe(
                map(page => page),
                catchError(err => throwError(err))
            );
    }
}
