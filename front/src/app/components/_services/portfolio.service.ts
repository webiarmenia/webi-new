import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../../app.globals';
import {Observable, Subject, throwError} from 'rxjs';
import {Portfolio} from '../_models/portfolio';
import {catchError, map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class PortfolioService {
    currentPortfolio = new Subject<any>();
    query = this.global.queryUrl;
    allPortfolio: Portfolio[];

    constructor(private http: HttpClient, private global: Globals) {
    }

    setCurrentPortfolio(e) {
        this.currentPortfolio.next(e);
    }

    getCurrentPortfolio() {
        return this.currentPortfolio.asObservable();
    }


    getAll(): Observable<Portfolio[]> {
        const data = 'data';
        return this.http.get(`${this.query}portfolio`)
            .pipe(map(response => {
                    this.allPortfolio = response[data].map(item => {
                        return {
                            id: item._id,
                            slug: item.slug,
                            sliderPosition: item.sliderPosition,
                            title: item.title ? {
                                en: item.title.en,
                                ru: item.title.ru,
                                am: item.title.am
                            } : {},
                            // description: {
                            //     en: item.description.en,
                            //     ru: item.description.ru,
                            //     am: item.description.am
                            // },
                            shortDescription: item.shortDescription ? {
                                en: item.shortDescription.en,
                                ru: item.shortDescription.ru,
                                am: item.shortDescription.am
                            } : {},
                            hover: item.hover ? {
                                en: item.hover.en,
                                ru: item.hover.ru,
                                am: item.hover.am
                            } : {},
                            url: item.url,
                            image: item.image,
                            details: item.details
                        };
                    });
                    return this.allPortfolio;
                }),
                catchError(err => {
                    console.log(err);
                    return throwError(err);
                }));
    }

    getOne(id): Observable<Portfolio> {
        return this.http.get(`${this.query}portfolio/${id}`)
            .pipe(map(data => {
                return data['data'];
            }));
    }
}
