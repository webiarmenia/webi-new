import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'replace'
})
export class ReplacePipe implements PipeTransform {

    transform(value, args): any {
        return value.replace(args, '<br><br>');
    }

}
