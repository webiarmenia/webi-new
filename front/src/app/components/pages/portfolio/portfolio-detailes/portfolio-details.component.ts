import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';

import {PortfolioService} from '../../../_services/portfolio.service';

import {Portfolio} from '../../../_models/portfolio';
import {SettingService} from '../../../_services/setting.service';
import {Globals} from '../../../../app.globals';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {ScrollService} from '../../../_services/scroll.service';
import {AlertService} from '../../../_services/alert.service';

@Component({
    selector: 'app-portfolio-details',
    templateUrl: './portfolio-details.component.html',
    styleUrls: ['./portfolio-details.component.scss'],
})
export class PortfolioDetailsComponent implements OnInit, OnDestroy {

    scrolled = false;
    titleLeft;
    titleTop;
    scrollSub: Subscription;
    changeColor = false;
    deleteHeight;

    currentPortfolio: Portfolio;
    done = false;
    id;
    title;
    text;
    imageUrl;
    portfolio: Portfolio[];

    @HostListener('wheel', ['$event']) mouseWheel(e) {
        if (!this.scrolled) {
            if (e.deltaY > 0) {
                document.getElementById('html').style.overflow = 'hidden';
                return this.start();
            }
        }
    }

    @HostListener('document:scroll', ['$event']) mouseScroll(e) {
        const footer = document.getElementsByTagName('footer')[0] as HTMLElement;
        if (window.pageYOffset + 100 > footer.offsetTop) {
            document.getElementById('nav').style.display = 'none';
            document.getElementById('sidebar-wrapper').style.display = 'none';
        } else {
            document.getElementById('nav').style.display = 'block';
            document.getElementById('sidebar-wrapper').style.display = 'block';
        }
    }


    constructor(private route: ActivatedRoute,
                private alertService: AlertService,
                private portfolioService: PortfolioService,
                private settingsService: SettingService,
                private scrollService: ScrollService,
                private global: Globals) {
        alertService.alert_loading('show');
        let slug: any;
        route.params.subscribe(params => slug = params.id);

        this.portfolioService.getAll().subscribe(p => p.map(port => {
                if (port.slug === slug) {
                    console.log('if', port);
                    this.imageUrl = port.image;
                    this.currentPortfolio = port;
                    alertService.alert_loading('close');
                    return this.done = true;
                }
            }),
            err => {
                this.alertService.alert_loading('close');
                this.alertService.error('error', {message: 'Internet error'});
            });

        this.scrollSub = this.scrollService.getPortfolioColor()
            .subscribe(color => this.scrolled = color);
    }


    ngOnInit() {
        setTimeout(() => {
            document.getElementById('nav').classList.add('portfolio-detail-dark');
        }, 200);
    }

    ngOnDestroy(): void {
        document.getElementById('nav').classList.remove('portfolio-detail');
    }

    start() {
        const scrollHeight = document.documentElement.clientHeight;
        const titleTop = Math.floor((scrollHeight - 200) / 2);
        const titleTopTest = Math.floor(titleTop / 4);
        this.scrolled = true;

        let x = 10;

        const interval = setInterval(() => {
            if (x > 0 && x < 20) {
                this.titleTop = titleTop + 'px';
                this.titleLeft = '20%';
                this.deleteHeight = '100vh';
            } else if (x > 20 && x < 40) {
                this.titleTop = (titleTopTest * 3) + 'px';
                this.titleLeft = '15%';
                this.deleteHeight = '75vh';
            } else if (x > 40 && x < 60) {
                this.changeColor = true;
                this.titleTop = (titleTopTest * 2) + 'px';
                this.titleLeft = '10%';
                this.deleteHeight = '50vh';
            } else if (x > 60 && x < 80) {
                this.titleTop = (titleTopTest) + 'px';
                this.titleLeft = '5%';
                this.deleteHeight = '25vh';
            } else if (x > 80 && x < 100) {
                this.titleTop = '100px';
                this.titleLeft = 0;
                this.deleteHeight = 0;
                this.scrollService.setPortfolioColor(true);
                this.scrollService.setColors(true);
                this.scrolled = true;
                return clearInterval(interval);
            }
            x += 20;
        }, 100);

        setTimeout(() => {
            document.getElementById('nav').classList.remove('portfolio-detail-dark');
            document.getElementById('nav').classList.add('portfolio-detail-light');

        }, 1200);

        setTimeout(() => {
            document.getElementById('html').style.overflow = 'auto';
        }, 2000);

    }

}
