import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {PortfolioRoutingModule} from './portfolio-routing.module';

import {SharedComponentsModule, SharedModule} from '../../_modules';
import {PortfolioComponent} from './portfolio.component';
import {PortfolioDetailsComponent} from './portfolio-detailes/portfolio-details.component';
import {AllPortfoliosComponent} from './all-portfolios/all-portfolios.component';


@NgModule({
    declarations: [
        PortfolioComponent,
        PortfolioDetailsComponent,
        AllPortfoliosComponent
    ],
    imports: [
        CommonModule,
        PortfolioRoutingModule,
        SharedModule,
        SharedComponentsModule
    ]
})
export class PortfolioModule {
}
