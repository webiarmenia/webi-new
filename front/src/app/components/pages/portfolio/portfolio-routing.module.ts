import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PortfolioComponent} from './portfolio.component';
import {PortfolioDetailsComponent} from './portfolio-detailes/portfolio-details.component';

const routes: Routes = [
    {path: '' , component: PortfolioComponent},
    {path: ':id', component: PortfolioDetailsComponent},
    {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
    exports: [
        RouterModule
    ],
    imports: [
        RouterModule.forChild(routes)
    ]
})
export class PortfolioRoutingModule {
}
