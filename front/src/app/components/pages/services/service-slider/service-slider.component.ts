import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Detail} from '../../../_models/Service/Detail';
import {OwlOptions} from 'ngx-owl-carousel-o/lib/models/owl-options.model';

@Component({
    selector: 'app-service-slider',
    templateUrl: './service-slider.component.html',
    styleUrls: ['./service-slider.component.scss']
})
export class ServiceSliderComponent implements OnInit {

    mouseWillCount = 0;

    mainOptions: OwlOptions = {
        loop: true,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: true,
        dots: false,
        margin: 50,
        autoplay: true,
        autoplaySpeed: 5000,
        autoplayTimeout: 1000,
        autoplayHoverPause: true,
        navSpeed: 700,
        responsive: {
            0: {
                items: 1
            },
            992: {
                items: 2
            }
        },
        nav: false
    };

    subOptions: OwlOptions = {
        nav: false,
        loop: true,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: true,
        dots: false,
        navSpeed: 700,
        margin: 24,
        stagePadding: 50,
        responsive: {
            0: {
                items: 1,
                // stagePadding: 5,
            },
            500: {
                items: 1,
                // stagePadding: 50,
            },
            740: {
                items: 2
            },
        },
        autoplay: true,
        autoplayTimeout: 1500,
        autoplayHoverPause: false,
        autoplaySpeed: 3000,
    };

    @Input() content: Detail;

    @ViewChild('sliderOne', {static: false}) sliderOne;
    @ViewChild('sliderTwo', {static: false}) sliderTwo;

    constructor() {
    }

    ngOnInit() {
        console.log(this.content);
    }

    mouseEnter() {
        document.getElementById('html').style.overflowY = 'hidden';
    }

    mouseLeave() {
        document.getElementById('html').style.overflowY = 'auto';
    }

    mouseWheel(event) {
        console.log(event.deltaY);
        this.mouseWillCount++;
        if (this.mouseWillCount === 1) {
            setTimeout(() => {
                event.deltaY > 0 ? this.sliderOne.next() : this.sliderOne.prev();
                event.deltaY > 0 ? this.sliderTwo.next() : this.sliderTwo.prev();
                this.mouseWillCount = 0;
            }, 100);
        }
    }

}
