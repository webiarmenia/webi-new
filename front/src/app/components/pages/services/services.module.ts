import {NgModule} from '@angular/core';
import {SharedComponentsModule, SharedModule} from '../../_modules';
import {ServicesRoutingModule} from './services-routing.module';
import {ServicesComponent} from './services.component';
import { ServiceTextImageComponent } from './service-text-image/service-text-image.component';
import { ServiceTextComponent } from './service-text/service-text.component';
import { ServiceImageComponent } from './service-image/service-image.component';
import { ServiceCartTypeOneComponent } from './service-cart-type-one/service-cart-type-one.component';
import { ServiceCartTypeTwoComponent } from './service-cart-type-two/service-cart-type-two.component';
import { ServiceCardTypeThreeComponent } from './service-card-type-three/service-card-type-three.component';
import { ServiceLogoComponent } from './service-logo/service-logo.component';
import { ServiceBlockComponent } from './service-block/service-block.component';
import { ServiceSliderComponent } from './service-slider/service-slider.component';


@NgModule({
    declarations: [
        ServicesComponent,
        ServiceTextImageComponent,
        ServiceTextComponent,
        ServiceImageComponent,
        ServiceCartTypeOneComponent,
        ServiceCartTypeTwoComponent,
        ServiceCardTypeThreeComponent,
        ServiceLogoComponent,
        ServiceBlockComponent,
        ServiceSliderComponent,
    ],
    imports: [
        SharedModule,
        SharedComponentsModule,
        ServicesRoutingModule
    ],
    exports: []
})

export class ServicesModule {
}
