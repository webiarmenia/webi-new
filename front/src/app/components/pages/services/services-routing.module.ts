import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ServicesComponent} from './services.component';

const routes: Routes = [
    {path: '', component: ServicesComponent, pathMatch: 'full'},
    {path: ':slug', component: ServicesComponent},
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)],
})

export class ServicesRoutingModule {
}
