import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {ServicesService} from '../../_services/services.service';
import {ActivatedRoute} from '@angular/router';
import {ServerResponse} from '../../_models/Service/ServerResponse';
import {AlertService} from '../../_services/alert.service';
import {Page} from '../../_models/Service/Page';
import {ScrollService} from '../../_services/scroll.service';
import {ActionsService} from '../../_services/actions.service';
import {Subscription} from 'rxjs';
import {ResponsiveData} from '../../_models/ResponsiveData';

@Component({
    selector: 'app-services',
    templateUrl: './services.component.html',
    styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit, OnDestroy {
    done = false;
    pageInfo: Page;

    // // FONT_SIZE // // //
    windowSubscription: Subscription;
    windowSize: ResponsiveData;

    constructor(
        private alertService: AlertService,
        private servicesService: ServicesService,
        private scrollService: ScrollService,
        private actionsService: ActionsService,
        private route: ActivatedRoute
    ) {
        alertService.alert_loading('show');
        this.initPage();
        this.windowSubscription = actionsService.getWindowSize()
            .subscribe((size: ResponsiveData) => this.windowSize = size);
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.windowSubscription.unsubscribe();
    }

    @HostListener('window:scroll') scroll() {
        if (window.pageYOffset + 100 > document.documentElement.clientHeight) {
            this.scrollService.setColors(true);
        } else if (window.pageYOffset + 100 < document.documentElement.clientHeight) {
            this.scrollService.setColors(false);
        }
    }

    initPage() {
        this.route.params.subscribe(
            data => {
                if (data.slug) {
                    return this.servicesService.getPageById(data.slug)
                        .subscribe(
                            (page: ServerResponse) => {
                                this.alertService.alert_loading('close');
                                if (page.success) {
                                    console.log(page);
                                    this.pageInfo = page.pages[0];
                                    this.done = true;
                                } else {
                                    this.alertService.error('error', page.msg);
                                }
                            }
                        );
                } else {
                    return this.servicesService.getAllPages('service')
                        .subscribe(
                            (pages: ServerResponse) => {
                                console.log(pages);
                            }
                        );
                }
            }
        );
    }

}
