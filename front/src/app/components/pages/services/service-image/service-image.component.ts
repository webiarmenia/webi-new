import {Component, Input, OnInit} from '@angular/core';
import {Detail} from '../../../_models/Service/Detail';

@Component({
    selector: 'app-service-image',
    templateUrl: './service-image.component.html',
    styleUrls: ['./service-image.component.scss']
})
export class ServiceImageComponent implements OnInit {
    @Input() content: Detail;

    constructor() {
    }

    ngOnInit() {
    }

}
