import {Component, Input, OnInit} from '@angular/core';
import {Detail} from '../../../_models/Service/Detail';

@Component({
    selector: 'app-service-logo',
    templateUrl: './service-logo.component.html',
    styleUrls: ['./service-logo.component.scss']
})
export class ServiceLogoComponent implements OnInit {

    @Input() content: Detail;

    constructor() {
    }

    ngOnInit() {
    }

}
