import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Detail} from '../../../_models/Service/Detail';
import {Subscription} from 'rxjs';
import {ResponsiveData} from '../../../_models/ResponsiveData';
import {ActionsService} from '../../../_services/actions.service';

@Component({
    selector: 'app-service-card-type-three',
    templateUrl: './service-card-type-three.component.html',
    styleUrls: ['./service-card-type-three.component.scss']
})
export class ServiceCardTypeThreeComponent implements OnInit, OnDestroy {

    // // FONT_SIZE // // //
    windowSubscription: Subscription;
    windowSize: ResponsiveData;

    @Input() content: Detail;

    constructor(private actionsService: ActionsService) {
        this.windowSubscription = actionsService.getWindowSize()
            .subscribe((size: ResponsiveData) => this.windowSize = size);
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.windowSubscription.unsubscribe();
    }

}
