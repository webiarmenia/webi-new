import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {ResponsiveData} from '../../../_models/ResponsiveData';
import {Detail} from '../../../_models/Service/Detail';
import {ActionsService} from '../../../_services/actions.service';

@Component({
    selector: 'app-service-block',
    templateUrl: './service-block.component.html',
    styleUrls: ['./service-block.component.scss']
})
export class ServiceBlockComponent implements OnInit, OnDestroy {

    // // FONT_SIZE // // //
    windowSubscription: Subscription;
    windowSize: ResponsiveData;

    @Input() content: Detail;

    constructor(private actionsService: ActionsService) {
        this.windowSubscription = actionsService.getWindowSize()
            .subscribe((size: ResponsiveData) => this.windowSize = size);
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.windowSubscription.unsubscribe();
    }

}
