import {NgModule} from '@angular/core';
import {IntroductionComponent} from './introduction/introduction.component';
import {ProcessComponent} from './process/process.component';
import {SuggestComponent} from './suggest/suggest.component';
import {TeamComponent} from './team/team.component';
import {PortfolioComponent} from './portfolio/portfolio.component';
import {TechnologyComponent} from '../technology/technology.component';
import {SharedComponentsModule, SharedModule} from '../../_modules';
import {HomeRoutingModule} from './home-routing.module';
import {HomeComponent} from './home.component';
import {ServicesComponent} from './services/services.component';
import {AdvantageComponent} from './advantage/advantage.component';

import {MouseSliderComponent} from './services/mouse-slider-services/mouse-slider.component';
import {MouseSliderAdvantageComponent} from './advantage/mouse-slider-adventage/mouse-slider-advantage.component';
import {SliderInnerComponent} from './portfolio/slider-inner/slider-inner.component';
import {ReplacePipe} from '../../_pipes/replace.pipe';
import {TeamSliderComponent} from './team/team-slider/team-slider.component';


@NgModule({
    declarations: [
        HomeComponent,
        IntroductionComponent,
        ProcessComponent,
        PortfolioComponent,
        TechnologyComponent,
        SuggestComponent,
        TeamComponent,
        ServicesComponent,
        AdvantageComponent,
        MouseSliderComponent,
        MouseSliderAdvantageComponent,
        SliderInnerComponent,
        ReplacePipe,
        SliderInnerComponent,
        TeamSliderComponent
    ],
    imports: [
        SharedModule,
        SharedComponentsModule,
        HomeRoutingModule,
    ],
    exports: [
        HomeComponent,
        IntroductionComponent,
        ProcessComponent,
        PortfolioComponent,
        TechnologyComponent,
        SuggestComponent,
        TeamComponent,
    ]
})
export class HomeModule {
}
