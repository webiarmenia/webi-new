import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-advantage',
    templateUrl: './advantage.component.html',
    styleUrls: ['./advantage.component.scss']
})
export class AdvantageComponent implements OnInit {
    titleTop: string;

    constructor() {
    }

    ngOnInit() {
    }

    mouseOver(e: MouseEvent) {
        setTimeout(() => {
            if (e.offsetY > 110) {
                this.titleTop = (e.offsetY - 100) + 'px';
            }
        }, 300);
    }

    mouseLeave() {
        this.titleTop = '0';
    }


}
