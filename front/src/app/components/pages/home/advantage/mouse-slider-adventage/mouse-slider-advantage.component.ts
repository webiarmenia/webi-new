import {Component, OnInit, ViewChild} from '@angular/core';

@Component({
    selector: 'app-mouse-slider-advantage',
    templateUrl: './mouse-slider-advantage.component.html',
    styleUrls: ['./mouse-slider-advantage.component.scss']
})
export class MouseSliderAdvantageComponent implements OnInit {

    @ViewChild('sliderInner', {static: false}) sliderInner;
    @ViewChild('adSlider', {static: false}) adSlider;

    mouseOn = false;
    done = false;
    mouseWillCount = 0;

    customOptionsAd: any = {
        nav: false,
        loop: true,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: false,
        dots: false,
        navSpeed: 700,
        margin: 45,
        stagePadding: 50,
        responsive: {
            0: {
                items: 1,
                stagePadding: 0,
            },
            500: {
                items: 1,
                stagePadding: 20,
            },
            740: {
                items: 2
            },
            992: {
                items: 3
            }
        }
    };

    constructor() {
    }

    ngOnInit() {
    }

    mouseEnter() {
        document.getElementById('html').style.overflowY = 'hidden';
        this.mouseOn = true;
    }

    mouseLeave() {
        document.getElementById('html').style.overflowY = 'auto';
    }

    mouseWheel(event) {
        this.mouseWillCount++;
        if (this.mouseWillCount === 1) {
            setTimeout(() => {
                event.deltaY > 0 ? this.adSlider.next() : this.adSlider.prev();
                this.mouseWillCount = 0;
            }, 100);
        }
    }


}
