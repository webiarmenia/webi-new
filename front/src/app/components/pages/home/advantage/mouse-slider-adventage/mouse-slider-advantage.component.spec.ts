import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MouseSliderAdventageComponent } from './mouse-slider-advantage.component';

describe('MouseSliderAdventageComponent', () => {
  let component: MouseSliderAdventageComponent;
  let fixture: ComponentFixture<MouseSliderAdventageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MouseSliderAdventageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MouseSliderAdventageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
