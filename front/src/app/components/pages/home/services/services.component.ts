import {Component, OnInit} from '@angular/core';
import {CardService} from '../../../_services/card.service';
import {Card} from '../../../_models/card';

@Component({
    selector: 'app-services',
    templateUrl: './services.component.html',
    styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {
    cards: Card[];

    constructor(private cardService: CardService) {
    }

    ngOnInit() {
        this.initCards();
    }

    initCards() {
        // this.cardService.getAll().subscribe( c => this.cards = c );
    }

}
