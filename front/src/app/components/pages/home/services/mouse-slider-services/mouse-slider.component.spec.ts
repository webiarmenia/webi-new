import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MouseSliderComponent } from './mouse-slider.component';

describe('MouseSliderComponent', () => {
  let component: MouseSliderComponent;
  let fixture: ComponentFixture<MouseSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MouseSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MouseSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
