import {Component, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {Card} from '../../../../_models/card';
import {CardService} from '../../../../_services/card.service';

@Component({
    selector: 'app-mouse-slider',
    templateUrl: './mouse-slider.component.html',
    styleUrls: ['./mouse-slider.component.scss']
})
export class MouseSliderComponent implements OnInit {

    @ViewChild('sliderInner', {static: false}) sliderInner;
    @ViewChild('owlCar', {static: false}) owlCar;

    mouseOn = false;
    mouseWillCount = 0;

    cards: Card[] | Subscription;
    done = false;
    customOptions: any = {
        nav: false,
        loop: false,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: false,
        dots: false,
        navSpeed: 700,
        margin: 50,
        stagePadding: 100,
        width: 400,
        responsive: {
            0: {
                items: 1,
                stagePadding: 10,
                margin: 10,
            },
            400: {
                items: 1
            },
            740: {
                items: 2,
                stagePadding: 50
            },
            1200: {
                items: 3
            }
        }
    };

    constructor(private service: CardService) {
    }

    ngOnInit() {
        this.initCards();
    }

    initCards() {
        this.service.getAll().subscribe(
            d => {
                this.cards = d;
                this.done = true;
            }
        );
    }

    mouseEnter() {
        document.getElementById('html').style.overflowY = 'hidden';
        this.mouseOn = true;
    }

    mouseLeave() {
        document.getElementById('html').style.overflowY = 'auto';
    }

    mouseWheel(event) {
        console.log(this.owlCar);
        this.mouseWillCount++;
        if (this.mouseWillCount === 1) {
            setTimeout(() => {
                event.deltaY > 0 ? this.owlCar.next() : this.owlCar.prev();
                this.mouseWillCount = 0;
            }, 100);
        }
    }

}
