import {
    Component, ElementRef,
    HostListener, OnDestroy,
    OnInit, ViewChild,
} from '@angular/core';
import {ScrollService} from '../../_services/scroll.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

    @ViewChild('scrollDiv', {static: true}) scrollDiv: ElementRef;
    done = true;
    scrollHeight: number;
    windowHeight: number;
    scrollTop = 0;
    scrollPosition = 0;
    scrollCount;
    currentSection = 'section1';
    section1 = true;
    section2 = false;
    section3 = false;
    section4 = false;
    section5 = false;
    footer = false;
    wheelCheck = true;

    wheelCount = 0;
    wheelCountStart = 0;
    newTime = 0;
    touchPad: boolean;
    checkScroll: number;
    checkForFooter = false;

    mobile = false;

    @HostListener('wheel', ['$event']) mouseWheel(e) {
        if (this.wheelCount === 0) {
            this.wheelCountStart = new Date().getTime();
        }

        this.newTime = new Date().getTime();
        this.wheelCount++;

        if (new Date().getTime() - this.wheelCountStart > 100) {
            this.touchPad = this.wheelCount > 10;
        }

        if (this.scrollPosition >= this.windowHeight * 5) {
            document.getElementById('nav').style.display = 'none';
            document.getElementById('sidebar-wrapper').style.display = 'none';
            return this.initScrollDesktop(e.deltaY);
        } else {
            document.getElementById('nav').style.display = 'block';
            document.getElementById('sidebar-wrapper').style.display = 'block';
        }

        if (this.scrollPosition > this.windowHeight * 4 && this.scrollPosition < this.windowHeight * 5) {
            this.scrollPosition = this.windowHeight * 5;
        }

        if (this.touchPad && this.wheelCheck) {
            if (e.deltaY > 0) {
                this.initScrollDesktop(this.windowHeight, 'touchPad');
            } else {
                this.initScrollDesktop(-this.windowHeight, 'touchPad');
            }
        } else if (!this.touchPad && this.wheelCheck) {
            if (e.deltaY > 0) {
                this.initScrollDesktop(this.windowHeight, 'wheel');
            } else {
                this.initScrollDesktop(-this.windowHeight, 'wheel');
            }
        }

        this.wheelCheck = false;

        // // // TimeOut // // //

        if (!this.touchPad && this.newTime - this.wheelCountStart > 1000) {
            document.getElementById('parentDiv').style.transition = '1s all';
            this.wheelCount = 0;
            this.wheelCheck = true;
        } else if (this.touchPad && this.newTime - this.wheelCountStart > 550) {
            document.getElementById('parentDiv').style.transition = '550ms all';
            this.wheelCount = 0;
            this.wheelCheck = true;
        }
    }

    @HostListener('window:scroll', ['$event']) scroll(e) {
        console.log('SCROLL');
    }

    @HostListener('touchmove', ['$event']) touchMove() {
    }

    @HostListener('window:keydown', ['$event'])
    onKeyDown(event) {
        if (event.keyCode === 32) {
            this.initScrollDesktop(this.windowHeight);
        }
        if (event.keyCode === 40) {
            this.initScrollDesktop(this.windowHeight);
        } else if (event.keyCode === 38) {
            this.initScrollDesktop(-this.windowHeight);
        }
    }


    constructor(private scrollService: ScrollService) {
    }

    ngOnInit() {
        this.windowHeight = window.innerHeight;
        if (document.documentElement.clientWidth <= 992) {
            this.mobile = true;
        }
    }

    ngOnDestroy(): void {
        window.scrollTo(0, 0);
        // window.scrollTo(0, window.innerWidth < 1000 ? 100 : 200);
    }

    // // // // // SCROLL ANIMATIONS // // // // //

    initScrollDesktop(position: number, type?: string) {
        this.scrollHeight = this.scrollDiv.nativeElement.clientHeight;
        // this.scrollCount = Math.floor(this.scrollHeight / this.windowHeight);
        // this.scrollPosition += this.scrollPosition >= this.windowHeight * 5 ? Math.ceil(position / 5) : position;
        // console.log(this.scrollPosition);
        this.scrollPosition += position;

        if (this.scrollPosition < 0) {
            this.scrollPosition = 0;
        } else if (this.scrollPosition > this.scrollHeight - this.windowHeight) {
            this.scrollPosition = this.scrollHeight - this.windowHeight;
        }
        this.checkForFooter = !this.checkForFooter;

        this.scrollDiv.nativeElement.style.transform = `translate3d(0, ${-this.scrollPosition}px, 0)`;
        this.setHeaderColors(this.scrollPosition);
    }

    setHeaderColors(position: number) {
        setTimeout(() => {
            if (position === 0) {
                this.scrollService.setColors(false);
            } else if (position === this.windowHeight) {
                this.scrollService.setColors(true);
            } else if (position === this.windowHeight * 2) {
                this.scrollService.setColors(false);
            } else if (position === this.windowHeight * 3) {
                this.scrollService.setColors(true);
            } else if (position === this.windowHeight * 4) {
                this.scrollService.setColors(false);
            }
        }, 350);
    }
}

