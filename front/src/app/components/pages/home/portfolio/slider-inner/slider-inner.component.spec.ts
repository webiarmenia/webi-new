import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SliderInnerComponent } from './slider-inner.component';

describe('SliderInnerComponent', () => {
  let component: SliderInnerComponent;
  let fixture: ComponentFixture<SliderInnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SliderInnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SliderInnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
