import {Component, OnInit, ViewChild} from '@angular/core';
import {PortfolioService} from '../../../../_services/portfolio.service';
import {Portfolio} from '../../../../_models/portfolio';
import {Router} from '@angular/router';

@Component({
    selector: 'app-slider-inner',
    templateUrl: './slider-inner.component.html',
    styleUrls: ['./slider-inner.component.scss']
})
export class SliderInnerComponent implements OnInit {
    mobile = false;

    @ViewChild('sliderOne', {static: false}) sliderOne;
    @ViewChild('sliderTwo', {static: false}) sliderTwo;

    done = false;
    mouseWillCount = 0;
    cardsTop: Portfolio[] = [];
    cardsBottom: Portfolio[] = [];
    portTest: Portfolio[];
    customOptionsOne: any;
    customOptionsTwo: any;

    constructor(private portfolioService: PortfolioService, private router: Router) {
    }

    ngOnInit() {
        if (document.documentElement.clientWidth <= 720) {
            this.mobile = true;
        }
        this.getAllPortfolios();
    }

    getAllPortfolios() {
        this.portfolioService.getAll().toPromise().then(card => {
            card.map(portfolio => {
                portfolio.sliderPosition === 'top' ? this.cardsTop.push(portfolio) : this.cardsBottom.push(portfolio);
            });
        }).then(done => {
            this.initSliders();
            this.done = true;
        });
    }

    openPortfolio(slug) {
        this.router.navigate(['portfolio', slug]).then(d => console.log(d));
    }

    over(e: Portfolio, index) {
        const preview = {
            id: index,
            title: e.title.en,
            desc: e.shortDescription.en
        };
        this.portfolioService.setCurrentPortfolio(preview);
    }

    leave() {
        // this.portfolioService.setCurrentPortfolio(null);
    }

    mouseEnter() {
        document.getElementById('html').style.overflowY = 'hidden';
    }

    mouseLeave() {
        this.portfolioService.setCurrentPortfolio(null);
        document.getElementById('html').style.overflowY = 'auto';
    }

    mouseWheel(event) {
        this.mouseWillCount++;
        if (this.mouseWillCount === 1) {
            setTimeout(() => {
                console.log(this.sliderOne);
                event.deltaY > 0 ? this.sliderOne.next() : this.sliderOne.prev();
                event.deltaY > 0 ? this.sliderTwo.next() : this.sliderTwo.prev();
                this.mouseWillCount = 0;
            }, 100);
        }
    }

    initSliders() {
        this.customOptionsOne = {
            nav: false,
            loop: false,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: false,
            dots: false,
            navSpeed: 700,
            margin: 24,
            stagePadding: 50,
            responsive: {
                0: {
                    items: 1,
                    stagePadding: 5,
                },
                500: {
                    items: 1,
                    stagePadding: 50,
                },
                740: {
                    items: 2
                },
            }
        };
        this.customOptionsTwo = {
            nav: false,
            loop: false,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: false,
            dots: false,
            navSpeed: 700,
            margin: 20,
            stagePadding: 100,
            responsive: {
                0: {
                    items: 1,
                    stagePadding: 10,
                },
                500: {
                    items: 2,
                    stagePadding: 20,
                },
                740: {
                    items: 3
                },
            }
        };
    }

}
