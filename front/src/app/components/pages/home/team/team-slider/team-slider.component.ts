import {Component, OnInit, ViewChild} from '@angular/core';
import {TeamService} from '../../../../_services/team.service';
import {Team} from '../../../../_models/team';
import {Globals} from '../../../../../app.globals';

@Component({
    selector: 'app-team-slider',
    templateUrl: './team-slider.component.html',
    styleUrls: ['./team-slider.component.scss']
})
export class TeamSliderComponent implements OnInit {

    @ViewChild('teamCarousel', {static: false}) teamCarousel;

    team: Team[];
    done = false;
    imageUrl;
    customOptionsTeam = {
        nav: false,
        loop: true,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: false,
        dots: false,
        navSpeed: 700,
        responsive: {
            0: {
                items: 1
            }
        }
    };

    constructor(
        private teamService: TeamService,
        global: Globals
    ) {
        this.imageUrl = global.imageUrl + 'team/';
    }

    ngOnInit() {
        this.getTeam();
    }

    mouseEnter() {
        document.getElementById('html').style.overflowY = 'hidden';
    }

    mouseLeave() {
        document.getElementById('html').style.overflowY = 'auto';
    }

    mouseWheel(event) {
        event.deltaY > 0 ? this.teamCarousel.next() : this.teamCarousel.prev();
    }

    initPerson(index) {
        const selectid = document.getElementById('slider-item-' + index);
        selectid.classList.add('slide-item-animation');
        console.log(selectid.classList);
        // this.teamService.initIndex(index);
    }

    getTeam() {
        this.team = this.teamService.allTeem || this.teamService.getAll().subscribe();
    }

    closeTab() {
        document.getElementById('html').style.overflowY = 'auto';
        document.getElementById('detail-view').classList.remove('animation-start');
        document.getElementById('team-wrapper').style.opacity = '1';
        document.getElementById('curtain').style.height = '0';
    }
}
