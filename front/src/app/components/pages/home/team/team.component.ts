import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TeamService} from '../../../_services/team.service';
import {Team} from '../../../_models/team';
import {SettingService} from '../../../_services/setting.service';
import {Globals} from '../../../../app.globals';
import {Subscription} from 'rxjs';
import {ScrollService} from '../../../_services/scroll.service';
import {fadeInSacleAnimation} from '../../../_animations';

export enum KEY_CODE {
    RIGHT_ARROW = 39,
    LEFT_ARROW = 37,
    ESC = 27
}

@Component({
    selector: 'app-team',
    templateUrl: './team.component.html',
    styleUrls: ['./team.component.scss'],
    animations: [fadeInSacleAnimation]

})
export class TeamComponent implements OnInit, OnDestroy {
    state = 'hide';
    stateSubscription: Subscription;
    tabToOpen;
    visible = true;
    done = false;
    team: Team[];
    title;
    text;
    imageUrl;
    mouseWillCount = 0;
    currantSlide;
    clickedElement;
    startPosition = {x: null, y: null, height: null, width: null};
    mobile = false;

    // @ts-ignore
    @ViewChild('teamCarousel', {static: false}) teamCarousel;
    @ViewChild('btnImg', {static: false}) btnImg: ElementRef;
    @ViewChild('teamDetailSlider', {static: false}) teamDetailSlider;
    customOptionsTeam: any;
    customOptions: any = {
        nav: false,
        loop: true,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: false,
        dots: false,
        navSpeed: 700,
        margin: 25,
        stagePadding: 10,
        width: 400,
        responsive: {
            0: {
                items: 1
            },
            500: {
                items: 2
            },
            900: {
                items: 2,
                stagePadding: 50,
            },
            1200: {
                items: 4
            },
            1500: {
                items: 5
            }
        }
    };

    @HostListener('window:keyup', ['$event'])
    keyEvent(event: KeyboardEvent) {
        if (event.keyCode === KEY_CODE.ESC) {
            this.closeTab();
        }
    }

    constructor(
        private scrollService: ScrollService,
        private el: ElementRef,
        private teamService: TeamService,
        private  settingsService: SettingService,
        global: Globals
    ) {
        this.stateSubscription = this.scrollService.getScrollAnimation().subscribe(
            animation => {
                if (animation.team) {
                    this.state = animation.team;
                }
            }
        );
        this.imageUrl = global.imageUrl + 'team/';
    }

    ngOnInit() {
        this.getTeam();
        this.title = this.settingsService.getValueByKeyLanguage('home-team-title', 'en');
        this.text = this.settingsService.getValueByKeyLanguage('home-team-text', 'en');
        this.visible = window.innerWidth >= 768; // Changed
        this.stateSubscription = this.teamService.selectedPersonIndex.subscribe(d => {
            this.tabToOpen = d;
            this.initSlide(d);
        });
    }

    ngOnDestroy() {
        this.stateSubscription.unsubscribe();
    }

    getTeam() {
        this.team = this.teamService.allTeem || this.teamService.getAll().subscribe();
    }

    closeTab() {
        this.tabToOpen = null;
        document.getElementById('html').style.overflowY = 'auto';
        document.getElementById('detail-view').classList.remove('animation-start');
        document.getElementById('team-wrapper').style.opacity = '1';
        document.getElementById('curtain').style.height = '0';
        this.clickedElement.style.display = 'block';
    }

    initSlide(d) {
        this.customOptionsTeam = {
            nav: false,
            loop: true,
            startPosition: d ? d : 0,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: false,
            dots: false,
            navSpeed: 700,
            responsive: {
                0: {
                    items: 1
                }
            }
        };
    }

    // ==============================================
    // ==============================================
    // ==============================================

    mouseEnter2() {
        document.getElementById('html').style.overflowY = 'hidden';
    }

    mouseLeave2() {
        document.getElementById('html').style.overflowY = 'auto';
    }

    mouseWheel2(event) {
        this.mouseWillCount++;
        if (this.mouseWillCount === 1) {
            setTimeout(() => {
                event.deltaY > 0 ? this.teamCarousel.next() : this.teamCarousel.prev();
                this.mouseWillCount = 0;
            }, 100);
        }
    }

    mouseEnter() {
        document.getElementById('html').style.overflowY = 'hidden';
    }

    mouseLeave() {
        document.getElementById('html').style.overflowY = 'auto';
    }

    mouseWheel(event) {
        this.mouseWillCount++;
        if (this.mouseWillCount === 1) {
            setTimeout(() => {
                event.deltaY > 0 ? this.nextSlid() : this.prevSlide();
                this.mouseWillCount = 0;
            }, 300);
        }
    }

    mouseDragEnd(e) {
        console.log(e);
    }

    initPerson(e, index) {
        this.currantSlide = index;
        this.startPosition = {
            x: e.clientX - e.offsetX,
            y: e.clientY - e.offsetY,
            height: e.target.clientHeight,
            width: e.target.clientWidth
        };
        this.clickedElement = e.target.children[0];
        this.clickedElement.style.display = 'none';
        setTimeout(() => {
            this.teamService.initIndex(index);
            document.getElementById('detail-view').classList.add('animation-start');
            this.startAnimation(index);
        }, 50);
    }

    startAnimation(index) {
        setTimeout(() => {
            this.startPosition = {
                x: 0,
                y: 0,
                height: window.innerHeight,
                width: window.innerWidth / 2
            };
            document.getElementById(`big-slide-${index}`).style.zIndex = '10';
            document.getElementById(`big-slide-${index}`).style.opacity = '1';
            document.getElementById('team-wrapper').style.opacity = '0';
            document.getElementById('curtain').style.height = '100vh';
            const imageWrappers = document.querySelectorAll('.team-detail .image-wrapper');
            const images = document.querySelectorAll('.team-detail .image-wrapper img');
            const infoWrapper = document.querySelectorAll('.team-detail .info-wrapper');
            const personTextContent = document.querySelectorAll('.team-detail .person-text-content');

            for (let i = 0; i < imageWrappers.length; i++) {
                imageWrappers[i].classList.add('height250');
                images[i].classList.add('height250');
                infoWrapper[i].classList.add('scale2');
                personTextContent[i].classList.add('show-content');
            }
        }, 20);
        setTimeout(() => {
            // document.getElementById('hidden-slider-over').style.display = 'block';
            // document.getElementById('detail-view').style.display = 'none';
        }, 2000);
    }

    nextSlid() {
        document.getElementById(`big-slide-${this.currantSlide}`).style.zIndex = '0';
        document.getElementById(`big-slide-${this.currantSlide}`).style.opacity = '0';

        this.currantSlide = this.currantSlide < this.team.length - 1 ? this.currantSlide + 1 : 0;
        console.log(this.currantSlide);

        document.getElementById(`big-slide-${this.currantSlide}`).style.zIndex = '10';
        document.getElementById(`big-slide-${this.currantSlide}`).style.opacity = '1';
    }

    prevSlide() {
        document.getElementById(`big-slide-${this.currantSlide}`).style.zIndex = '0';
        document.getElementById(`big-slide-${this.currantSlide}`).style.opacity = '0';

        this.currantSlide = this.currantSlide > 0 ? this.currantSlide - 1 : this.team.length - 1;
        console.log(this.currantSlide);

        document.getElementById(`big-slide-${this.currantSlide}`).style.zIndex = '10';
        document.getElementById(`big-slide-${this.currantSlide}`).style.opacity = '1';
    }
}


