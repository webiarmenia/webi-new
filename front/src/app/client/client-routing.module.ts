import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CabinetComponent} from './cabinet/cabinet.component';
import {AuthGuard} from './_guards/auth.guard';
import {LoginComponent} from './components/login/login.component';


const cabinetRoutes: Routes = [
    {path: '', redirectTo: 'projects', pathMatch: 'full'},
    {
        path: 'projects',
        loadChildren: () => import('./modules/projects/projects.module').then(m => m.ProjectsModule),
    },
    {
        path: 'payments',
        loadChildren: () => import('./modules/payments/payments.module').then(m => m.PaymentsModule),
    },
    {
        path: 'settings',
        loadChildren: () => import('./modules/settings/settings.module').then(m => m.SettingsModule),
    },
];

const routes: Routes = [
    {
        path: ':nik-name', canActivate: [AuthGuard], component: CabinetComponent, children: cabinetRoutes
    },
    {path: ':nik-name/login', component: LoginComponent},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ClientRoutingModule {

}
