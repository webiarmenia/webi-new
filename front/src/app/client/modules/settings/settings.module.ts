import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../../components/_modules';
import {SettingsComponent} from './settings.component';
import {SettingsRoutingModule} from './settings-routing.module';


@NgModule({
    declarations: [
        SettingsComponent
    ],
    imports: [
        CommonModule,
        SettingsRoutingModule,
        SharedModule,
    ]
})

export class SettingsModule {
}
