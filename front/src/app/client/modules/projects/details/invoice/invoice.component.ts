import {Component, OnInit} from '@angular/core';
import {ProjectService} from '../../../../_services/project.service';
import {InvoiceInfo} from '../../../../_models/InvoiceInfo';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
    selector: 'app-invoice',
    templateUrl: './invoice.component.html',
    styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {

    invoices: InvoiceInfo[];
    current: InvoiceInfo;
    noteForm;
    writeNote = false;

    constructor(
        private formBuilder: FormBuilder,
        private projectService: ProjectService,
    ) {
    }

    ngOnInit() {
        this.getInvoices();
        this.initNoteForm();
    }

    getInvoices() {
        if (this.projectService.invoices) {
            this.invoices = this.projectService.invoices;
            // console.log(this.invoices);
        } else {
            this.projectService.getInvoices().toPromise()
                .then(() => {
                    this.invoices = this.projectService.invoices;
                });
        }
    }

    showInvoice(index) {
        this.current = this.invoices[index];
        this.projectService.updateInvoice(this.current, {seen: true}).toPromise().then(
            () => this.invoices[index].seen = true,
            e => console.log(e)
        );
    }

    closeInvoice() {
        this.current = null;
        console.log(this.invoices);
    }

    closeNote() {
        this.writeNote = false;
        document.getElementById('invoice').style.overflow = 'auto';
    }

    openNoteRef() {
        document.getElementById('invoice').style.overflow = 'hidden';
        this.writeNote = true;
    }

    openNoteAcc() {
        document.getElementById('invoice').style.overflow = 'hidden';
        this.writeNote = true;
    }

    initNoteForm() {
        this.noteForm = this.formBuilder.group({
            note: [null, [Validators.required, Validators.minLength(50)]]
        });
    }

    resetForm() {
        this.noteForm.reset();
    }

    sendNote(form) {
        console.log(form.value);
    }
}
