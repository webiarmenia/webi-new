import {Component, OnInit} from '@angular/core';
import {ProjectService} from '../../../_services/project.service';
import {ActivatedRoute} from '@angular/router';
import {Project} from '../../../_models/Project';

@Component({
    selector: 'app-details',
    templateUrl: './details.component.html',
    styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

    project: Project;
    projectSlug;

    constructor(
        private route: ActivatedRoute,
        private projectService: ProjectService
    ) {
    }

    ngOnInit() {
        this.route.paramMap.subscribe(params => {
            this.projectSlug = params.get('slug');
        });
        this.getProject();
    }

    getProject() {
        if (this.projectService.projects) {
            this.project = this.projectService.getProjectById(this.projectSlug);
            console.log('if cont', this.project);
        } else {
            this.projectService.getProjects().toPromise().then(() => {
                this.project = this.projectService.getProjectById(this.projectSlug);
                console.log('else cont', this.project);
            });
        }
    }
}
