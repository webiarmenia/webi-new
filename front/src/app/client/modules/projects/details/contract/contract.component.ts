import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {InvoiceInfo} from '../../../../_models/InvoiceInfo';
import {ProjectService} from '../../../../_services/project.service';

@Component({
    selector: 'app-contract',
    templateUrl: './contract.component.html',
    styleUrls: ['./contract.component.scss']
})
export class ContractComponent implements OnInit {

    contracts;
    current;
    noteForm;
    writeNote = false;

    constructor(
        private formBuilder: FormBuilder,
        private projectService: ProjectService,
    ) {
    }

    ngOnInit() {
        this.getInvoices();
        this.initNoteForm();
    }

    getInvoices() {
        if (this.projectService.contracts) {
            this.contracts = this.projectService.contracts;
            console.log(this.contracts);
        } else {
            this.projectService.getContracts().toPromise()
                .then(() => {
                    this.contracts = this.projectService.contracts;
                });
        }
    }

    showInvoice(index) {
        this.current = this.contracts[index];
        this.projectService.updateContract(this.current, {seen: true}).toPromise().then(
            () => this.contracts[index].seen = true,
            e => console.log(e)
        );
    }

    closeInvoice() {
        this.current = null;
        console.log(this.contracts);
    }

    closeNote() {
        this.writeNote = false;
        document.getElementById('invoice').style.overflow = 'auto';
    }

    openNoteRef() {
        document.getElementById('invoice').style.overflow = 'hidden';
        this.writeNote = true;
    }

    openNoteAcc() {
        document.getElementById('invoice').style.overflow = 'hidden';
        this.writeNote = true;
    }

    initNoteForm() {
        this.noteForm = this.formBuilder.group({
            note: [null, [Validators.required, Validators.minLength(50)]]
        });
    }

    resetForm() {
        this.noteForm.reset();
    }

    sendNote(form) {
        console.log(form.value);
    }
}
