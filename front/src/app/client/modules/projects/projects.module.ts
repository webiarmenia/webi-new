import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProjectsRoutingModule} from './projects-routing.module';
import {SharedModule} from '../../../components/_modules';
import {ProjectsComponent} from './projects.component';
import {DetailsComponent} from './details/details.component';
import { InvoiceComponent } from './details/invoice/invoice.component';
import { ContractComponent } from './details/contract/contract.component';
import { PaymentsComponent } from './details/payments/payments.component';
import { HistoryComponent } from './details/history/history.component';


@NgModule({
    declarations: [
        ProjectsComponent,
        DetailsComponent,
        InvoiceComponent,
        ContractComponent,
        PaymentsComponent,
        HistoryComponent
    ],
    imports: [
        CommonModule,
        ProjectsRoutingModule,
        SharedModule,
    ]
})
export class ProjectsModule {
}
