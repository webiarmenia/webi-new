import {NgModule} from '@angular/core';
import {Routes, RouterModule, Router} from '@angular/router';
import {ProjectsComponent} from './projects.component';
import {DetailsComponent} from './details/details.component';
import {InvoiceComponent} from './details/invoice/invoice.component';
import {ContractComponent} from './details/contract/contract.component';
import {PaymentsComponent} from './details/payments/payments.component';
import {HistoryComponent} from './details/history/history.component';

const childrenRoutes: Routes = [
    {path: 'invoices', component: InvoiceComponent},
    {path: 'contracts', component: ContractComponent},
    {path: 'payments', component: PaymentsComponent},
    {path: 'history', component: HistoryComponent},
];

const routes: Routes = [
    {path: '', component: ProjectsComponent},
    {path: ':slug', component: DetailsComponent, children: childrenRoutes},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ProjectsRoutingModule {

}
