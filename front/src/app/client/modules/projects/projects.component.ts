import {Component, OnInit} from '@angular/core';
import {ProjectService} from '../../_services/project.service';
import {Project} from '../../_models/Project';

@Component({
    selector: 'app-projects',
    templateUrl: './projects.component.html',
    styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

    projects: Project[];
    notes: [];

    constructor(
        private projectService: ProjectService
    ) {
    }

    ngOnInit() {
        this.getProjects();
    }

    getProjects() {
        if (this.projectService.projects) {
            this.projects = this.projectService.projects;
        } else {
            this.projectService.getProjects().subscribe(
                (d: Project[]) => {
                    console.log('else proj');
                    this.projects = d;
                },
                e => console.log(e.status));
        }
    }
}
