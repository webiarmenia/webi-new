import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PaymentsComponent} from './payments.component';
import {SharedModule} from '../../../components/_modules';
import {PaymentsRoutingModule} from './payments-routing.module';


@NgModule({
    declarations: [
        PaymentsComponent
    ],
    imports: [
        CommonModule,
        PaymentsRoutingModule,
        SharedModule,
    ]
})
export class PaymentsModule {
}
