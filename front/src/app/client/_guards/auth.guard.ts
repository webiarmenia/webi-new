import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {LoginService} from '../_services/login.service';
import * as decode from 'jwt-decode';
import {UserService} from '../_services/user.service';


console.log('auth guard client');

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private logService: LoginService,
        private userService: UserService
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        const token = localStorage.getItem('client-token');

        if (token) {
            const userInfo = decode(token);
            this.userService.setClient({
                id: userInfo.id,
                firstName: userInfo.firstName,
                lastName: userInfo.lastName,
                email: userInfo.email,
                nikName: userInfo.nikName,
                status: userInfo.status
            });
            return true;
        } else {
            console.log('auth guard client false');
            this.logService.returnUrl = state.url;
            console.log('erfewrfe', state.url);
            const nik = state.url.split('/')[2]
            console.log(nik);
            this.router.navigate(['cabinet/' + nik + '/login']).then();
            return false;
        }
    }


}


// canActivate(
//     next: ActivatedRouteSnapshot,
//     state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
//     console.log(state.url);
//     return true;
//
// }
