
export interface InvoiceInfo {
    id: string;
    project: string;
    invoicePdf: string;
    clientAccepted: boolean;
    refused: boolean;
    clientNotes: string;
    seen: boolean;
    hasUpdate: boolean;
    created: Date;
    updated: Date;
}
