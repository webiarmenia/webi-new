export interface Client {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    nikName: string;
    status: string;
}
