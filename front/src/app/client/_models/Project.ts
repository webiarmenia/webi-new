export interface Project {
    id: string;
    slug: string;
    name: string;
    description: string;
    created: Date;
    updated: Date;
    notice: number;
}
