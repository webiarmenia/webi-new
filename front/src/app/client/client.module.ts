import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ClientRoutingModule} from './client-routing.module';
import {CabinetComponent} from './cabinet/cabinet.component';
import {LoginComponent} from './components/login/login.component';
import {TopMenuClientComponent} from './components/top-menu-client/top-menu-client.component';
import {SharedModule} from '../components/_modules';


@NgModule({
    declarations: [
        CabinetComponent,
        LoginComponent,
        TopMenuClientComponent,
    ],
    imports: [
        CommonModule,
        ClientRoutingModule,
        SharedModule,
    ]
})
export class ClientModule {
}
