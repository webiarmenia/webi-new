import {Component, OnInit} from '@angular/core';
import {DataService} from '../_services/data.service';
import {Project} from '../_models/Project';
import {ProjectService} from '../_services/project.service';

@Component({
    selector: 'app-cabinet',
    templateUrl: './cabinet.component.html',
    styleUrls: ['./cabinet.component.scss']
})
export class CabinetComponent implements OnInit {

    constructor(
        private projectService: ProjectService
    ) {
    }

    dropMenu = true;
    projectsNotice: number;

    ngOnInit() {
        this.getProjectsNotice();
    }

    getProjectsNotice() {
        if (this.projectService.projects) {
            this.projectsNotice = this.projectService.getNotice();
        } else {
            this.projectService.getProjects().subscribe(
                () => {
                    this.projectsNotice = this.projectService.getNotice();
                },
                e => console.log(e.status));
        }
    }
}
