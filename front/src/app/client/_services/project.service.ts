import {Injectable} from '@angular/core';
import {Globals} from '../../app.globals';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {UserService} from './user.service';
import {Client} from '../_models/Client';
import {Project} from '../_models/Project';
import {map, catchError} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {InvoiceInfo} from '../_models/InvoiceInfo';

@Injectable({
    providedIn: 'root'
})
export class ProjectService {

    queryUrl;
    client: Client;
    projects: Project[];
    currentProject: Project;
    invoices: InvoiceInfo[];
    contracts;

    constructor(
        private userService: UserService,
        private http: HttpClient,
        private globals: Globals
    ) {
        this.queryUrl = globals.clientUrl;
        this.client = this.userService.getClient();
    }

    getProjects(): Observable<Project[]> {
        return this.http.get(this.queryUrl + 'projects/' + this.client.id)
            .pipe(
                map((data: Project[]) => {
                        this.projects = data;
                        return this.projects;
                    },
                    catchError((error: HttpErrorResponse) => {
                        return throwError('errorMessage');
                    }))
            );
    }

    getProjectById(slug): Project {
        if (this.projects) {
            this.currentProject = this.projects.filter(pr => pr.slug === slug)[0];
            return this.currentProject;
        } else {
            return null;
        }
    }

    getInvoices() {
        return this.http.get(this.queryUrl + 'projects/invoices/' + this.currentProject.id)
            .pipe(
                map((data: any) => {
                        this.invoices = data;
                        // console.log('service', this.invoices);
                        return this.invoices;
                    },
                    catchError((error: HttpErrorResponse) => {
                        return throwError('errorMessage');
                    }))
            );
    }

    getContracts() {
        return this.http.get(this.queryUrl + 'projects/contracts/' + this.currentProject.id)
            .pipe(
                map((data: any) => {
                        this.contracts = data;
                        // console.log('service', this.invoices);
                        return this.contracts;
                    },
                    catchError((error: HttpErrorResponse) => {
                        return throwError('errorMessage');
                    }))
            );
    }

    updateInvoice(inv, body) {
        return this.http.post(this.queryUrl + 'projects/update-invoice/' + inv.id, body);
    }

    updateContract(cont, body) {
        return this.http.post(this.queryUrl + 'projects/update-contract/' + cont.id, body);
    }

    getNotice(): number {
        let notice = 0;
        this.projects.map(project => {
            if (project.notice > 0) {
                notice++;
            }
        });
        return notice;
    }
}
