import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class InterceptorService {

    constructor() {
    }

    intercept(req: HttpRequest<any>,
              next: HttpHandler): Observable<HttpEvent<any>> {
        const idToken = localStorage.getItem('client-token');
        if (idToken) {
            const cloned = req.clone({
                headers: req.headers.set('Authorization', 'Bearer ' + idToken)
            });
            return next.handle(cloned)
                .pipe(
                    retry(1),
                    catchError((error: HttpErrorResponse) => {
                        const errorMessage = '';
                        if (error.status === 401) {
                            localStorage.clear();
                            window.location.href = '/cabinet/login';
                        }
                        return throwError(errorMessage);
                    })
                );
        } else {
            return next.handle(req);
        }
    }
}
