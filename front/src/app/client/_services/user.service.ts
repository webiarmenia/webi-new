import {Injectable} from '@angular/core';
import {Globals} from '../../app.globals';
import {HttpClient} from '@angular/common/http';
import {Client} from '../_models/Client';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    queryUrl;
    client: Client;

    constructor(
        private http: HttpClient,
        private globals: Globals
    ) {
        this.queryUrl = globals.clientUrl;
    }

    setClient(client: Client) {
        this.client = client;
    }

    getClient() {
        return this.client;
    }

    getEmail() {
        return this.client.email;
    }
}
