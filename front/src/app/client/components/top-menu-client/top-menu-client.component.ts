import {Component, HostListener, OnInit} from '@angular/core';
import {LoginService} from '../../_services/login.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../_services/user.service';
import {Client} from '../../_models/Client';
import {AlertService} from "../../../components/_services/alert.service";

@Component({
    selector: 'app-top-menu-client',
    templateUrl: './top-menu-client.component.html',
    styleUrls: ['./top-menu-client.component.scss']
})
export class TopMenuClientComponent implements OnInit {
    myForm: FormGroup;
    dropMenu = false;
    client: Client;
    passHidden = true;

    @HostListener('document:click', ['$event'])
    documentClick(event) {
        event.target.getAttribute('id') === 'cabinetToggle' ?  this.toggleMenu() : this.dropMenu = false;
    }

    constructor(
        private alert: AlertService,
        private userService: UserService,
        private service: LoginService,
        private formBuilder: FormBuilder,
        private router: Router,
    ) {
    }

    ngOnInit() {
        this.client = this.userService.getClient();
        this.myForm = this.formBuilder.group({
            oldPassword: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    toggleMenu() {
       this.dropMenu = !this.dropMenu;
    }

    logout() {
        LoginService.logout();
        this.router.navigate(['/']);
    }

    openForm() {
        this.dropMenu = true;
        this.passHidden = !this.passHidden;
    }

    changePass() {
        this.alert.alert_loading('show');
        this.service.changePass(this.myForm.value).subscribe(
            d => {
                if (d) {
                    this.myForm.reset();
                    this.passHidden = true;
                    this.alert.success('success', 'Password changed');
                    this.alert.alert_loading('close');
                }
            },
            e => {
                this.alert.alert_loading('close');
                this.alert.error('error', e);
                console.log(e);
            }
        );
    }
}
