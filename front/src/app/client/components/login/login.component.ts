import {Component, OnInit} from '@angular/core';
import {FormGroup, Validators, FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {LoginService} from '../../_services/login.service';
import * as decode from 'jwt-decode';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    myForm: FormGroup;
    returnUrl: string;
    nikName;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private service: LoginService
    ) {
        this.nikName = this.route.snapshot.params['nik-name'];
        this.service.returnUrl = this.service.returnUrl ? this.service.returnUrl : 'cabinet/' + this.nikName;
        this.returnUrl = this.service.returnUrl;
    }

    ngOnInit() {
        this.myForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });
    }

    submit() {
        this.myForm.value.nikName = this.nikName;
        this.service.login(this.myForm.value).subscribe(
            (data: any) => {
                if (data.success) {
                    console.log(this.returnUrl);
                    localStorage.setItem('client-token', data.token);
                    this.router.navigate([this.returnUrl]).then();
                } else {
                    this.router.navigate([this.nikName + '/login']).then();
                }
            }, (err) => {
                localStorage.clear();
                this.router.navigate([this.nikName + '/login']);
            });
    }

}
