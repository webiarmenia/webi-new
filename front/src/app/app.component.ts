import {Component, HostListener} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {SettingService} from './components/_services/setting.service';
import {TeamService} from './components/_services/team.service';
import {PortfolioService} from './components/_services/portfolio.service';
import {ActionsService} from './components/_services/actions.service';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'webiFront';
    done = false;
    done2 = false;

    constructor(
        private translate: TranslateService,
        private settings: SettingService,
        private teamService: TeamService,
        private portfolioService: PortfolioService,
        private actionsService: ActionsService
    ) {
        settings.getAll().subscribe(
            d => {
                this.done = true;
            }
        );
        teamService.getAll().subscribe(
            d => {
                this.done2 = true;
            }
        );
        this.portfolioService.getAll().subscribe();
        translate.setDefaultLang('en');
        this.translate.currentLang = 'en';
    }

    @HostListener('window: resize')
    onResize() {
        if (window.innerWidth > 992) {
            const size = {
                width: window.innerWidth > 1920 ? 1920 : window.innerWidth,
                height: window.innerHeight,
                rate:
                    window.innerWidth >= 1920 ? 1 :
                        window.innerWidth < 1520 && window.innerWidth > 1220 ? 1.25 :
                            window.innerWidth < 1220 && window.innerWidth > 1020 ? 1.30 :
                                window.innerWidth < 1020 && window.innerWidth > 992 ? 1.35 : 1
            };
            this.actionsService.responsiveData.next(size);
            this.actionsService.mobileResponsiveData.next(992);
        } else {
            const width = window.innerWidth;
            this.actionsService.mobileResponsiveData.next(width);
        }
    }

}


