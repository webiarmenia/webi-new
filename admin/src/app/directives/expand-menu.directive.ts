import { Directive, HostBinding, HostListener, ElementRef, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[appExpandMenu]'
})
export class ExpandMenuDirective implements AfterViewInit {
  elements = [];

  constructor(private elem: ElementRef) {
  }

  @HostBinding('class.active') isOpen = false;
  @HostBinding('class.not-active') isClose = true;
  @HostListener('click', ['$event']) toggleOpen(e) {
    if (e.target.className === 'active-link') {
      return null;
    }
    if (this.elem.nativeElement.className === 'sidebar-list-item not-active') {
      this.elements.forEach(el => {
        el.className = 'sidebar-list-item not-active';
      });
      this.elem.nativeElement.className = 'sidebar-list-item active';
    } else if (this.elem.nativeElement.className === 'sidebar-list-item active') {
      this.elem.nativeElement.className = 'sidebar-list-item not-active';
    }
  }

  ngAfterViewInit() {
    this.elements = this.elem.nativeElement.offsetParent.querySelectorAll('.sidebar-list-item');
  }

}
