export class Advantage {
  _id: String;
  title: object;
  description: object;
  order: Number;
}
