export interface FAQ {
    _id: String;
    shortDescription: {
        am: string,
        ru: string,
        en: string
    };
    talk: Array<Object> | null;
}
