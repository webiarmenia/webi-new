export interface Invoice {
  id: string;
  fromName: string;
  fromEmail: string;
  fromAddress: string;
  fromPhone: string;
  fromBusiness: string;
  toName: string;
  toEmail: string;
  toAddress: string;
  toPhone: string;
  number: string;
  date: string;
  due: string;
  notes: string;
  total: number;
  balance: number;
  currency: string;
  hasFee: boolean;
  details: [Object];
  fee: {
    name: string;
    value: number;
    type: number;
  };
}
