import {Invoice} from './Invoice';

export interface InvoiceInfo {
    id: string;
    projectId: string;
    clientAccepted: boolean;
    clientNotes: string;
    seen: boolean;
    invoicePdf: string;
    invoice: Invoice;
    hasUpdate: boolean;
    refused: boolean;
}
