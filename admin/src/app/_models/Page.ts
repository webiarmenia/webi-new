export class Page {
  _id: String;
  slug : String;
  enDescription: String;
  ruDescription: String;
  amDescription: String;
  enTitle: String;
  ruTitle: String;
  amTitle: String;
  // enContent: String;
  // ruContent: String;
  // amContent: String;
  // banner: String;
  components: [{}];
  random: String;
}
