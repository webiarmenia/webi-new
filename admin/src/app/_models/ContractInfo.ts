import {Contract} from './Contract';

export interface ContractInfo {
  id: string;
  projectId: string;
  accepted: boolean;
  notes: string;
  seen: boolean;
  pdf: string;
  contract: Contract;
  hasUpdate: boolean;
  created: Date;
  updated: Date;
}
