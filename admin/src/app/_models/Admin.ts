export class Admin {
    id: String;
    firstName: String;
    lastName: String;
    email: String;
    role: String;
    permissions: String;
    avatar: String;
}
