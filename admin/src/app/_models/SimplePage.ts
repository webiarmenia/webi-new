export interface SimplePage {
    _id: String;
    key: String;
    slug: String;
    shortDescription: {
        am: string,
        ru: string,
        en: string
    };
    title: {
        am: string,
        ru: string,
        en: string
    };
    content: Object;
    dirName: string;
}
