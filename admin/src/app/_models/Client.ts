export interface Client {
  id: string;
  status: string;
  hasPassword: boolean;
  companyName: string;
  firstName: string;
  lastName: string;
  nikName: string;
  email: string;
}
