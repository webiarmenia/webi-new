export interface Project {

  id: string;
  slug: string;
  created: Date;
  updated: Date;
  name: string;
  description: string;
  priority: number;

}
