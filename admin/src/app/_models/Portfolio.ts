export class Portfolio {
  _id: String;
  url: String;
  slug: String;
  // enDescription: String;
  // ruDescription: String;
  // amDescription: String;

  enShortDescription: String;
  ruShortDescription: String;
  amShortDescription: String;
  enTitle: String;
  ruTitle: String;
  amTitle: String;
  image: String;
  random: String;
  sliderPosition: String;
  details: [{}];
}
