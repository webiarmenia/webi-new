import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './components/pages/login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {InterceptorService} from './_services/interceptor.service';
import {NgSelectModule} from '@ng-select/ng-select';
import {DashboardModule} from './components/pages/dashboard/dashboard.module';
import {Globals} from './app.globals';
import {FullNameFilterPipe} from './_pipes/full-name-filter.pipe';
import {TitleDescriptionFilterPipe} from './_pipes/title-description-filter.pipe';

import {ColorPickerModule} from 'ngx-color-picker';
import {HeaderComponent} from './components/partials/header/header.component';
import {MenuComponent} from './components/partials/menu/menu.component';
import {FooterComponent} from './components/partials/footer/footer.component';
import {ExpandMenuDirective} from './directives/expand-menu.directive';
import { AdvantageCreateComponent } from './components/pages/advantage/advantage-create/advantage-create.component';
import { AdvantageEditComponent } from './components/pages/advantage/advantage-edit/advantage-edit.component';
import { AdvantageListComponent } from './components/pages/advantage/advantage-list/advantage-list.component';
import { AdvantageViewComponent } from './components/pages/advantage/advantage-view/advantage-view.component';
import { AdminToolsComponent } from './components/pages/admin/admin-tools.component';


@NgModule({
  declarations: [
    FullNameFilterPipe,
    TitleDescriptionFilterPipe,
    AppComponent,
    LoginComponent,
    // HeaderComponent,
    // MenuComponent,
    // FooterComponent,
    // ExpandMenuDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DashboardModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgSelectModule,
    ColorPickerModule
  ],
  providers: [
    Globals,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
