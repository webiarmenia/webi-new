import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../app.globals';
import {Client} from '../_models/Client';

@Injectable({
    providedIn: 'root'
})
export class ClientService {

    url;
    updateClient: Client;

    constructor(private http: HttpClient, globals: Globals) {
        this.url = globals.queryUrl + 'admin/client';
    }

    getClients() {
        return this.http.get(this.url);
    }

    addAccount(client) {
        return this.http.post(this.url, client);
    }

    updateAccount(id, client) {
        console.log(id);
        return this.http.put(this.url + '/' + id, client);
    }

    getOne(nikName) {
        return this.http.get(this.url + '/' + nikName);

    }

    sendPass(id) {
        return this.http.get(this.url + '/password/' + id);
    }
}
