import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../app.globals';
import {Invoice} from '../_models/Invoice';
import {InvoiceInfo} from '../_models/InvoiceInfo';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  url;
  project;
  invoice: Invoice;
  invoiceInfo: InvoiceInfo;

  constructor(private http: HttpClient, globals: Globals) {
    this.url = globals.queryUrl + 'admin/client/invoice';
  }

  getInvoices(projectId) {
    return this.http.get(`${this.url}/${projectId}`);
  }

  addInvoice(invoice, projectId) {
    return this.http.post(`${this.url}/${projectId}`, invoice);
  }

  updateInvoice(id, invoice) {
    return this.http.put(this.url + '/' + id, invoice);
  }

  updateInvoicePDF(id, invoice) {
    return this.http.put(this.url + '/pdf/' + id, invoice);
  }


  getOne(id) {
    return this.http.get(this.url + '/one/' + id);
  }

  sendPDF(file) {
    return this.http.post(`${this.url}/mail`, file);
  }

}
