import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Globals} from '../app.globals';

@Injectable({
    providedIn: 'root'
})
export class EditorService {
    url;

    constructor(
        private http: HttpClient,
        private global: Globals
    ) {
        this.url = global.queryUrl + 'admin/';

    }

    sendData(data, type) {
        return this.http.post(this.url + 'editor/' + type, data);
    }

    deleteData(arr) {

        console.log(arr)

        const httpOptions = {
            headers: new HttpHeaders({'Content-Type': 'application/json'}),
            body: arr
        };

        return this.http.delete(this.url + 'editor', httpOptions);
    }

}
