import {Injectable} from '@angular/core';
import {Admin} from '../_models/Admin';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../app.globals';

@Injectable({
    providedIn: 'root'
})
export class AdminService {

    url;
    admin: Admin;

    updateAdmin: Admin;

    constructor(private http: HttpClient, globals: Globals) {
        this.url = globals.queryUrl + 'admin/';
    }

    getAdmins() {
        return this.http.get(this.url);
    }

    addAdmin(admin) {
        return this.http.post(this.url + 'create', admin);
    }

    update(id, client) {
        return this.http.put(this.url + 'update/' + id, client);
    }

    getOne(id) {
        return this.http.get(this.url + 'one/' + id);
    }

    setAdmin(admin: Admin) {
        this.admin = admin;
    }

    getAdmin() {
        return this.admin
    }

    canCreate() {
        return !!this.admin.permissions['can_create'];
    }

    canUpdate() {
        return !!this.admin.permissions['can_update'];
    }

    canDelete() {
        return !!this.admin.permissions['can_delete'];
    }

}
