import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StylesService {
  dropdown = false;
  collapse = false;
  collapseSubject = new Subject<any>();
  dropdownSubject = new Subject<any>();

  constructor() { }

  getCollapse() {
    return this.collapseSubject.asObservable();
  }

  setCollase() {
    this.collapse = !this.collapse;
    this.collapseSubject.next(this.collapse);
  }

}
