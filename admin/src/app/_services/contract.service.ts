import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../app.globals';
import {Contract} from '../_models/Contract';
import {ContractInfo} from '../_models/ContractInfo';

@Injectable({
  providedIn: 'root'
})
export class ContractService {

  url;
  project;
  contract: Contract;
  contractInfo: ContractInfo;

  constructor(private http: HttpClient, globals: Globals) {
    this.url = globals.queryUrl + 'admin/client/contract';
  }

  getContracts(projectId) {
    return this.http.get(`${this.url}/${projectId}`);
  }

  addContract(contract, projectId) {
    return this.http.post(`${this.url}/${projectId}`, contract);
  }

  updateContract(id, contract) {
    return this.http.put(this.url + '/' + id, contract);
  }

  updateContractPDF(id, contract) {
    return this.http.put(this.url + '/pdf/' + id, contract);
  }

  getOne(id) {
    return this.http.get(this.url + '/one/' + id);
  }

  sendPDF(file) {
    return this.http.post(`${this.url}/mail`, file);
  }
}
