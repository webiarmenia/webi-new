import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../app.globals';
import {Project} from '../_models/Project';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  url;
  project: Project;
  projects: Project[];

  constructor(private http: HttpClient, globals: Globals) {
    this.url = globals.queryUrl + 'admin/client/project';
  }

  getProjects(clientId) {
    return this.http.get(`${this.url}/${clientId}`);
  }

  addProject(project) {
    console.log(project)
    return this.http.post(this.url, project);
  }

  updateProject(id, project) {
    return this.http.put(this.url + '/' + id, project);
  }

  getOne(id) {
    return this.http.get(this.url + '/one/'  + id);

  }

}
