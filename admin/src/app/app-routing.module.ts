import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './components/pages/login/login.component';
import {AuthGuard} from './_guards/auth.guard';


const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: '',  redirectTo: '/admin/portfolio', pathMatch: 'full'},
  // {
  //   path: '', loadChildren: './components/pages/dashboard/dashboard.module#DashboardModule'
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
