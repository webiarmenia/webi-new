import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs';
import * as decode from 'jwt-decode';
import {Admin} from '../_models/Admin';
import {AdminService} from '../_services/admin.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(private router: Router, private adminService: AdminService) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (localStorage.getItem('jwt_token')) {
            console.log('Guard');
            const tokenPayload = decode(localStorage.getItem('jwt_token'));
            this.adminService.setAdmin(tokenPayload)
            return true;
        }
        this.router.navigate(['login']).then();
    }
}
