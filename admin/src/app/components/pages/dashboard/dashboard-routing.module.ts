import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {DashboardComponent} from './dashboard.component';
import {AuthGuard} from '../../../_guards/auth.guard';
import {AdminGuard} from '../../../_guards/admin.guard';

const routes: Routes = [
    {
        path: 'admin', canActivate: [AuthGuard], component: DashboardComponent, children: [
            {path: 'setting', loadChildren: () => import('../setting/setting.module').then(m => m.SettingModule)},
            {path: 'category', loadChildren: () => import('../category/category.module').then(m => m.CategoryModule)},
            {path: 'language', loadChildren: () => import('../language/language.module').then(m => m.LanguageModule)},
            {path: 'media', loadChildren: () => import('../media/media.module').then(m => m.MediaModule)},
            {path: 'menu', loadChildren: () => import('../menu/menu.module').then(m => m.MenuModule)},
            {
                path: 'portfolio',
                loadChildren: () => import('../portfolio/portfolio.module').then(m => m.PortfolioModule)
            },
            {path: 'page', loadChildren: () => import('../page/page.module').then(m => m.PageModule)},
            {path: 'service', loadChildren: () => import('../page/service/service.module').then(m => m.ServiceModule)},
            {path: 'simple', loadChildren: () => import('../page/simple/simple.module').then(m => m.SimpleModule)},
            {path: 'faq', loadChildren: () => import('../page/faq/faq.module').then(m => m.FaqModule)},
            {path: 'news', loadChildren: () => import('../news/news.module').then(m => m.NewsModule)},
            {path: 'team', loadChildren: () => import('../team/team.module').then(m => m.TeamModule)},
            {
                path: 'client',
                canActivate: [AdminGuard],
                loadChildren: () => import('../client/client.module').then(m => m.ClientModule)
            },
            {path: 'card', loadChildren: () => import('../card/card.module').then(m => m.CardModule)},
            {
                path: 'advantage',
                loadChildren: () => import('../advantage/advantage.module').then(m => m.AdvantageModule)
            },
            {
                path: 'admin-tools',
                canActivate: [AdminGuard],
                loadChildren: () => import('../admin/admin-tools.module').then(m => m.AdminToolsModule)
            },
        ]
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule {
}
