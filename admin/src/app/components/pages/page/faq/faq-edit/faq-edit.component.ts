import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SimplePage} from '../../../../../_models/SimplePage';
import {FAQ} from '../../../../../_models/FAQ';
import {DataService} from '../../../../../_services/data.service';
import {ItemService} from '../../../../../_services/item.service';
import {Router} from '@angular/router';
import {AdminService} from '../../../../../_services/admin.service';
import {Globals} from '../../../../../app.globals';

@Component({
    selector: 'app-faq-edit',
    templateUrl: './faq-edit.component.html',
    styleUrls: ['./faq-edit.component.css']
})
export class FaqEditComponent implements OnInit {
    faqForm: FormGroup;
    faq: FAQ;
    language: String = 'en';
    slugs = [];
    url;
    delete: any;

    constructor(
        private fb: FormBuilder,
        private dataService: DataService,
        private itemService: ItemService,
        private router: Router,
        private adminService: AdminService,
        private globals: Globals,
    ) {

        this.url = globals.queryUrl;
        if (this.itemService.faq) {
            this.faq = this.itemService.faq;
        }
    }

    ngOnInit() {
        this.faqForm = this.fb.group({
            amShortDescription: [this.faq ? this.faq['shortDescription'].am : null],
            ruShortDescription: [this.faq ? this.faq['shortDescription'].ru : null],
            enShortDescription: [this.faq ? this.faq['shortDescription'].en : null],
            talk: this.fb.array(this.faq ? this.createTalkForm() : [])
        });
    }

    myFAQ() {

        const shortDescription: {} = {
            am: this.faqForm.get('amShortDescription').value,
            ru: this.faqForm.get('ruShortDescription').value,
            en: this.faqForm.get('enShortDescription').value
        };

        const form = {
            shortDescription: shortDescription,
            talk: this.faqForm.get('talk').value
        };

        if (this.faq) {
            this.dataService.updateData(form, 'faq', this.faq._id).subscribe(data => {

                this.router.navigate(['admin/faq']).then();
            }, e => console.log(e));
        } else {
            this.dataService.sendData(form, 'faq').subscribe(data => {

                this.router.navigate(['admin/faq']).then();
            }, e => console.log(e));
        }

    }

    addTools() {
        const type = this.faqForm.controls.talk as FormArray;
        type.push(this.fb.group({
            amAnswer: null,
            ruAnswer: null,
            enAnswer: [null, Validators.required],
            amQuestion: null,
            ruQuestion: null,
            enQuestion: [null, Validators.required],
            order: 0,
            show: false
        }));

    }

    createTalkForm() {
        setTimeout(() => {
            const type = this.faqForm.controls.talk as FormArray;

            this.faq.talk.forEach((t, i) => {

                type.push(this.fb.group({
                    amAnswer: t['amAnswer'],
                    ruAnswer: t['ruAnswer'],
                    enAnswer: [t['enAnswer'], Validators.required],
                    amQuestion: t['amQuestion'],
                    ruQuestion: t['ruQuestion'],
                    enQuestion: [t['enQuestion'], Validators.required],
                    order: t['order'],
                    show: t['show']
                }));
            });
        })

        return [];
    }


    changeLanguage(language) {
        this.language = language;
    }


    removeFormEl(i) {
        this.delete = confirm('Are you want to delete?');
        const type = this.faqForm.controls.talk as FormArray;
        if (this.delete) {
            type.removeAt(i);
        }
    }

}
