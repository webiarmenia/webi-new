import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FaqListComponent} from './faq-list/faq-list.component';
import {FaqEditComponent} from './faq-edit/faq-edit.component';




const routes: Routes = [
    {path: '', component: FaqListComponent},
    {path: 'edit', component: FaqEditComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FaqRoutingModule {
}
