import {Component, OnInit} from '@angular/core';
import {DataService} from '../../../../../_services/data.service';
import {Router} from '@angular/router';
import {ItemService} from '../../../../../_services/item.service';
import {AdminService} from '../../../../../_services/admin.service';
import {FAQ} from '../../../../../_models/FAQ';

@Component({
    selector: 'app-faq-list',
    templateUrl: './faq-list.component.html',
    styleUrls: ['./faq-list.component.css']
})
export class FaqListComponent implements OnInit {

    faqs: FAQ[] = [];
    delete: any;

    constructor(
        private dataService: DataService,
        private router: Router,
        private itemService: ItemService,
        private adminService: AdminService) {
    }

    ngOnInit() {
        this.dataService.getData('faq').subscribe(data => {
            this.faqs = data['faqs'];
            console.log(this.faqs);
        }, (err) => {
            if (err.status === 401) {
                localStorage.clear();
                this.router.navigate(['login']);
            }
        });
    }


    deleteFAQ(faq, i) {
        this.delete = confirm('Are you want to delete?');
        if (this.delete === true) {
            this.dataService.delete('faq', faq._id).subscribe(data => {
                if (data['success']) {
                    this.faqs.splice(i, 1);
                } else {
                    this.router.navigate(['login']);
                }
            }, (err) => {
                console.log(err);
            });
        }
    }

    updateFAQ(faq) {
        this.itemService.faq = faq;
        this.router.navigate(['admin/faq/edit']);
    }

    create() {
        this.itemService.faq = null;
        this.router.navigate(['admin/faq/edit']);
    }

}
