import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FaqListComponent} from './faq-list/faq-list.component';
import {FaqEditComponent} from './faq-edit/faq-edit.component';
import {FaqRoutingModule} from './faq-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
    declarations: [FaqListComponent, FaqEditComponent],
    imports: [
        CommonModule,
        FaqRoutingModule,
        FormsModule,
        ReactiveFormsModule
    ]
})
export class FaqModule {
}
