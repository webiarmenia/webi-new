import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {NgxTreeDndModule} from 'ngx-tree-dnd';
import {ServiceEditComponent} from './service-edit/service-edit.component';
import {ServiceListComponent} from './service-list/service-list.component';
import {ServiceRoutingModule} from './service-routing.module';
import {ColorPickerModule} from 'ngx-color-picker';


@NgModule({
    declarations: [
        ServiceEditComponent,
        ServiceListComponent
    ],
    imports: [
        CommonModule,
        ServiceRoutingModule,
        FormsModule,
        ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
        NgxTreeDndModule,
        ColorPickerModule
    ],
})
export class ServiceModule {
}
