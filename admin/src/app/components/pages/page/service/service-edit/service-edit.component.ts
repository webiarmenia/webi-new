import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../../../../../_services/data.service';
import {ItemService} from '../../../../../_services/item.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-service-edit',
    templateUrl: './service-edit.component.html',
    styleUrls: ['./service-edit.component.css']
})
export class ServiceEditComponent implements OnInit {
    serviceForm: FormGroup;
    categories = ['eCommerce', 'Web', 'Android'];
    public imagePath;
    language: String = 'en';
    detailsView = false;
    imageWithTextPosition;
    grStart = '#e94141';
    grEnd = '#e4ce41';
    order = 1;
    inValidSlug = false;
    slugs = [];

    imageSize = [3, 4, 6, 8, 9];
    textSize = [1, 2, 3, 4, 5, 6, 7];
    imageHeights = [30, 50, 70];
    alignments = ['center', 'right', 'left'];
    verticalAlignments = ['center', 'top', 'bottom'];
    objectFits = ['cover', 'contain', 'none'];
    delete: any;
    cardsCount = 4;
    cardsLogoCount = 4;

    page;


    constructor(
        private fb: FormBuilder,
        private dataService: DataService,
        private itemService: ItemService,
        private router: Router
    ) {
        if (this.itemService.servicePage) {
            this.page = this.itemService.servicePage;
            this.grStart = this.page.grStart;
            this.grEnd = this.page.grEnd;
            console.log(this.page);
        }
    }


    ngOnInit() {
        this.serviceForm = this.fb.group({
            slug: [this.page ? this.page.slug : '', Validators.required],
            category: [this.page ? this.page.category : 'top', Validators.required],
            amTitle: [this.page ? this.page['title'].am : ''],
            ruTitle: [this.page ? this.page['title'].ru : ''],
            enTitle: [this.page ? this.page['title'].en : '', Validators.required],
            amDescription: [this.page ? this.page['description'].am : ''],
            ruDescription: [this.page ? this.page['description'].ru : ''],
            enDescription: [this.page ? this.page['description'].en : ''],
            imgURL: [this.page ? this.page.image : '', Validators.required],
            grStart: [this.page ? this.page.grStart : '#e94141', Validators.required],
            grEnd: [this.page ? this.page.grEnd : '#e4ce41', Validators.required],
            details: this.fb.array(this.page ? this.createDetailsForm() : [])
        });
    }

    grEndColor(e) {
        this.serviceForm.get('grEnd').setValue(e);
    }

    grStartColor(e) {
        this.serviceForm.get('grStart').setValue(e);
    }

    filterSlug(e) {
        this.inValidSlug = false;
        console.log(this.serviceForm);

        this.slugs.forEach(slug => {
            if (slug.toLowerCase() === e.target.value.toLowerCase()) {
                this.inValidSlug = true;
            }
        });
    }


    onFileChange(event) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            const reader = new FileReader();
            this.imagePath = file;
            reader.readAsDataURL(this.imagePath);
            reader.onload = () => {
                this.serviceForm.get('imgURL').setValue(reader.result);
            };
        }
    }


    cardsLogo(event, i, index) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            console.log(this.serviceForm.get('details')['controls'][i]['controls'].cards['controls'][index]['controls']);
            const reader = new FileReader();
            this.imagePath = file;
            reader.readAsDataURL(this.imagePath);
            reader.onload = () => {
                this.serviceForm.get('details')['controls'][i]['controls'].cards['controls'][index]['controls'].imgURL.setValue(reader.result);
                console.log(this.serviceForm.value);
            };
        }
    }

    imgWithTextWithLogo(event, i, index) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            const reader = new FileReader();
            this.imagePath = file;
            reader.readAsDataURL(this.imagePath);
            reader.onload = () => {
                this.serviceForm.get('details')['controls'][i]['controls'].textWithLogo['controls'][index]['controls'].imgURL.setValue(reader.result);
                console.log(this.serviceForm.value);
            };
        }
    }

    logosImg(event, i, index) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            // console.log(this.serviceForm.get('details')['controls'][i]['controls'].logos['controls'][index]['controls']);
            const reader = new FileReader();
            this.imagePath = file;
            reader.readAsDataURL(this.imagePath);
            reader.onload = () => {
                this.serviceForm.get('details')['controls'][i]['controls'].logos['controls'][index]['controls'].imgURL.setValue(reader.result);
                console.log(this.serviceForm.value);
            };
        }
    }

    blockImg(event, i, row, imgURL) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            const reader = new FileReader();
            this.imagePath = file;
            reader.readAsDataURL(this.imagePath);
            reader.onload = () => {
                this.serviceForm.get('details')['controls'][i]['controls'][row]['controls'][imgURL].setValue(reader.result);
                console.log(this.serviceForm.value);
            };
        }
    }

    changeLogo(id) {
        document.getElementById(id).click();
        console.log(this.serviceForm);
    }

    imageWithText(event, i) {

        if (event.target.files.length > 0) {
            const file = event.target.files[0];

            this.imageWithTextPosition = this.serviceForm.get('details')['controls'][i].get('imagePosition').value;

            const reader = new FileReader();
            this.imagePath = file;
            reader.readAsDataURL(this.imagePath);
            reader.onload = () => {
                this.serviceForm.get('details')['controls'][i].get('imgURL').setValue(reader.result);
                console.log(this.serviceForm.get('details')['controls'][i].value);
            };
        }
    }

    imageWithTextWithLogo(event, i) {

        if (event.target.files.length > 0) {
            const file = event.target.files[0];

            this.imageWithTextPosition = this.serviceForm.get('details')['controls'][i].get('imagePosition').value;

            const reader = new FileReader();
            this.imagePath = file;
            reader.readAsDataURL(this.imagePath);
            reader.onload = () => {
                this.serviceForm.get('details')['controls'][i].get('imgURL').setValue(reader.result);
                console.log(this.serviceForm.get('details')['controls'][i].value);
            };
        }
    }

    imageWithImage(event, i, name) {
        if (name === 'img1') {
            const file = event.target.files[0];
            const reader = new FileReader();
            this.imagePath = file;
            reader.readAsDataURL(this.imagePath);
            reader.onload = () => {
                this.serviceForm.get('details')['controls'][i]['controls'].img1['controls'].imgURL.setValue(reader.result);
                console.log(this.serviceForm.get('details').value);
            };
        } else {
            const file = event.target.files[0];
            const reader = new FileReader();
            this.imagePath = file;
            reader.readAsDataURL(this.imagePath);
            reader.onload = () => {
                this.serviceForm.get('details')['controls'][i]['controls'].img2['controls'].imgURL.setValue(reader.result);
            };
        }
    }

    addTools() {
        this.detailsView = !this.detailsView;
    }

    sliderImg(event, i, r, imgIndex) {

        const file = event.target.files[0];
        const reader = new FileReader();
        this.imagePath = file;
        reader.readAsDataURL(this.imagePath);
        reader.onload = () => {
            this.serviceForm.get('details')['controls'][i]['controls'].sliders['controls'][r]['controls'].imagesURL['controls'][imgIndex]['controls'].imgURL.setValue(reader.result);
            console.log(this.serviceForm.get('details').value);
        };

    }

    addTool(value) {
        this.detailsView = !this.detailsView;
        const type = this.serviceForm.controls.details as FormArray;
        if (value === 'imageWithText') {
            type.push(this.fb.group({
                type: 'imageWithText',
                textSize: '',
                imgURL: ['', Validators.required],
                imagePosition: 'left',
                imgSize: '3',
                imageOrder: '1',
                imageHeight: '30',
                alignment: 'center',
                verticalAlignment: 'center',
                objectFit: 'cover',
                text: this.fb.array([])
            }));
        } else if (value === 'imageWithTextWithLogo') {
            type.push(this.fb.group({
                type: 'imageWithTextWithLogo',
                textSize: '',
                imgURL: ['', Validators.required],
                imagePosition: 'left',
                imgSize: '3',
                imageOrder: '1',
                imageHeight: '30',
                alignment: 'center',
                verticalAlignment: 'center',
                objectFit: 'cover',
                textWithLogo: this.fb.array([])
            }));
        } else if (value === 'text') {
            type.push(this.fb.group({
                type: 'text',
                amTitle: '',
                ruTitle: '',
                enTitle: ['', Validators.required],
                amDescription: '',
                ruDescription: '',
                enDescription: ['', Validators.required],
                titleAlign: 'left',
                titleWidth: 12,
                descriptionAlign: 'left',

            }));
        } else if (value === 'imageWithImage') {
            type.push(this.fb.group({
                type: 'imageWithImage',
                imageHeight: '30',
                imagePosition: 'left',
                imgSize: '3',
                imageOrder: '1',
                img1: this.fb.group({
                    imgURL: ['', Validators.required],
                    alignment: 'center',
                    verticalAlignment: 'center',
                    objectFit: 'cover',
                }),
                img2: this.fb.group({
                    imgURL: ['', Validators.required],
                    alignment: 'center',
                    verticalAlignment: 'center',
                    objectFit: 'cover',
                })

            }));
        } else if (value === 'card') {
            type.push(this.fb.group({
                type: 'card',
                count: 3,
                backColor: false,
                hover: false,
                textAlign: 'left',
                cards: this.fb.array(this.createCardsForm())
            }));
        } else if (value === 'cardLogo') {
            type.push(this.fb.group({
                type: 'cardLogo',
                count: 3,
                size: 'small',
                backColor: false,
                hover: false,
                textAlign: 'left',
                cards: this.fb.array(this.createCardsLogoForm())
            }));
        } else if (value === 'block') {
            type.push(this.fb.group({
                type: 'block',
                backColor: false,
                objectFit: 'cover',
                alignment: 'center',
                verticalAlignment: 'center',
                row1: this.fb.group({
                    img1URL: ['', Validators.required],
                    img2URL: ['', Validators.required],
                    amTitle: '',
                    ruTitle1: '',
                    enTitle1: ['', Validators.required],
                    amDescription1: '',
                    ruDescription1: '',
                    enDescription1: ['', Validators.required],
                    amTitle2: '',
                    ruTitle2: '',
                    enTitle2: ['', Validators.required],
                    amDescription2: '',
                    ruDescription2: '',
                    enDescription2: ['', Validators.required],
                }),
                row2: this.fb.group({
                    img1URL: ['', Validators.required],
                    img2URL: ['', Validators.required],
                    amTitle1: '',
                    ruTitle1: '',
                    enTitle1: ['', Validators.required],
                    amDescription1: '',
                    ruDescription1: '',
                    enDescription1: ['', Validators.required],
                    amTitle2: '',
                    ruTitle2: '',
                    enTitle2: ['', Validators.required],
                    amDescription2: '',
                    ruDescription2: '',
                    enDescription2: ['', Validators.required],
                }),
            }));
        } else if (value === 'slider') {
            type.push(this.fb.group({
                type: 'slider',
                count: 1,
                sliders: this.fb.array(this.createSlidersForm())
            }));
        } else if (value === 'logo') {
            type.push(this.fb.group({
                type: 'logo',
                count: 3,
                imageHeight: '30',
                objectFit: 'cover',
                logos: this.fb.array(this.createLogosForm())
            }));
        }
    }

    createDetailsForm() {
        setTimeout(() => {
            const type = this.serviceForm.controls.details as FormArray;
            this.page.details.forEach((p, i) => {
                if (p['type'] === 'imageWithText') {
                    type.push(this.fb.group({
                        type: 'imageWithText',
                        textSize: p['textSize'],
                        imgURL: [p['imgURL'], Validators.required],
                        imagePosition: p['imagePosition'],
                        imgSize: p['imgSize'],
                        imageOrder: p['imageOrder'],
                        imageHeight: p['imageHeight'],
                        alignment: p['alignment'],
                        verticalAlignment: p['verticalAlignment'],
                        objectFit: p['objectFit'],
                        text: this.fb.array(this.createTextForm(i))
                    }));
                } else if (p['type'] === 'imageWithTextWithLogo') {
                    type.push(this.fb.group({
                        type: 'imageWithTextWithLogo',
                        textSize: p['textSize'],
                        imgURL: [p['imgURL'], Validators.required],
                        imagePosition: p['imagePosition'],
                        imgSize: p['imgSize'],
                        imageOrder: p['imageOrder'],
                        imageHeight: p['imageHeight'],
                        alignment: p['alignment'],
                        verticalAlignment: p['verticalAlignment'],
                        objectFit: p['objectFit'],
                        textWithLogo: this.fb.array(this.createTextFormWithLogo(i))
                    }));
                } else if (p['type'] === 'text') {
                    type.push(this.fb.group({
                        type: 'text',
                        amTitle: p['amTitle'],
                        ruTitle: p['ruTitle'],
                        enTitle: [p['enTitle'], Validators.required],
                        amDescription: p['amDescription'],
                        ruDescription: p['ruDescription'],
                        enDescription: [p['enDescription'], Validators.required],
                        titleAlign: p['titleAlign'],
                        titleWidth: p['titleWidth'],
                        descriptionAlign: p['descriptionAlign'],
                    }));
                } else if (p['type'] === 'imageWithImage') {
                    type.push(this.fb.group({
                        type: 'imageWithImage',
                        imageHeight: p['imageHeight'],
                        imagePosition: p['imagePosition'],
                        imgSize: p['imgSize'],
                        imageOrder: p['imageOrder'],
                        img1: this.fb.group({
                            imgURL: [p['img1']['imgURL'], Validators.required],
                            alignment: p['img1']['alignment'],
                            verticalAlignment: p['img1']['verticalAlignment'],
                            objectFit: p['img1']['objectFit'],
                        }),
                        img2: this.fb.group({
                            imgURL: [p['img2']['imgURL'], Validators.required],
                            alignment: p['img2']['alignment'],
                            verticalAlignment: p['img2']['verticalAlignment'],
                            objectFit: p['img2']['objectFit'],
                        })
                    }));
                } else if (p['type'] === 'block') {
                    type.push(this.fb.group({
                        type: 'block',
                        backColor: p['backColor'],
                        objectFit: p['objectFit'],
                        alignment: p['alignment'],
                        verticalAlignment: p['verticalAlignment'],
                        row1: this.fb.group({
                            img1URL: [p['row1']['img1URL'], Validators.required],
                            img2URL: [p['row1']['img2URL'], Validators.required],
                            amTitle1: p['row1']['amTitle1'],
                            ruTitle1: p['row1']['ruTitle1'],
                            enTitle1: [p['row1']['enTitle1'], Validators.required],
                            amDescription1: p['row1']['amDescription1'],
                            ruDescription1: p['row1']['ruDescription1'],
                            enDescription1: [p['row1']['enDescription1'], Validators.required],
                            amTitle2: p['row1']['amTitle2'],
                            ruTitle2: p['row1']['ruTitle2'],
                            enTitle2: [p['row1']['enTitle2'], Validators.required],
                            amDescription2: p['row1']['amDescription2'],
                            ruDescription2: p['row1']['ruDescription2'],
                            enDescription2: [p['row1']['enDescription2'], Validators.required],
                        }),
                        row2: this.fb.group({
                            img1URL: [p['row2']['img1URL'], Validators.required],
                            img2URL: [p['row2']['img2URL'], Validators.required],
                            amTitle1: p['row2']['amTitle1'],
                            ruTitle1: p['row2']['ruTitle1'],
                            enTitle1: [p['row2']['enTitle1'], Validators.required],
                            amDescription1: p['row2']['amDescription1'],
                            ruDescription1: p['row2']['ruDescription1'],
                            enDescription1: [p['row2']['enDescription1'], Validators.required],
                            amTitle2: p['row2']['amTitle2'],
                            ruTitle2: p['row2']['ruTitle2'],
                            enTitle2: [p['row2']['enTitle2'], Validators.required],
                            amDescription2: p['row2']['amDescription2'],
                            ruDescription2: p['row2']['ruDescription2'],
                            enDescription2: [p['row2']['enDescription2'], Validators.required],
                        }),
                    }));
                } else if (p['type'] === 'card') {
                    type.push(this.fb.group({
                        type: 'card',
                        count: p['count'],
                        backColor: p['backColor'],
                        hover: p['hover'],
                        textAlign: p['textAlign'],
                        cards: this.fb.array(this.createCardEditForm(i))
                    }));
                } else if (p['type'] === 'cardLogo') {
                    type.push(this.fb.group({
                        type: p['type'],
                        count: p['count'],
                        size: p['size'],
                        backColor: p['backColor'],
                        hover: p['hover'],
                        textAlign: p['textAlign'],
                        cards: this.fb.array(this.createCardsLogoEditForm(i))
                    }));
                } else if (p['type'] === 'logo') {
                    type.push(this.fb.group({
                        type: p['type'],
                        count: p['count'],
                        imageHeight: p['imageHeight'],
                        objectFit: p['objectFit'],
                        logos: this.fb.array(this.createLogosEditForm(i))
                    }));
                } else if (p['type'] === 'slider') {
                    type.push(this.fb.group({
                        type: 'slider',
                        count: p['count'],
                        sliders: this.fb.array(this.createSlidersEditForm(i))
                    }));
                }
            });
        });
        return [];
    }

    createTextForm(i) {
        setTimeout(() => {
            const count = this.serviceForm.controls.details['controls'][i].controls.text as FormArray;

            this.page.details[i].text.forEach((t) => {
                count.push(this.fb.group({
                    amTitle: [t['amTitle']],
                    ruTitle: [t['ruTitle']],
                    enTitle: [t['enTitle'], Validators.required],
                    amContent: [t['amContent']],
                    ruContent: [t['ruContent']],
                    enContent: [t['enContent'], Validators.required],
                }));
            });
        });
        return [];
    }

    createTextFormWithLogo(i) {
        setTimeout(() => {
            const count = this.serviceForm.controls.details['controls'][i].controls.textWithLogo as FormArray;

            this.page.details[i].textWithLogo.forEach((t) => {
                count.push(this.fb.group({
                    amTitle: [t['amTitle']],
                    ruTitle: [t['ruTitle']],
                    enTitle: [t['enTitle'], Validators.required],
                    amContent: [t['amContent']],
                    ruContent: [t['ruContent']],
                    enContent: [t['enContent'], Validators.required],
                    imgURL: [t['imgURL']]
                }));
            });
        });
        return [];
    }

    createCardEditForm(i) {
        setTimeout(() => {
            const count = this.serviceForm.controls.details['controls'][i].controls.cards as FormArray;

            this.page.details[i].cards.forEach((c) => {
                count.push(this.fb.group({
                    amTitle: [c['amTitle']],
                    ruTitle: [c['ruTitle']],
                    enTitle: [c['enTitle'], Validators.required],
                    amContent: [c['amContent']],
                    ruContent: [c['ruContent']],
                    enContent: [c['enContent'], Validators.required],
                }));
            });
        });
        return [];
    }

    createSlidersEditForm(i) {
        setTimeout(() => {
            const count = this.serviceForm.controls.details['controls'][i].controls.sliders as FormArray;

            console.log(count)

            this.page.details[i].sliders.forEach((c, index) => {
                count.push(this.fb.group({
                    imageHeight: c['imageHeight'],
                    alignment: c['alignment'],
                    verticalAlignment: c['verticalAlignment'],
                    objectFit: c['objectFit'],
                    watchList: c['watchList'],
                    imagesURL: this.fb.array(this.createSliderImagesEditForm(i, index))
                }));

            });
        });
        return [];
    }

    createLogosEditForm(i) {
        setTimeout(() => {
            const count = this.serviceForm.controls.details['controls'][i].controls.logos as FormArray;

            this.page.details[i].logos.forEach((c) => {
                count.push(this.fb.group({
                    imgURL: [c['imgURL']],
                }));

            });
        });
        return [];
    }

    createCardsLogoEditForm(i) {
        setTimeout(() => {
            const count = this.serviceForm.controls.details['controls'][i].controls.cards as FormArray;

            this.page.details[i].cards.forEach((c) => {
                count.push(this.fb.group({
                    imgURL: [c['imgURL']],
                    amTitle: [c['amTitle']],
                    ruTitle: [c['ruTitle']],
                    enTitle: [c['enTitle'], Validators.required],
                    amContent: [c['amContent']],
                    ruContent: [c['ruContent']],
                    enContent: [c['enContent'], Validators.required],
                }));
            });
        });
        return [];
    }

    createCardsForm() {
        setTimeout(() => {
            const length = this.serviceForm.controls.details['controls'].length;
            const count = this.serviceForm.controls.details['controls'][length - 1].controls.cards as FormArray;
            for (let j = 0; j < this.serviceForm.controls.details['controls'][length - 1].controls.count.value; j++) {
                count.push(this.fb.group({
                    amTitle: [''],
                    ruTitle: [''],
                    enTitle: ['', Validators.required],
                    amContent: [''],
                    ruContent: [''],
                    enContent: ['', Validators.required],
                }));
            }
        });
        return [];
    }

    createCardsLogoForm() {
        setTimeout(() => {
            const length = this.serviceForm.controls.details['controls'].length;
            const count = this.serviceForm.controls.details['controls'][length - 1].controls.cards as FormArray;
            for (let j = 0; j < this.serviceForm.controls.details['controls'][length - 1].controls.count.value; j++) {
                count.push(this.fb.group({
                    imgURL: ['', Validators.required],
                    amTitle: [''],
                    ruTitle: [''],
                    enTitle: ['', Validators.required],
                    amContent: [''],
                    ruContent: [''],
                    enContent: ['', Validators.required],
                }));
            }
        });
        return [];
    }

    changeCardsCount(e, i, type) {
        console.log(this.serviceForm.value);
        if (type === 'card') {
            const count = this.serviceForm.controls.details['controls'][i].controls.cards as FormArray;
            if (count.length === 4) {
                count.removeAt(3);
            } else {
                count.push(this.fb.group({
                    imgURL: [''],
                    amTitle: [''],
                    ruTitle: [''],
                    enTitle: ['', Validators.required],
                    amContent: [''],
                    ruContent: [''],
                    enContent: ['', Validators.required],
                }));
            }
            this.cardsCount = 7 - e.target.value;
        } else {
            const count = this.serviceForm.controls.details['controls'][i].controls.cards as FormArray;
            if (count.length === 4) {
                count.removeAt(3);
            } else {
                count.push(this.fb.group({
                    imgURL: [''],
                    amTitle: [''],
                    ruTitle: [''],
                    enTitle: ['', Validators.required],
                    amContent: [''],
                    ruContent: [''],
                    enContent: ['', Validators.required],
                }));
            }
            this.cardsLogoCount = 7 - e.target.value;
        }

    }

    changeLogosCount(e, i) {
        const count = this.serviceForm.controls.details['controls'][i].controls.logos as FormArray;
        if (count.length === 4) {
            count.removeAt(3);
        } else {
            count.push(this.fb.group({
                imgURL: [''],
                amTitle: [''],
                ruTitle: [''],
                enTitle: ['', Validators.required],
                amContent: [''],
                ruContent: [''],
                enContent: ['', Validators.required],
            }));
        }
    }

    addLogo(i) {
        const count = this.serviceForm.controls.details['controls'][i].controls.logos as FormArray;
        count.push(this.fb.group({
            imgURL: '',
        }));
    }

    createSlidersForm() {
        setTimeout(() => {
            const length = this.serviceForm.controls.details['controls'].length;
            const count = this.serviceForm.controls.details['controls'][length - 1].controls.sliders as FormArray;
            if (count.length === 2) {
                count.removeAt(1);
            } else {
                count.push(this.fb.group({
                    imageHeight: '30',
                    alignment: 'center',
                    verticalAlignment: 'center',
                    objectFit: 'cover',
                    watchList: 3,
                    imagesURL: this.fb.array([])
                }));
            }
        });
        return [];
    }

    createLogosForm() {
        setTimeout(() => {
            const length = this.serviceForm.controls.details['controls'].length;
            const count = this.serviceForm.controls.details['controls'][length - 1].controls.logos as FormArray;
            for (let i = 0; i < this.serviceForm.controls.details['controls'][length - 1].controls.count.value; ++i) {
                count.push(this.fb.group({
                    imgURL: ['', Validators.required]
                }));
            }
        });
        return [];
    }

    addImagesForSlider(detail, row) {
        const images = this.serviceForm.controls.details['controls'][detail].controls.sliders['controls'][row].controls.imagesURL as FormArray;
        images.push(this.fb.group({
            imgURL: ['', Validators.required]
        }));
        return [];
    }

    createSliderImagesEditForm(i, index) {
        setTimeout(() => {
            const count = this.serviceForm.controls.details['controls'][i].controls.sliders['controls'][index]['controls'].imagesURL as FormArray;


            this.page.details[i].sliders[index].imagesURL.forEach((c) => {
                count.push(this.fb.group({
                    imgURL: [c['imgURL']],
                }));

            });
        });
        return [];
    }

    removeSliderImg(detail, row, i) {
        this.delete = confirm('Are you want to delete?');
        const images = this.serviceForm.controls.details['controls'][detail].controls.sliders['controls'][row].controls.imagesURL as FormArray;
        if (this.delete) {
            images.removeAt(i);
        }
    }


    addTextCount(i) {

        const count = this.serviceForm.controls.details['controls'][i].controls.text as FormArray;
        count.push(this.fb.group({
            amTitle: [''],
            ruTitle: [''],
            enTitle: ['', Validators.required],
            amContent: [''],
            ruContent: [''],
            enContent: ['', Validators.required],
        }));
    }


    addTextLogo(i, index) {
        const count = this.serviceForm.controls.details['controls'][i].controls.textWithLogo as FormArray;
        count.push(this.fb.group({
            amTitle: [''],
            ruTitle: [''],
            enTitle: ['', Validators.required],
            amContent: [''],
            ruContent: [''],
            enContent: ['', Validators.required],
            imgURL: ['', Validators.required]
        }));
        console.log(this.serviceForm.value);
    }

    removeText(i, index, name) {
        this.delete = confirm('Are you want to delete?');
        const type = this.serviceForm.controls.details['controls'][i].controls[name] as FormArray;
        if (this.delete) {
            type.removeAt(index);
        }
    }

    reverse(i) {
        const val = this.serviceForm.get('details')['controls'][i].get('imageOrder')['value'];
        this.serviceForm.get('details')['controls'][i].get('imageOrder').setValue(3 - val);
        console.log(this.serviceForm.value);
    }

    removeFormEl(i) {
        this.delete = confirm('Are you want to delete?');
        const type = this.serviceForm.controls.details as FormArray;
        if (this.delete) {
            type.removeAt(i);
        }
    }

    changeLanguage(language) {
        this.language = language;
    }


    myService() {
        const title: {} = {
            am: this.serviceForm.get('amTitle').value,
            ru: this.serviceForm.get('ruTitle').value,
            en: this.serviceForm.get('enTitle').value
        };


        const description: {} = {
            am: this.serviceForm.get('amDescription').value,
            ru: this.serviceForm.get('ruDescription').value,
            en: this.serviceForm.get('enDescription').value
        };
        const form = {
            slug: this.serviceForm.get('slug').value,
            grStart: this.serviceForm.get('grStart').value,
            grEnd: this.serviceForm.get('grEnd').value,
            category: this.serviceForm.get('category').value,
            imgURL: this.serviceForm.get('imgURL').value,
            title: title,
            description: description,
            details: this.serviceForm.get('details').value
        };
        if (this.page) {
            this.dataService.updateData(form, 'service-page', this.page._id).subscribe(data => {
                this.router.navigate(['admin/service']).then();
            }, e => {
                console.log(e);
            });
        } else {
            this.dataService.sendData(form, 'service-page').subscribe(data => {
                this.router.navigate(['admin/service']).then();
            }, e => console.log(e));
        }


    }

}
