import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ServiceListComponent} from './service-list/service-list.component';
import {ServiceEditComponent} from './service-edit/service-edit.component';




const routes: Routes = [
    {path: '', component: ServiceListComponent},
    {path: 'edit', component: ServiceEditComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ServiceRoutingModule {
}
