import { Component, OnInit } from '@angular/core';
import {DataService} from '../../../../../_services/data.service';
import {Router} from '@angular/router';
import {ItemService} from '../../../../../_services/item.service';
import {AdminService} from '../../../../../_services/admin.service';

@Component({
  selector: 'app-service-list',
  templateUrl: './service-list.component.html',
  styleUrls: ['./service-list.component.css']
})
export class ServiceListComponent implements OnInit {
    pages = [];
    delete: any;

    constructor(
        private dataService: DataService,
        private router: Router,
        private itemService: ItemService,
        private adminService: AdminService) {}

  ngOnInit() {
      this.dataService.getData('service-page').subscribe(data => {
          this.pages = data['pages'];
          console.log(this.pages)
      }, (err) => {
          if (err.status === 401) {
              localStorage.clear();
              this.router.navigate(['login']);
          }
      });

  }

    deletePage(page, i) {
        this.delete = confirm('Are you want to delete?');
        if (this.delete === true) {
            this.dataService.delete('service-page', page._id).subscribe(data => {
                if (data['success']) {
                    this.pages.splice(i, 1);
                } else {
                    this.router.navigate(['login']);
                }
            }, (err) => {
                console.log(err);
            });
        }
    }

    updatePage(page) {
        this.itemService.servicePage = page;
        this.router.navigate(['admin/service/edit']);
        // console.log(this.itemService.portfolio)
    }


    viewPage(page) {
        this.itemService.servicePage = page;
    }


    create() {
        this.itemService.servicePage = null;
        this.router.navigate(['admin/service/edit']);
    }


}
