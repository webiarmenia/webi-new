import {Component, OnDestroy, OnInit, Renderer2} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../../../../_services/data.service';
import {Router} from '@angular/router';
import {Globals} from '../../../../app.globals';
import {ItemService} from '../../../../_services/item.service';
import {Page} from '../../../../_models/Page';



@Component({
  selector: 'app-page-create',
  templateUrl: './page-create.component.html',
  styleUrls: ['./page-create.component.scss']
})
export class PageCreateComponent implements OnInit, OnDestroy {
  pageForm: FormGroup;
  textForm: FormGroup;
  formForm: FormGroup;
  sliderForm: FormGroup;
  feedbackForm: FormGroup;
  cardForm: FormGroup;
  language: String = 'en';
  randomString;
  dirName;
  url;
  saved = false;
  count = -1;
  selected = false;
  typeOfComponents = [];
  componentType: String = '';
  page: Page;
  slugs = [];
  isValid = true;

  imgURL: any;
  public imagePath;
  imageWithTextPosition = 'left';
  imageTextInput = {
    en: '',
    ru: '',
    am: ''
  };
  position = true;
  id;


  constructor(
    private formBuilder: FormBuilder,
    private dataService: DataService,
    private router: Router,
    private globals: Globals,
    private itemService: ItemService
  ) {
    this.url = this.globals.queryUrl;

    if (this.itemService.page) {
      this.page = this.itemService.page;
      this.typeOfComponents = this.page.components;
    }
    if (this.slugs.length === 0) {
      this.dataService.getData('page').subscribe(data => {
        const pages = data['data'];
        if (pages.length > 0) {
          pages.forEach(p => {
            this.slugs.push(p.slug);
          });
        }
      }, (err) => {
        console.log(err);
      });
    } else {
      this.slugs = this.itemService.slugs;
    }
  }

  ngOnInit() {
    this.pageForm = this.formBuilder.group({
      slug: [this.page ? this.page.slug : '', Validators.required],
      amTitle: [this.page ? this.page['title'].am : ''],
      ruTitle: [this.page ? this.page['title'].ru : ''],
      enTitle: [this.page ? this.page['title'].en : '', Validators.required],
      amDescription: [this.page ? this.page['description'].am : ''],
      ruDescription: [this.page ? this.page['description'].ru : ''],
      enDescription: [this.page ? this.page['description'].en : '', Validators.required],
      // amContent: [''],
      // ruContent: [''],
      // enContent: ['', Validators.required],
      // img: ['', Validators.required],
      components: this.formBuilder.array(this.page ? this.page.components : [])
    });
    this.randomString = this.generateRandomString(10);
    this.dirName = this.randomString;


    this.textForm = this.formBuilder.group({
      type: 'text',
      amTitleText: '',
      ruTitleText: '',
      enTitleText: '',
      amDescriptionText: '',
      ruDescriptionText: '',
      enDescriptionText: '',
      position: ''
    });

    this.formForm = this.formBuilder.group({
      type: 'form',
      name: '',
      email: ''
    });

  }

  imageWithText(event, i) {

    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.pageForm.get('components')['controls'][i].get('img').setValue(file);
      this.imageWithTextPosition = this.pageForm.get('components')['controls'][i].get('imagePosition').value
      const reader = new FileReader()
      this.imagePath = file;
      reader.readAsDataURL(this.imagePath);
      reader.onload = () => {
        this.pageForm.get('components')['controls'][i].get('imgURL').setValue(reader.result);
        console.log(this.pageForm.get('components')['controls'][i].value)
      };

    }
  }

  imagePosition(e) {
    this.imageWithTextPosition = e.target.value;
    this.position = e.target.value === 'left';

  }

  imageWithTextInput(e, l) {
    this.imageTextInput[l] = e.target.value;
  }

  showDiv() {
  }


  selectType(value) {

    const modal = document.getElementsByClassName('hidden-modal')[0];
    modal['style'].display = 'none';

    const type = this.pageForm.controls.components as FormArray;
    if (value === 'text') {
      type.push(this.formBuilder.group({
        type: 'text',
        amTitleText: '',
        ruTitleText: '',
        enTitleText: '',
        amDescriptionText: '',
        ruDescriptionText: '',
        enDescriptionText: '',
        position: 'center'
      }));
    } else if (value === 'form') {
      type.push(this.formBuilder.group({
          type: 'form',
          fields: this.formBuilder.array([]),
          index: ''
        })
      );
    } else if (value === 'slider') {

    } else if (value === 'card') {
      type.push(this.formBuilder.group({
          type: 'card',
          amTitleCard: '',
          ruTitleCard: '',
          enTitleCard: '',
          amDescriptionCard: '',
          ruDescriptionCard: '',
          enDescriptionCard: '',
          amListCard: '',
          ruListCard: '',
          enListCard: '',
        })
      );
    } else if (value === 'imageWithText') {
      type.push(this.formBuilder.group({
        type: 'imageWithText',
        amText: '',
        ruText: '',
        enText: '',
        img: '',
        imagePosition: 'left',
        imageSize: '4',
        imgURL: ''
      }));
    }

    console.log('+++  ', this.pageForm.controls.components['controls']);
    this.count++;
  }

  selectInputType(value) {
    const modal = document.getElementsByClassName('hidden-modal-form')[0];
    modal['style'].display = 'none';

    const add_input = document.getElementsByClassName('add-input')[0];
    add_input['style'].display = 'block';

    // const inputType = this.pageForm.controls.components.controls. as FormArray;

    const inputType = this.pageForm.controls.components['controls'][this.id]['controls'].fields as FormArray;
    if (value === 'input') {
      inputType.push(this.formBuilder.group({
        inpType: 'input',
        enMessage: '',
        ruMessage: '',
        amMessage: ''
      }));
    } else if (value === 'textArea') {
      inputType.push(this.formBuilder.group({
        inpType: 'textArea',
        enMessage: '',
        ruMessage: '',
        amMessage: ''
      }));
    }
  }


  filterSlug(e) {
    this.isValid = true;
    this.slugs.forEach(slug => {
      if (slug.toLowerCase() === e.target.value.toLowerCase()) {
        this.isValid = false;
      }
    });
  }


  addComponent() {
    const modal = document.getElementsByClassName('hidden-modal')[0];
    modal['style'].display = 'block';

    document.getElementsByClassName('hidden-modal-form')[0]['style'].display = 'none';
  }

  addInput(i) {
    const modal = document.getElementsByClassName('hidden-modal-form')[0];
    modal['style'].display = 'block';

    const add_input = document.getElementsByClassName('add-input')[0];
    add_input['style'].display = 'none';

    this.id = i;
  }

  myPage() {

    console.log(this.pageForm.value.components);

    // if (this.isValid) {
    //   if (this.page) {
    //     this.typeOfComponents = this.typeOfComponents.map((t, i) => {
    //       return {
    //         type: t.type,
    //       };
    //     });
    //   }
    //
    //   this.pageForm.get('components').setValue(this.typeOfComponents);
    //   const fd: any = new FormData();
    //
    //   const components = this.pageForm.get('components').value;
    //   const slug = this.pageForm.get('slug').value;
    //
    //
    //   const title: {} = {
    //     am: this.pageForm.get('amTitle').value,
    //     ru: this.pageForm.get('ruTitle').value,
    //     en: this.pageForm.get('enTitle').value
    //   };
    //   const description: {} = {
    //     am: this.pageForm.get('amDescription').value,
    //     ru: this.pageForm.get('ruDescription').value,
    //     en: this.pageForm.get('enDescription').value
    //   };
    //
    //
    //   // const content: {} = {
    //   //   am: this.pageForm.get('amContent').value,
    //   //   ru: this.pageForm.get('ruContent').value,
    //   //   en: this.pageForm.get('enContent').value
    //   // };
    //   // fd.append('slug', this.dirName);
    //   // fd.append('img', this.pageForm.get('img').value);
    //   // fd.append('content', JSON.stringify(content));
    //
    //   fd.append('slug', this.pageForm.get('slug').value);
    //   fd.append('title', JSON.stringify(title));
    //   fd.append('description', JSON.stringify(description));
    //   fd.append('components', JSON.stringify(components));
    //   fd.append('random', this.dirName);
    //
    //
    //   if (!this.page) {
    //     console.log('New Page');
    //     // this.dataService.sendData(fd, 'page').subscribe(data => {
    //     //   if (data['success']) {
    //     //     this.saved = true;
    //     //     this.router.navigate(['admin/page']);
    //     //   }
    //     // }, (err) => {
    //     //   console.log(err);
    //     // });
    //   } else {
    //     console.log('Edit Page');
    //
    //     // this.dataService.updateData(fd, 'page', this.page._id).subscribe(data => {
    //     //   if (data['success']) {
    //     //     this.saved = true;
    //     //     this.router.navigate(['admin/page']);
    //     //   }
    //     // }, (err) => {
    //     //   console.log(err);
    //     // });
    //   }
    // }

  }

  changeLanguage(language) {
    this.language = language;
  }

  generateRandomString(stringLength) {
    let randomString = '';
    let randomAscii;
    for (let i = 0; i < stringLength; i++) {
      randomAscii = Math.floor((Math.random() * 25) + 97);
      randomString += String.fromCharCode(randomAscii);
    }
    return randomString;
  }

  ngOnDestroy() {
    if (!this.saved) {
      // this.ckService.ckDeleteDir(this.dirName, 'page').subscribe();
    }
  }
}
