import {Component, OnInit} from '@angular/core';
import {DataService} from '../../../../../_services/data.service';
import {Router} from '@angular/router';
import {ItemService} from '../../../../../_services/item.service';
import {AdminService} from '../../../../../_services/admin.service';
import {SimplePage} from '../../../../../_models/SimplePage';

@Component({
    selector: 'app-simple-list',
    templateUrl: './simple-list.component.html',
    styleUrls: ['./simple-list.component.css']
})
export class SimpleListComponent implements OnInit {

    simplePages: SimplePage[] = [];
    delete: any;

    constructor(
        private dataService: DataService,
        private router: Router,
        private itemService: ItemService,
        private adminService: AdminService) {
    }

    ngOnInit() {
        this.dataService.getData('simple-page').subscribe(data => {
            this.simplePages = data['pages'];
            console.log(this.simplePages);
        }, (err) => {
            if (err.status === 401) {
                localStorage.clear();
                this.router.navigate(['login']);
            }
        });
    }


    deletePage(page, i) {
        this.delete = confirm('Are you want to delete?');
        if (this.delete === true) {
            this.dataService.delete('simple-page', page._id).subscribe(data => {
                if (data['success']) {
                    this.simplePages.splice(i, 1);
                } else {
                    this.router.navigate(['login']);
                }
            }, (err) => {
                console.log(err);
            });
        }
    }

    updatePage(page) {
        this.itemService.simplePage = page;
        this.router.navigate(['admin/simple/edit']);
        // console.log(this.itemService.portfolio)
    }


    /////////////////////////// imasty vorna  viewPage ????  service page um el ka //////////////////

    viewPage(page) {
        this.itemService.simplePage = page;
    }


    create() {
        this.itemService.simplePage = null;
        this.router.navigate(['admin/simple/edit']);
    }

}
