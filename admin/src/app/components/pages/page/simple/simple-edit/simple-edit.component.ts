import {Component, OnDestroy, OnInit} from '@angular/core';
import {SimplePage} from '../../../../../_models/SimplePage';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../../../../../_services/data.service';
import {ItemService} from '../../../../../_services/item.service';
import {Router} from '@angular/router';
import {AdminService} from '../../../../../_services/admin.service';
import {Globals} from '../../../../../app.globals';
import {EditorService} from '../../../../../_services/editor.service';
import {generateTypeCheckBlock} from '@angular/compiler-cli/src/ngtsc/typecheck/src/type_check_block';

@Component({
    selector: 'app-simple-edit',
    templateUrl: './simple-edit.component.html',
    styleUrls: ['./simple-edit.component.css']
})
export class SimpleEditComponent implements OnInit, OnDestroy {

    simpleForm: FormGroup;
    simple: SimplePage;
    language: String = 'en';
    slugs = [];
    inValidSlug = false;

    slugPattern = '[^\\s]+';

    editorConfigs;
    dirName;
    url;
    randomString;
    randomsArr = [];

    constructor(
        private fb: FormBuilder,
        private dataService: DataService,
        private itemService: ItemService,
        private router: Router,
        private adminService: AdminService,
        private globals: Globals,
        private editorService: EditorService
    ) {
        if (this.itemService.simplePage) {
            this.simple = this.itemService.simplePage;
            console.log(this.simple);
        }

        this.url = globals.queryUrl;
        this.dirName = this.generateRandomString(10);
        this.editorConfigs = {
            plugins: 'print preview fullpage powerpaste casechange importcss tinydrive searchreplace autolink' +
                ' autosave save directionality advcode visualblocks visualchars fullscreen image link media' +
                ' mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime' +
                ' advlist lists checklist wordcount tinymcespellchecker a11ychecker imagetools textpattern ' +
                'noneditable help formatpainter permanentpen pageembed charmap mentions quickbars linkchecker ' +
                'emoticons',
            menubar: 'file edit view insert format tools table tc help',
            toolbar: 'undo redo | bold italic underline strikethrough | ' +
                'fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify |' +
                ' outdent indent |  numlist bullist checklist |' +
                ' forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak |' +
                ' charmap emoticons | fullscreen  preview save print |' +
                ' insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl ',

            imagetools_toolbar: 'rotateleft rotateright | flipv fliph | editimage imageoptions',
            images_upload_url: this.url + '/uploads/pages/editor/' + this.dirName + '/',
            images_upload_handler: this.handlerEditor,
        };
    }

    generateRandomString(stringLength) {


        if (this.simple) {
            return this.simple.dirName;
        } else {
            let randomString = '';
            let randomAscii;
            for (let i = 0; i < stringLength; i++) {
                randomAscii = Math.floor((Math.random() * 25) + 97);
                randomString += String.fromCharCode(randomAscii);
            }
            return randomString;
        }
    }


    ngOnInit() {
        this.simpleForm = this.fb.group({
            slug: [this.simple ? this.simple.slug : null, [Validators.required, Validators.pattern(this.slugPattern)]],
            amTitle: [this.simple ? this.simple['title'].am : null],
            ruTitle: [this.simple ? this.simple['title'].ru : null],
            enTitle: [this.simple ? this.simple['title'].en : null, Validators.required],
            amShortDescription: [this.simple ? this.simple['shortDescription'].am : null],
            ruShortDescription: [this.simple ? this.simple['shortDescription'].ru : null],
            enShortDescription: [this.simple ? this.simple['shortDescription'].en : null],
            key: [{value: this.simple ? this.simple['key'] : null, disabled: this.havePermission()}],
            content: [this.simple ? this.simple['content'] : null],
        });
    }

    handlerEditor = (blobInfo, success, failure) => {


        console.log(1111111);


        let formData;

        formData = new FormData();
        // formData.append('random', this.randomString);
        formData.append('random', this.dirName);
        formData.append('folder', 'simplePage');
        formData.append('image', blobInfo.blob(), blobInfo.filename());


        this.editorService.sendData(formData, 'page').subscribe((d: any) => {
            console.log(d)
                // this.randomsArr.push(d['fileName']);
                // console.log(this.randomsArr);
                success(this.url + 'uploads/page/editor/' + this.dirName + '/' + d.fileName);
            },
            e => console.log(e)
        );
    }

    havePermission() {

        return this.adminService.getAdmin().role !== 'superAdmin';
    }


    //////////////////////////////////  Slug Validation Function (teri e nayev Servicneri hamar) /////////////////////////////

    filterSlug(e) {
        this.inValidSlug = false;

        this.slugs.forEach(slug => {
            if (slug.toLowerCase() === e.target.value.toLowerCase()) {
                this.inValidSlug = true;
            }
        });
    }


    changeLanguage(language) {
        this.language = language;
    }


    mySimple() {
        console.log(this.simpleForm.value);
        const title: {} = {
            am: this.simpleForm.get('amTitle').value,
            ru: this.simpleForm.get('ruTitle').value,
            en: this.simpleForm.get('enTitle').value
        };


        const shortDescription: {} = {
            am: this.simpleForm.get('amShortDescription').value,
            ru: this.simpleForm.get('ruShortDescription').value,
            en: this.simpleForm.get('enShortDescription').value
        };
        const form = {
            slug: this.simpleForm.get('slug').value,
            key: this.simpleForm.get('key').value,
            title: title,
            shortDescription: shortDescription,
            content: this.simpleForm.get('content').value,
            dirName: this.dirName
        };
        if (this.simple) {

            this.dataService.updateData(form, 'simple-page', this.simple._id).subscribe(data => {
                this.randomsArr = [];
                this.router.navigate(['admin/simple']).then();
            }, e => {
                console.log(e);
            });
        } else {
            this.dataService.sendData(form, 'simple-page').subscribe(data => {
                this.randomsArr = [];

                this.router.navigate(['admin/simple']).then();
            }, e => console.log(e));
        }
    }

    ngOnDestroy(): void {
        if (this.randomsArr.length > 0) {
            this.editorService.deleteData(this.randomsArr).subscribe((d: any) => {
                    console.log(this.randomsArr);
                    // success(this.url + '/uploads/pages/ckeditor/' + this.dirName + '/' + d.filename);
                },
                e => console.log(e)
            );
        }
    }

}
