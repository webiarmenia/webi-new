import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SimpleListComponent} from './simple-list/simple-list.component';
import {SimpleEditComponent} from './simple-edit/simple-edit.component';




const routes: Routes = [
    {path: '', component: SimpleListComponent},
    {path: 'edit', component: SimpleEditComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SimpleRoutingModule {
}
