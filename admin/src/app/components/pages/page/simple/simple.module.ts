import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SimpleListComponent} from './simple-list/simple-list.component';
import {SimpleEditComponent} from './simple-edit/simple-edit.component';
import {SimpleRoutingModule} from './simple-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EditorModule} from '@tinymce/tinymce-angular';


@NgModule({
    declarations: [
        SimpleListComponent,
        SimpleEditComponent
    ],
    imports: [
        CommonModule,
        SimpleRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        EditorModule
    ]
})
export class SimpleModule {
}
