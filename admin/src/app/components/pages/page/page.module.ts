import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {NgxTreeDndModule} from 'ngx-tree-dnd';
import {PageRoutingModule} from './page-routing.module';
import {PageListComponent} from './page-list/page-list.component';
import {PageCreateComponent} from './page-create/page-create.component';
import {PageViewComponent} from './page-view/page-view.component';
import {PageEditComponent} from './page-edit/page-edit.component';
import {SortablejsModule} from 'angular-sortablejs';


@NgModule({
  declarations: [
    PageListComponent,
    PageCreateComponent,
    PageViewComponent,
    PageEditComponent,
  ],
  imports: [
    CommonModule,
    PageRoutingModule,
    FormsModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    NgxTreeDndModule,
    SortablejsModule.forRoot({animation: 150}),

  ],
})
export class PageModule {
}
