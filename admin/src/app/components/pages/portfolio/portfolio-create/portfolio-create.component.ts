import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../../../../_services/data.service';
import {Router} from '@angular/router';
import {CkeditorService} from '../../../../_services/ckeditor.service';
import {Globals} from '../../../../app.globals';
import {ItemService} from '../../../../_services/item.service';
import {Portfolio} from '../../../../_models/Portfolio';


@Component({
    selector: 'app-portfolio-create',
    templateUrl: './portfolio-create.component.html',
    styleUrls: ['./portfolio-create.component.scss']
})
export class PortfolioCreateComponent implements OnInit, OnDestroy {
    portfolioForm: FormGroup;
    portfolio: Portfolio;
    portfolios: Portfolio[];
    slugs = [];
    detailsView = false;
    inValidSlug = false;
    language: String = 'en';
    randomString;
    dirName;
    url;
    saved = false;

    imgURL: any;
    public imagePath;
    imageWithTextPosition = 'left';
    imageWithImagePosition = 'left';
    position = true;
    alignments = ['center', 'right', 'left'];

    imageHeights = [30, 50, 70];
    imageSize = [3, 4, 6, 8, 9];

    verticalAlignments = ['center', 'top', 'bottom'];
    objectFits = ['cover', 'contain', 'none'];

    imagesArr = [];

    delete: any;

    constructor(
        private formBuilder: FormBuilder,
        private dataService: DataService,
        private itemService: ItemService,
        private ckService: CkeditorService,
        private router: Router,
        private globals: Globals,
    ) {
        if (this.itemService.portfolio) {
            this.portfolio = this.itemService.portfolio;
            console.log(this.portfolio);
        }
        this.dataService.getData('portfolio').subscribe(data => {
            data['portfolios'].forEach(p => {
                this.slugs.push(p.slug)
            });
            console.log(this.slugs)

        }, err => {
            console.log(err)
        });

        this.url = this.globals.queryUrl;
    }

    ngOnInit() {
        this.portfolioForm = this.formBuilder.group({
            url: [this.portfolio ? this.portfolio.url : '', Validators.required],
            slug: [this.portfolio ? this.portfolio.slug : '', Validators.required],
            sliderPosition: [this.portfolio ? this.portfolio.sliderPosition : 'top', Validators.required],
            amTitle: [this.portfolio ? this.portfolio['title'].am : ''],
            ruTitle: [this.portfolio ? this.portfolio['title'].ru : ''],
            enTitle: [this.portfolio ? this.portfolio['title'].en : '', Validators.required],
            // amDescription: [''],
            // ruDescription: [''],
            // enDescription: ['', Validators.required],
            amHover: [this.portfolio ? this.portfolio['hover'].am : ''],
            ruHover: [this.portfolio ? this.portfolio['hover'].ru : ''],
            enHover: [this.portfolio ? this.portfolio['hover'].en : '', Validators.required],
            amShortDescription: [this.portfolio ? this.portfolio['shortDescription'].am : ''],
            ruShortDescription: [this.portfolio ? this.portfolio['shortDescription'].ru : ''],
            enShortDescription: [this.portfolio ? this.portfolio['shortDescription'].en : '', Validators.required],
            imgURL: [this.portfolio ? this.portfolio.image : '', Validators.required],
            details: this.formBuilder.array(this.portfolio ? this.buildForm() : [])
        });
        this.randomString = this.generateRandomString(10);
        this.dirName = this.randomString;

    }

    addTools() {
        this.detailsView = !this.detailsView;
    }

    filterSlug(e) {
        this.inValidSlug = false;
        this.slugs.forEach(slug => {
            if (slug.toLowerCase() === e.target.value.toLowerCase()) {
                this.inValidSlug = true;
            }
        });
    }

    buildForm() {
        setTimeout(() => {
            const type = this.portfolioForm.controls.details as FormArray;
            this.portfolio.details.forEach((p, i) => {
                if (p['type'] === 'text') {
                    type.push(this.formBuilder.group({
                        type: 'text',
                        amText1: [p['amText1']],
                        ruText1: [p['ruText1']],
                        enText1: [p['enText1']],
                        amText2: [p['amText2']],
                        ruText2: [p['ruText2']],
                        enText2: [p['enText2']],
                        titleAlign: [p['titleAlign']],
                        titleWidth: [p['titleWidth']],
                        descriptionAlign: [p['descriptionAlign']],
                    }));
                } else if (p['type'] === 'cover') {
                    type.push(this.formBuilder.group({
                        type: 'cover',
                        imgURL: [p['imgURL'], Validators.required],
                        alignment: p['alignment'],
                        verticalAlignment: p['verticalAlignment'],
                        objectFit: p['objectFit']

                    }));
                } else if (p['type'] === 'imageWithText') {
                    type.push(this.formBuilder.group({
                        // type: 'imageWithText',
                        // amTitle: [p['amTitle']],
                        // ruTitle: [p['ruTitle']],
                        // enTitle: [p['enTitle']],
                        // amText: [p['amText']],
                        // ruText: [p['ruText']],
                        // enText: [p['enText']],
                        // imagePosition: [p['imagePosition']],
                        // imgURL: [p['imgURL']],


                        type: 'imageWithText',
                        textSize: [p['textSize']],
                        imgURL: [p['imgURL'], Validators.required],
                        imagePosition: p['imagePosition'],
                        imgSize: p['imgSize'],
                        imageOrder: p['imageOrder'],
                        imageHeight: p['imageHeight'],
                        alignment: p['alignment'],
                        verticalAlignment: p['verticalAlignment'],
                        objectFit: p['objectFit'],
                        amTitle: [p['amTitle']],
                        ruTitle: [p['ruTitle']],
                        enTitle: [p['enTitle'], Validators.required],
                        amContent: [p['amContent']],
                        ruContent: [p['ruContent']],
                        enContent: [p['enContent'], Validators.required],

                    }));
                } else if (p['type'] === 'imageWithImage') {
                    type.push(this.formBuilder.group({
                        // type: 'imageWithImage',
                        // imgURL1: [p['imgURL1'], Validators.required],
                        // imgURL2: [p['imgURL2'], Validators.required],
                        // imagePosition: [p['imagePosition']],
                        // imgURL1: [p['imgURL1']],
                        // imgURL2: [p['imgURL2']]


                        type: 'imageWithImage',
                        imageHeight: p['imageHeight'],
                        imagePosition: p['imagePosition'],
                        imgSize: p['imgSize'],
                        imageOrder: p['imageOrder'],
                        img1: this.formBuilder.group({
                            imgURL: [p['img1']['imgURL'], Validators.required],
                            alignment: p['img1']['alignment'],
                            verticalAlignment: p['img1']['verticalAlignment'],
                            objectFit: p['img1']['objectFit'],
                        }),
                        img2: this.formBuilder.group({
                            imgURL: [p['img2']['imgURL'], Validators.required],
                            alignment: p['img2']['alignment'],
                            verticalAlignment: p['img2']['verticalAlignment'],
                            objectFit: p['img2']['objectFit'],
                        })
                    }));
                }
            });

        });
        return [];
    }


    onFileChange(event) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];

            const reader = new FileReader();
            this.imagePath = file;
            reader.readAsDataURL(this.imagePath);
            reader.onload = () => {
                // this.portfolioForm.get('details')['controls'][i].get('imgURL').setValue(reader.result);
                this.portfolioForm.get('imgURL').setValue(reader.result);
            };


        }

    }

    coverImage(event, i) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            const reader = new FileReader();
            this.imagePath = file;
            reader.readAsDataURL(this.imagePath);
            reader.onload = () => {
                this.portfolioForm.get('details')['controls'][i].get('imgURL').setValue(reader.result);
            };
        }
    }


    imageWithText(event, i) {

        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            this.imageWithTextPosition = this.portfolioForm.get('details')['controls'][i].get('imagePosition').value;

            const reader = new FileReader();
            this.imagePath = file;
            reader.readAsDataURL(this.imagePath);
            reader.onload = () => {
                this.portfolioForm.get('details')['controls'][i].get('imgURL').setValue(reader.result);
                console.log(this.portfolioForm.get('details')['controls'][i].value);
            };
        }
    }


    imageWithImage(event, i, name) {
        if (name === 'img1') {
            const file = event.target.files[0];
            const reader = new FileReader();
            this.imagePath = file;
            reader.readAsDataURL(this.imagePath);
            reader.onload = () => {
                this.portfolioForm.get('details')['controls'][i]['controls'].img1['controls'].imgURL.setValue(reader.result);
            };
        } else {
            const file = event.target.files[0];
            const reader = new FileReader();
            this.imagePath = file;
            reader.readAsDataURL(this.imagePath);
            reader.onload = () => {
                this.portfolioForm.get('details')['controls'][i]['controls'].img2['controls'].imgURL.setValue(reader.result);
            };
        }
    }


    imagePosition(e) {
        this.imageWithTextPosition = e.target.value;
        this.position = e.target.value === 'left';

    }

    imageImagePosition(e) {
        this.imageWithImagePosition = e.target.value;
        this.position = e.target.value === 'left';
    }

    addDetails() {
        const modal = document.getElementsByClassName('hidden-modal')[0];
        modal['style'].display = 'block';
    }

    removeFormEl(i) {
        this.delete = confirm('Are you want to delete?');
        const type = this.portfolioForm.controls.details as FormArray;
        if (this.delete) {
            type.removeAt(i);
        }
    }

    selectType(value) {
        this.detailsView = !this.detailsView;


        const type = this.portfolioForm.controls.details as FormArray;
        if (value === 'text') {
            type.push(this.formBuilder.group({
                type: 'text',
                amText1: '',
                ruText1: '',
                enText1: '',
                amText2: '',
                ruText2: '',
                enText2: '',
                titleAlign: 'left',
                titleWidth: 12,
                descriptionAlign: 'left',
            }));
        } else if (value === 'cover') {
            type.push(this.formBuilder.group({
                type: 'cover',
                imgURL: ['', Validators.required],
                alignment: 'center',
                verticalAlignment: 'center',
                objectFit: 'cover',
            }));
        } else if (value === 'imageWithText') {
            type.push(this.formBuilder.group({
                type: 'imageWithText',
                textSize: '',
                imgURL: ['', Validators.required],
                imagePosition: 'left',
                imgSize: '3',
                imageOrder: '1',
                imageHeight: '30',
                alignment: 'center',
                verticalAlignment: 'center',
                objectFit: 'cover',
                amTitle: [''],
                ruTitle: [''],
                enTitle: ['', Validators.required],
                amContent: [''],
                ruContent: [''],
                enContent: ['', Validators.required],
            }));
        } else if (value === 'imageWithImage') {
            type.push(this.formBuilder.group({
                type: 'imageWithImage',
                imageHeight: '30',
                imagePosition: 'left',
                imgSize: '3',
                imageOrder: '1',
                img1: this.formBuilder.group({
                    imgURL: ['', Validators.required],
                    alignment: 'center',
                    verticalAlignment: 'center',
                    objectFit: 'cover',
                }),
                img2: this.formBuilder.group({
                    imgURL: ['', Validators.required],
                    alignment: 'center',
                    verticalAlignment: 'center',
                    objectFit: 'cover',
                })

            }));
        }

    }


    myPortfolio() {
        // console.log(this.portfolioForm.value);
        // const fd: any = new FormData();

        // const description: {} = {
        //   am: this.portfolioForm.get('amDescription').value,
        //   ru: this.portfolioForm.get('ruDescription').value,
        //   en: this.portfolioForm.get('enDescription').value
        // };
        const title: {} = {
            am: this.portfolioForm.get('amTitle').value,
            ru: this.portfolioForm.get('ruTitle').value,
            en: this.portfolioForm.get('enTitle').value
        };

        const hover: {} = {
            am: this.portfolioForm.get('amHover').value,
            ru: this.portfolioForm.get('ruHover').value,
            en: this.portfolioForm.get('enHover').value
        };
        const shortDescription: {} = {
            am: this.portfolioForm.get('amShortDescription').value,
            ru: this.portfolioForm.get('ruShortDescription').value,
            en: this.portfolioForm.get('enShortDescription').value
        };

        const form = {
            url: this.portfolioForm.get('url').value,
            slug: this.portfolioForm.get('slug').value,
            imgURL: this.portfolioForm.get('imgURL').value,
            sliderPosition: this.portfolioForm.get('sliderPosition').value,
            title: title,
            hover: hover,
            shortDescription: shortDescription,
            random: this.dirName,
            details: this.portfolioForm.get('details').value
        };

        if (this.portfolio) {
            // console.log(this.portfolioForm.value);
            this.dataService.updateData(form, 'portfolio', this.portfolio._id).subscribe(data => {
                if (data['success']) {
                    this.saved = true;
                    this.router.navigate(['admin/portfolio']);
                }
            }, (err) => {
                console.log(err);
            });

        } else {
            this.dataService.sendData(form, 'portfolio').subscribe(data => {
                if (data['success']) {
                    this.saved = true;
                    this.router.navigate(['admin/portfolio']);
                }
            }, (err) => {
                console.log(err);
            });
        }
    }

    changeLanguage(language) {
        this.language = language;
    }

    generateRandomString(stringLength) {
        let randomString = '';
        let randomAscii;
        for (let i = 0; i < stringLength; i++) {
            randomAscii = Math.floor((Math.random() * 25) + 97);
            randomString += String.fromCharCode(randomAscii);
        }
        return randomString;
    }

    ngOnDestroy() {
        if (!this.saved) {
            this.ckService.ckDeleteDir(this.dirName, 'portfolio').subscribe();
        }
    }

    reverse(i) {
        this.portfolioForm.get('details')['controls'][i].get('imageOrder').setValue(3 - this.portfolioForm.get('details')['controls'][i].get('imageOrder')['value']);
        console.log(this.portfolioForm.value)
    }


    // imagesArrFilter(obj, key) {
    //   for (let i = 0; i < this.imagesArr.length; ++i) {
    //     if (this.imagesArr[i].id === obj.id && this.imagesArr[i].key === key) {
    //       // this.imagesArr.splice(i, 1);
    //       // this.imagesArr.push(obj);
    //       this.imagesArr[i] = obj;
    //       return this.imagesArr;
    //     }
    //   }
    //   this.imagesArr.push(obj);
    //   return this.imagesArr;
    // }

}
