import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AdminService} from '../../../../_services/admin.service';
import {Admin} from '../../../../_models/Admin';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-create-admin',
    templateUrl: './create-admin.component.html',
    styleUrls: ['./create-admin.component.css']
})
export class CreateAdminComponent implements OnInit {
    form: FormGroup;
    roles = ['admin', 'staff'];
    admin: Admin;
    done = false;
    passwordChange = true;

    constructor(
        private fb: FormBuilder,
        private adminService: AdminService,
        private router: Router
    ) {
        if (this.adminService.updateAdmin) {
            this.admin = this.adminService.updateAdmin;
            this.passwordChange = false;
        } else {
            // this.adminService.getOne(this.route.params['_value'].id).subscribe(data => {
            //     this.admin = data['admin'];
            //     console.log(this.admin)
            //     this.done = true;
            // }, e => {
            //     console.log(e);
            // })
        }
    }

    // password: [null, [Validators.required, Validators.minLength(4)]],

    ngOnInit() {
        this.form = this.fb.group({
            avatar: [this.admin ? this.admin.avatar : null],
            firstName: [this.admin ? this.admin.firstName : null, [Validators.required]],
            lastName: [this.admin ? this.admin.lastName : null, [Validators.required]],
            email: [this.admin ? this.admin.email : null, [Validators.email, Validators.required]],
            role: [this.admin ? this.admin.role : null, Validators.required],
            permissions: this.createPermissionForm()
        });
        if (!this.admin) {
            this.form.addControl('password', this.fb.control(null, [Validators.required, Validators.minLength(4)]));
        }
    }

    createPermissionForm() {
        if (this.admin) {
            return this.fb.group({
                can_create: [this.admin.permissions['can_create'], Validators.required],
                can_update: [this.admin.permissions['can_update'], Validators.required],
                can_delete: [this.admin.permissions['can_delete'], Validators.required]
            });
        } else {
            return this.fb.group({
                can_create: [false, Validators.required],
                can_update: [false, Validators.required],
                can_delete: [false, Validators.required]
            });
        }
    }

    create() {
        const fd = new FormData();
        fd.append('avatar', this.form.get('avatar').value);
        fd.append('firstName', this.form.get('firstName').value);
        fd.append('lastName', this.form.get('lastName').value);
        fd.append('email', this.form.get('email').value);
        fd.append('role', this.form.get('role').value);
        fd.append('permissions', JSON.stringify(this.form.get('permissions').value));

        if (this.admin) {
            if (this.passwordChange) {
                fd.append('password', this.form.get('password').value);
            }
            this.adminService.update(this.admin.id, fd).subscribe(data => {
                console.log(data);
                this.router.navigate(['admin/admin-tools/admins'])
            }, e => console.log(e))
        } else {
            console.log(this.form.value);
            fd.append('password', this.form.get('password').value);
            this.adminService.addAdmin(fd).subscribe(data => {
                console.log(data)
            }, e => console.log(e))
        }
    }


    onFileChange(event) {

        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            this.form.get('avatar').setValue(file);
        }
    }

    changePassword() {
        this.form.addControl('password', this.fb.control(null, [Validators.required, Validators.minLength(4)]));
        this.passwordChange = true;
    }


}
