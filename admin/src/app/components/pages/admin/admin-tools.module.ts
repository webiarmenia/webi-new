import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateAdminComponent} from './create-admin/create-admin.component';
import {AdminToolsRoutingModule} from './admin-tool-routing.module';
import {AdminsComponent} from './admins/admins.component';
import {AdminToolsComponent} from './admin-tools.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
    declarations: [
        CreateAdminComponent,
        AdminsComponent,
        AdminToolsComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        AdminToolsRoutingModule
    ]
})
export class AdminToolsModule {
}
