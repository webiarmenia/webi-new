import {Component, OnInit} from '@angular/core';
import {AdminService} from '../../../../_services/admin.service';
import {Admin} from '../../../../_models/Admin';
import {Router} from '@angular/router';

@Component({
    selector: 'app-admins',
    templateUrl: './admins.component.html',
    styleUrls: ['./admins.component.scss']
})
export class AdminsComponent implements OnInit {
    admins: Admin[] = [];

    constructor(
        private adminService: AdminService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.adminService.getAdmins().subscribe(data => {
            this.admins = data['admins'];
            console.log(typeof this.admins[0].avatar);
        }, e => {
            console.log(e);
        })
    }

    fullscreenCard(index) {
        document.getElementById('card-' + 0).classList.toggle('fullscreened');
    }

    edit(admin: Admin) {
        this.adminService.updateAdmin = admin;
        this.router.navigate([`admin/admin-tools/edit`]);
    }

    create() {
        this.adminService.updateAdmin = null;
        this.router.navigate([`admin/admin-tools/edit`]);
    }


}
