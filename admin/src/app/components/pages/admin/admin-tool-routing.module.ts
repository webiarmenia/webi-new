import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CreateAdminComponent} from './create-admin/create-admin.component';
import {AdminsComponent} from './admins/admins.component';
import {AdminToolsComponent} from './admin-tools.component';


const ADMIN_TOOLS: Routes = [
  {path: '', component: AdminToolsComponent},
  {path: 'admins', component: AdminsComponent},
  {path: 'edit', component: CreateAdminComponent}
];

@NgModule({
  imports: [RouterModule.forChild(ADMIN_TOOLS)],
  exports: [RouterModule]
})
export class AdminToolsRoutingModule {
}
