import { Component, OnInit } from '@angular/core';
import {Portfolio} from '../../../../_models/Portfolio';
import {DataService} from '../../../../_services/data.service';
import {Router} from '@angular/router';
import {ItemService} from '../../../../_services/item.service';
import {Advantage} from '../../../../_models/Advantage';

@Component({
  selector: 'app-advantage-list',
  templateUrl: './advantage-list.component.html',
  styleUrls: ['./advantage-list.component.css']
})
export class AdvantageListComponent implements OnInit {

  advantages: Advantage[];
  delete: any;
  searchTerm: String;

  constructor(private dataService: DataService, private router: Router, private itemService: ItemService) {
  }

  ngOnInit() {
    this.dataService.getData('advantage').subscribe(data => {
      this.advantages = data['advantages'];
    }, (err) => {
      if (err.status === 401) {
        localStorage.clear();
        this.router.navigate(['login']);
      }
    });
  }


  deleteAdvantage(advantage, i) {
    this.delete = confirm('Are you want to delete?');
    if (this.delete == true) {
      this.dataService.delete('advantage', advantage._id).subscribe(data => {

        if (data['success']) {
          this.advantages.splice(i, 1);
        } else {
          this.router.navigate(['login']);
        }
      }, (err) => {
        console.log(err);
      });
    }
  }

  updateAdvantage(advantage) {
    this.itemService.advantage = advantage;
    this.router.navigate(['admin/advantage/edit']);
  }


  viewAdvantage(advantage) {
    this.itemService.advantage = advantage;
  }


}
