import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../../../../_services/data.service';
import {Router} from '@angular/router';
import {Globals} from '../../../../app.globals';

@Component({
  selector: 'app-advantage-create',
  templateUrl: './advantage-create.component.html',
  styleUrls: ['./advantage-create.component.css']
})
export class AdvantageCreateComponent implements OnInit {


  myForm: FormGroup;
  language: String = 'en';

  constructor(
    private formBuilder: FormBuilder,
    private dataService: DataService,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.myForm = this.formBuilder.group({
      amTitle: [null],
      ruTitle: [null],
      enTitle: [null, Validators.required],
      amDescription: [null],
      ruDescription: [null],
      enDescription: [null, Validators.required],
      order: [null, Validators.required],
    });
  }

  create() {
    const form = {};
    const title: {} = {
      am: this.myForm.get('amTitle').value,
      ru: this.myForm.get('ruTitle').value,
      en: this.myForm.get('enTitle').value
    };
    const description: {} = {
      am: this.myForm.get('amDescription').value,
      ru: this.myForm.get('ruDescription').value,
      en: this.myForm.get('enDescription').value
    };

    form['order'] = this.myForm.get('order').value;
    form['title'] = title;
    form['description'] = description;

    this.dataService.sendData(form, 'advantage').subscribe(data => {
      if (data['success']) {
        this.router.navigate(['admin/advantage']);
      }
    }, (err) => {
      console.log(err);
    });
  }

  changeLanguage(language) {
    this.language = language;
  }

}
