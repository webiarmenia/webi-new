import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AdvantageListComponent} from './advantage-list/advantage-list.component';
import {AdvantageCreateComponent} from './advantage-create/advantage-create.component';
import {AdvantageEditComponent} from './advantage-edit/advantage-edit.component';
import {AdvantageViewComponent} from './advantage-view/advantage-view.component';


const ADVANTAGE_ROUTES: Routes = [
    {path: '', component: AdvantageListComponent},
    {path: 'create', component: AdvantageCreateComponent},
    {path: 'edit', component: AdvantageEditComponent},
    {path: ':id', component: AdvantageViewComponent},
];

@NgModule({
    imports: [RouterModule.forChild(ADVANTAGE_ROUTES)],
    exports: [RouterModule]
})
export class AdvantageRoutingModule {
}
