import {Component, OnInit} from '@angular/core';
import {ItemService} from '../../../../_services/item.service';
import {DataService} from '../../../../_services/data.service';
import {Router} from '@angular/router';
import {Advantage} from '../../../../_models/Advantage';

@Component({
  selector: 'app-advantage-view',
  templateUrl: './advantage-view.component.html',
  styleUrls: ['./advantage-view.component.css']
})
export class AdvantageViewComponent implements OnInit {
  delete;
  advantage: Advantage;
  done = false;

  constructor(
    private service: ItemService,
    private dataService: DataService,
    private router: Router) {
    if (!this.service.advantage) {
      this.router.navigate(['admin/advantage']);
    }
  }

  ngOnInit() {
    this.advantage = this.service.advantage;
    this.done = true;
  }

  updateAdvantage(advantage) {
    this.service.card = advantage;
    this.router.navigate(['admin/advantage/edit']);
  }

  deleteAdvantage(advantage) {
    this.delete = confirm('Are you want to delete?');
    if (this.delete === true) {
      this.dataService.delete('advantage', advantage._id).subscribe(data => {
        if (data['success']) {
          this.router.navigate(['admin/advantage']);
        } else {
          console.log('Data ', data);
        }
      }, (err) => {
        console.log(err);
      });
    }
  }

  ok() {
    this.router.navigate(['admin/advantage']);
  }


}
