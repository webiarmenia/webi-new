import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdvantageRoutingModule} from './advantage-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ColorPickerModule} from 'ngx-color-picker';
import {AdvantageCreateComponent} from './advantage-create/advantage-create.component';
import {AdvantageListComponent} from './advantage-list/advantage-list.component';
import {AdvantageViewComponent} from './advantage-view/advantage-view.component';
import {AdvantageEditComponent} from './advantage-edit/advantage-edit.component';

@NgModule({
    declarations: [
        AdvantageCreateComponent,
        AdvantageListComponent,
        AdvantageEditComponent,
        AdvantageViewComponent
    ],
    imports: [
        CommonModule,
        AdvantageRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        ColorPickerModule
    ]
})
export class AdvantageModule {
}
