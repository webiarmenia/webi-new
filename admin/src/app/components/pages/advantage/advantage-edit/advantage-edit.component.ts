import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../../../../_services/data.service';
import {Router} from '@angular/router';
import {ItemService} from '../../../../_services/item.service';
import {Advantage} from '../../../../_models/Advantage';

@Component({
  selector: 'app-advantage-edit',
  templateUrl: './advantage-edit.component.html',
  styleUrls: ['./advantage-edit.component.css']
})
export class AdvantageEditComponent implements OnInit {

  myForm: FormGroup;
  language: String = 'en';
  advantage: Advantage;

  constructor(
    private formBuilder: FormBuilder,
    private dataService: DataService,
    private router: Router,
    private itemService: ItemService
  ) {
    if (!this.itemService.advantage) {
      this.router.navigate(['admin/advantage']);
    }
  }

  ngOnInit() {
    this.advantage = this.itemService.advantage;

    this.myForm = this.formBuilder.group({
      amTitle: [this.advantage.title['am']],
      ruTitle: [this.advantage.title['ru']],
      enTitle: [this.advantage.title['en']],
      amDescription: [this.advantage.description['am']],
      ruDescription: [this.advantage.description['ru']],
      enDescription: [this.advantage.description['en'], Validators.required],
      order: [this.advantage.order, Validators.required],
    });
  }

  create() {
    const form = {};
    const title: {} = {
      am: this.myForm.get('amTitle').value,
      ru: this.myForm.get('ruTitle').value,
      en: this.myForm.get('enTitle').value
    };
    const description: {} = {
      am: this.myForm.get('amDescription').value,
      ru: this.myForm.get('ruDescription').value,
      en: this.myForm.get('enDescription').value
    };

    form['order'] = this.myForm.get('order').value;
    form['title'] = title;
    form['description'] = description;

    this.dataService.updateData(form, 'advantage', this.advantage._id).subscribe(data => {
      if (data['success']) {
        this.router.navigate(['admin/advantage']);
      }
    }, (err) => {
      console.log(err);
    });
  }

  changeLanguage(language) {
    this.language = language;
  }

}
