import {Component, OnInit} from '@angular/core';
import {ClientService} from '../../../_services/client.service';
import {Router} from '@angular/router';
import {Client} from '../../../_models/Client';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {

  clients: Client[];
  done = false;

  constructor(
    private  service: ClientService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.getClients();
  }

  getClients() {
    this.service.getClients().subscribe(
      data => {
        this.clients = data['clients'];
        console.log(this.clients);
        this.done = true;
      },
      err => console.log(err)
    );
  }

  updateAccount(client) {
    this.service.updateClient = client;
    this.router.navigate(['admin/client/edit']);
  }

  fullscreenCard(index) {
    document.getElementById('card-' + 0).classList.toggle('fullscreened');
  }

  view(client) {
    this.service.updateClient = client;
    this.router.navigate([`admin/client/${client.nikName}`]);
  }
}
