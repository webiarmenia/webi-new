import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ClientService} from '../../../../_services/client.service';
import {ProjectService} from '../../../../_services/project.service';
import {Client} from '../../../../_models/Client';

@Component({
  selector: 'app-view-account',
  templateUrl: './view-account.component.html',
  styleUrls: ['./view-account.component.css']
})
export class ViewAccountComponent implements OnInit {

  client: Client;
  projects;
  done = false;

  constructor(
    private router: Router,
    private service: ClientService,
    private projectService: ProjectService,
    private route: ActivatedRoute
  ) {
    if (!this.service.updateClient) {
      this.service.getOne(this.route.params['_value'].nikName).subscribe(data => {
        this.client = data['client'];
        console.log(this.client)
        this.getProjectsByClientId(this.client.id);
      }, e => console.log(e));
    } else {
      this.client = this.service.updateClient;
      this.getProjectsByClientId(this.client.id);
    }

  }

  ngOnInit() {

  }

  edit(client) {
    this.service.updateClient = client;
    this.router.navigate(['admin/client/edit']);
  }

  createProject() {
    this.projectService.project = null;
    this.router.navigate([`admin/client/${this.client.nikName}/create`])
  }

  updateAccount(project) {

  }

  sendPass() {
    this.service.sendPass(this.client.id).subscribe(d => {
      this.client.hasPassword = true;
    }, e => {
      console.log(e)
    });
  }

  getProjectsByClientId(id) {
    this.projectService.getProjects(id).subscribe(data => {
      this.projectService.projects = data['projects'];
      this.service.updateClient = this.client
      this.done = true;
    }, e => console.log(e));
  }


}
