import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ProjectService} from '../../../../../../_services/project.service';
import {ClientService} from '../../../../../../_services/client.service';
import {Client} from '../../../../../../_models/Client';
import {Project} from '../../../../../../_models/Project';

@Component({
    selector: 'app-project-create',
    templateUrl: './project-create.component.html',
    styleUrls: ['./project-create.component.css']
})
export class ProjectCreateComponent implements OnInit {

    form: FormGroup;
    priorityArr = [1, 2, 3, 4, 5];
    client: Client;
    project: Project;
    slugs = [];
    slugInvalid = false;

    constructor(
        private service: ProjectService,
        private clientService: ClientService,
        private router: Router,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute
    ) {
        if (this.service.project) {
            this.project = this.service.project;
        }
        this.client = this.clientService.updateClient;
        this.service.getProjects(this.client.id).subscribe(data => {
            data['projects'].forEach(p => {
                this.slugs.push(p.slug);
            });
            console.log(this.slugs);
        }, e => console.log(e));

    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            name: [this.project ? this.project.name : '', [Validators.required]],
            description: [this.project ? this.project.description : '', [Validators.required]],
            priority: [this.project ? this.project.priority : '', Validators.required],
            slug: [this.project ? this.project.slug : '', Validators.required],
            clientId: [this.client.id]
        });
    }

    slugFilter(e) {
        const val = e.target.value;
        if (this.slugs.length > 0) {
            this.slugs.forEach(s => {
                this.slugInvalid = s === val;
            });
        }
    }


    sendForm() {
        if (this.project) {
            this.service.updateProject(this.project.id, this.form.value).subscribe(data => {
                this.router.navigate([`admin/client/${this.client.nikName}`]);
            }, e => console.log(e));
        } else {
            this.service.addProject(this.form.value).subscribe(data => {
                this.router.navigate([`admin/client/${this.client.nikName}`]);
            }, e => console.log(e));
        }

    }

}
