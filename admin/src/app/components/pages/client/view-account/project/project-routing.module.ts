import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ProjectListComponent} from './project-list/project-list.component';
import {ProjectCreateComponent} from './project-create/project-create.component';
import {ProjectViewComponent} from './project-view/project-view.component';
import {ProjectInvoiceComponent} from './invoice/project-invoice/project-invoice.component';
import {InvoiceViewComponent} from './invoice/invoice-view/invoice-view.component';
import {InvoiceListComponent} from './invoice/invoice-list/invoice-list.component';
import {ContractEditComponent} from './contract/contract-edit/contract-edit.component';
import {ContractViewComponent} from './contract/contract-view/contract-view.component';
import {ContractListComponent} from './contract/contract-list/contract-list.component';


const invoiceChildren: Routes = [
  {path: 'invoice/edit', component: ProjectInvoiceComponent},
  {path: 'invoice/:id', component: InvoiceViewComponent},
  {path: 'invoice', component: InvoiceListComponent},
  {path: 'contract/edit', component: ContractEditComponent},
  {path: 'contract/:id', component: ContractViewComponent},
  {path: 'contract', component: ContractListComponent}
];

const routes: Routes = [
  {path: '', component: ProjectListComponent},
  {path: 'create', component: ProjectCreateComponent},
  {path: ':id', component: ProjectViewComponent, children: invoiceChildren},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule {
}
