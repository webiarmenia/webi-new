import {Component, OnInit} from '@angular/core';
import {Project} from '../../../../../../../_models/Project';
import {Client} from '../../../../../../../_models/Client';
import {Contract} from '../../../../../../../_models/Contract';
import {ContractInfo} from '../../../../../../../_models/ContractInfo';
import {InvoiceService} from '../../../../../../../_services/invoice.service';
import {ProjectService} from '../../../../../../../_services/project.service';
import {ClientService} from '../../../../../../../_services/client.service';
import {Router} from '@angular/router';
import {ContractService} from '../../../../../../../_services/contract.service';

@Component({
  selector: 'app-contract-list',
  templateUrl: './contract-list.component.html',
  styleUrls: ['./contract-list.component.scss']
})
export class ContractListComponent implements OnInit {
  project: Project;
  contracts: Contract[] = [];
  contractInfos: ContractInfo[];
  client: Client;
  done = false;

  constructor(
    private contractService: ContractService,
    private projectService: ProjectService,
    private clientService: ClientService,
    private router: Router
  ) {
    this.project = this.projectService.project;
    this.client = this.clientService.updateClient;
  }

  ngOnInit() {
    this.contractService.getContracts(this.project.id).subscribe(data => {
      this.contractInfos = data['contractInfos'];
      console.log('contracts ', this.contractInfos);

      this.done = true;
    }, e => console.log(e));
  }


  createContract() {
    this.contractService.contract = null;
    this.router.navigate([`admin/client/${this.client.nikName}/${this.project.id}/contract/edit`]);
  }

  updateContract(contract) {
    this.contractService.contract = contract;
    this.router.navigate([`admin/client/${this.client.nikName}/${this.project.id}/contract/edit`]);
  }

  viewContract(contractInfo) {
    this.contractService.contractInfo = contractInfo;
    this.contractService.contract = contractInfo.contract;
    this.router.navigate([`admin/client/${this.client.nikName}/${this.project.id}/contract/${contractInfo.contract.id}`]);

  }



}
