import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {NgxTreeDndModule} from 'ngx-tree-dnd';
import {ProjectListComponent} from './project-list/project-list.component';
import {ProjectEditComponent} from './project-edit/project-edit.component';
import {ProjectCreateComponent} from './project-create/project-create.component';
import {ProjectRoutingModule} from './project-routing.module';
import { ProjectViewComponent } from './project-view/project-view.component';
import { ProjectInvoiceComponent } from './invoice/project-invoice/project-invoice.component';
import { InvoiceViewComponent } from './invoice/invoice-view/invoice-view.component';
import { InvoiceListComponent } from './invoice/invoice-list/invoice-list.component';
import { ContractListComponent } from './contract/contract-list/contract-list.component';
import { ContractEditComponent } from './contract/contract-edit/contract-edit.component';
import { ContractViewComponent } from './contract/contract-view/contract-view.component';
import {EditorModule} from '@tinymce/tinymce-angular';

@NgModule({
  declarations: [
    ProjectListComponent,
    ProjectEditComponent,
    ProjectCreateComponent,
    ProjectViewComponent,
    ProjectInvoiceComponent,
    InvoiceViewComponent,
    InvoiceListComponent,
    ContractListComponent,
    ContractEditComponent,
    ContractViewComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxTreeDndModule,
    ProjectRoutingModule,
    EditorModule
  ],
})
export class ProjectModule {
}
