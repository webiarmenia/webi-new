import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ClientService} from '../../../../../../_services/client.service';
import {ProjectService} from '../../../../../../_services/project.service';
import {Project} from '../../../../../../_models/Project';
import {Client} from '../../../../../../_models/Client';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit {
  projects: Project[];
  done = false;
  client: Client;

  constructor(
    private router: Router,
    private service: ClientService,
    private projectService: ProjectService
  ) {
    this.client = this.service.updateClient;
  }

  ngOnInit() {
    this.projects = this.projectService.projects;
    console.log('+++ ', this.projects);
    // this.projectService.getProjects(this.client.id).subscribe(data => {
    //   this.projects = data['projects'];
    //   this.done = true;
    // }, e => console.log(e));
  }

  updateProject(project) {
    this.projectService.project = project;
    this.router.navigate([`admin/client/${this.client.nikName}/create`]);
  }

  viewProject(project) {
    this.projectService.project = project;
    this.router.navigate([`admin/client/${this.client.nikName}/${project.id}`]);
  }
}
