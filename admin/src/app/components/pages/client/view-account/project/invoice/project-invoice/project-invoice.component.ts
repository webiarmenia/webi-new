import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Project} from '../../../../../../../_models/Project';
import {ProjectService} from '../../../../../../../_services/project.service';
import {InvoiceService} from '../../../../../../../_services/invoice.service';
import {Invoice} from '../../../../../../../_models/Invoice';
import {ClientService} from '../../../../../../../_services/client.service';
import {Client} from '../../../../../../../_models/Client';

@Component({
    selector: 'app-project-invoice',
    templateUrl: './project-invoice.component.html',
    styleUrls: ['./project-invoice.component.css']
})
export class ProjectInvoiceComponent implements OnInit {

    form: FormGroup;
    delete: any;
    balance = 0;
    subTotal = 0;
    project: Project;
    feeFormSeen = false;
    invoice: Invoice;
    client: Client;

    feeNames = ['feeName1', 'feeName2', 'feeName3'];
    feeTypes = ['flat amount', 'percent'];
    currencies = ['USD', 'RUB', 'AMD'];

    constructor(
        private router: Router,
        private formBuilder: FormBuilder,
        private projectService: ProjectService,
        private invoiceService: InvoiceService,
        private clientService: ClientService,
    ) {
        this.project = this.projectService.project;
        this.invoice = this.invoiceService.invoice;
        this.client = this.clientService.updateClient;
        if (this.invoice) {
            this.balance = this.invoice.balance;
            this.subTotal = this.invoice.total;
        }
    }

    ngOnInit() {

        this.form = this.formBuilder.group({
            fromName: [this.invoice ? this.invoice.fromName : null, [Validators.required]],
            fromEmail: [this.invoice ? this.invoice.fromEmail : null, [Validators.required, Validators.email]],
            fromAddress: [this.invoice ? this.invoice.fromAddress : null, [Validators.required]],
            fromPhone: [this.invoice ? this.invoice.fromPhone : null],
            fromBusiness: [this.invoice ? this.invoice.fromBusiness : null],
            toName: [this.invoice ? this.invoice.toName : null, [Validators.required]],
            toEmail: [this.invoice ? this.invoice.toEmail : null, [Validators.required, Validators.email]],
            toAddress: [this.invoice ? this.invoice.toAddress : null, [Validators.required]],
            toPhone: [this.invoice ? this.invoice.toPhone : null],
            number: [this.invoice ? this.invoice.number : null, [Validators.required]],
            date: [this.invoice ? this.invoice.date : null, [Validators.required]],
            due: [this.invoice ? this.invoice.due : null, [Validators.required]],
            notes: [this.invoice ? this.invoice.notes : null],
            currency: [this.invoice ? this.invoice.currency : 0],
            details: this.invoice ? this.createDetailsForm() : this.formBuilder.array([]),
            // fee: this.invoice ? this.createFeeForm() : this.formBuilder.group({
            //   name: [null],
            //   value: [null],
            //   type: [null]
            // }),
            fee: this.createFeeForm(),
            total: [this.invoice ? this.invoice.total : null],
            balance: [this.invoice ? this.invoice.balance : null]
        });

    }


    feeChange() {
        this.feeFormSeen = !this.feeFormSeen;
        this.countBalance();
        this.form.controls.fee.reset();
    }

    back() {
        this.router.navigate([`admin/client/${this.client.nikName}/${this.project.id}/invoice`]);
    }


    createDetailsForm() {
        setTimeout(() => {
            const miniForm = this.form.controls.details as FormArray;
            this.invoice.details.forEach((d, i) => {
                miniForm.push(this.formBuilder.group({
                    description: [d['description']],
                    rate: [d['rate']],
                    qty: [d['qty']],
                    amount: [d['amount']],
                    tax: [d['tax']],
                    addDetails: [d['addDetails']]
                }));
            });

        });
        return this.formBuilder.array([])
    }

    createFeeForm() {
        if (this.invoice) {
            if (this.invoice.fee) {
                this.feeFormSeen = true;
                return this.formBuilder.group({
                    name: [this.invoice.fee.name],
                    value: [this.invoice.fee.value],
                    type: [this.invoice.fee.type]
                });
            }
        } else {
            return this.formBuilder.group({
                name: [null],
                value: [null],
                type: [null]
            });
        }
    }

    feeValueChange() {
        // console.log(this.form.controls.fee['controls'].value.value);
        // console.log(this.form.controls.fee['controls'].type.value);
        // if (this.form.controls.fee['controls'].type.value === 0) {
        //   this.balance += this.form.controls.fee['controls'].value.value
        // }
    }

    addForm() {
        const miniForm = this.form.controls.details as FormArray;

        miniForm.push(this.formBuilder.group({
            description: [null],
            rate: [null, [Validators.required]],
            qty: [1, [Validators.required]],
            amount: [null],
            tax: [null],
            addDetails: [null]
        }));

    }


    orDelete(i) {
        this.delete = confirm('Are you want to delete?');
        const detail = this.form.controls.details as FormArray;
        if (this.delete) {
            this.balance -= this.form.controls.details['controls'][i]['controls'].amount.value;
            detail.removeAt(i);
        }
    }


    onKeyUp(i) {
        // tslint:disable-next-line:max-line-length
        this.form.controls.details['controls'][i]['controls'].amount.setValue(this.form.controls.details['controls'][i].value.rate * this.form.controls.details['controls'][i].value.qty);
        this.countBalance();
    }

    countBalance() {
        this.balance = 0;
        this.subTotal = 0;
        if (this.feeFormSeen) {
            this.form.controls.details['controls'].forEach(c => {
                this.balance += c['controls'].amount.value;
                this.subTotal += c['controls'].amount.value;
            });

            if (this.form.controls.fee['controls'].type.value === 0) {
                this.balance += this.form.controls.fee['controls'].value.value;
            } else {
                this.balance += (this.form.controls.fee['controls'].value.value / 100) * this.balance;
            }

        } else {
            this.form.controls.details['controls'].forEach(c => {
                this.balance += c['controls'].amount.value;
                this.subTotal += c['controls'].amount.value;
            });
        }

    }


    myCreate() {
        console.log(this.form.value);
        if (this.invoice) {
            this.form.controls.total.setValue(this.subTotal);
            this.form.controls.balance.setValue(this.balance);

            this.invoiceService.updateInvoice(this.invoice.id, this.form.value).subscribe(data => {
                this.router.navigate([`admin/client/${this.client.nikName}/${this.project.id}/invoice`]);
            }, e => console.log(e))
        } else {
            this.form.controls.total.setValue(this.subTotal);
            this.form.controls.balance.setValue(this.balance);
            this.invoiceService.addInvoice(this.form.value, this.project.id).subscribe(data => {
                this.router.navigate([`admin/client/${this.client.nikName}/${this.project.id}/invoice`]);
            }, e => {
                console.log(e);
            });
        }

    }

}
