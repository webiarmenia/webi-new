import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Project} from '../../../../../../../_models/Project';
import {Client} from '../../../../../../../_models/Client';
import {ContractInfo} from '../../../../../../../_models/ContractInfo';
import {Contract} from '../../../../../../../_models/Contract';
import {ProjectService} from '../../../../../../../_services/project.service';
import {ClientService} from '../../../../../../../_services/client.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ContractService} from '../../../../../../../_services/contract.service';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';


@Component({
    selector: 'app-contract-view',
    templateUrl: './contract-view.component.html',
    styleUrls: ['./contract-view.component.scss']
})
export class ContractViewComponent implements OnInit {
    @ViewChild('content', {static: false}) content: ElementRef;


    project: Project;
    client: Client;
    contract: Contract;
    contractInfo: ContractInfo;
    done = false;
    feeNames = ['feeName1', 'feeName2', 'feeName3'];
    feeTypes = ['flat amount', 'percent'];
    message;

    constructor(
        private contractService: ContractService,
        private projectService: ProjectService,
        private clientService: ClientService,
        private route: ActivatedRoute,
        private router: Router,
    ) {
        this.client = this.clientService.updateClient;
        this.project = this.projectService.project;
        if (!this.contractService.contract) {
            console.log(this.route.params['_value'].id);
            this.contractService.getOne(this.route.params['_value'].id).subscribe(data => {
                this.contract = data['contract'];
                this.contractInfo = data['contractInfo'];
                console.log('contractInfo  ', this.contractInfo);
                console.log('contract  ', this.contract);
                this.done = true;
            }, e => {
                console.log(e);
            });
        } else {
            this.contract = this.contractService.contract;
            this.contractInfo = this.contractService.contractInfo;
            this.done = true;
        }
    }

    ngOnInit() {
    }

    pdf() {
        const content = this.content.nativeElement;
        html2canvas(content).then(canvas => {
            // const imgWidth = 208;
            // const imgHeight = canvas.height * imgWidth / canvas.width;
            //
            // const contentDataURL = canvas.toDataURL('image/png');
            // const pdf = new jspdf('p', 'mm', 'a4');
            // const position = 0;
            // pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);

            setTimeout(() => {
                const imgData = canvas.toDataURL('image/png');
                const imgWidth = 210;
                const pageHeight = 295;
                const imgHeight = canvas.height * imgWidth / canvas.width;
                let heightLeft = imgHeight;
                const doc = new jspdf('p', 'mm');
                let position = 0;

                doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
                heightLeft -= pageHeight;

                while (heightLeft >= 0) {
                    position = heightLeft - imgHeight;
                    doc.addPage();
                    doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
                    heightLeft -= pageHeight;
                }

                const obj = {
                    pdf: imgData
                };
                this.contractService.updateContractPDF(this.contract.id, obj).subscribe(data => {
                    this.router.navigate([`admin/client/${this.client.nikName}/${this.project.id}/contract`]);
                }, e => {
                    console.log(e);
                });

                doc.save('invoice.pdf');
            }, 200)
        });


    }

}
