import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Invoice} from '../../../../../../../_models/Invoice';
import {InvoiceService} from '../../../../../../../_services/invoice.service';
import {ActivatedRoute, Router} from '@angular/router';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import {InvoiceInfo} from '../../../../../../../_models/InvoiceInfo';
import {ProjectService} from '../../../../../../../_services/project.service';
import {ClientService} from '../../../../../../../_services/client.service';
import {Project} from '../../../../../../../_models/Project';
import {Client} from '../../../../../../../_models/Client';


@Component({
    selector: 'app-invoice-view',
    templateUrl: './invoice-view.component.html',
    styleUrls: ['./invoice-view.component.scss']
})
export class InvoiceViewComponent implements OnInit {
    @ViewChild('content', {static: false}) content: ElementRef;
    invoice: Invoice;
    invoiceInfo: InvoiceInfo;
    done = false;
    feeNames = ['feeName1', 'feeName2', 'feeName3'];
    feeTypes = ['flat amount', 'percent'];
    currencies = ['USD', 'RUB', 'AMD'];
    feeForCient;

    message;
    project: Project;
    client: Client;

    constructor(
        private invoiceService: InvoiceService,
        private projectService: ProjectService,
        private clientService: ClientService,
        private route: ActivatedRoute,
        private router: Router,
    ) {
        this.client = this.clientService.updateClient;
        this.project = this.projectService.project;
        if (!this.invoiceService.invoice) {
            this.invoiceService.getOne(this.route.params['_value'].id).subscribe(data => {
                this.invoice = data['invoice'];
                this.invoiceInfo = data['invoiceInfo'];
                console.log('invoiceInfo  ', this.invoiceInfo);
                console.log('invoice  ', this.invoice);
                this.countFee();
                this.done = true;
            }, e => {
                console.log(e);
            });
        } else {
            this.invoice = this.invoiceService.invoice;
            this.invoiceInfo = this.invoiceService.invoiceInfo;
            this.countFee();
            this.done = true;
        }
    }

    ngOnInit() {
    }

    countFee() {
        this.feeForCient = this.invoice.fee.value;
        if (this.invoice.fee.type === 1) {
            this.feeForCient = this.invoice.total * (this.invoice.fee.value / 100);
        }
    }

    back() {
        this.router.navigate([`admin/client/${this.client.nikName}/${this.project.id}/invoice`]);
    }

    pdf() {
        const content = this.content.nativeElement;
        html2canvas(content).then(canvas => {
            // Few necessary setting options

            setTimeout(() => {
                const imgWidth = 208;
                const imgHeight = canvas.height * imgWidth / canvas.width;

                const contentDataURL = canvas.toDataURL('image/png');
                const pdf = new jspdf('p', 'mm', 'a4');
                const position = 0;
                pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
                const obj = {
                    invoicePdf: contentDataURL
                };
                this.invoiceService.updateInvoicePDF(this.invoice.id, obj).subscribe(data => {
                    this.router.navigate([`admin/client/${this.client.nikName}/${this.project.id}/invoice`]);
                }, e => {
                    console.log(e);
                });

                pdf.save('invoice.pdf');
            },200);
        });


    }

    sendMessage() {
        console.log(this.message);

    }

}
