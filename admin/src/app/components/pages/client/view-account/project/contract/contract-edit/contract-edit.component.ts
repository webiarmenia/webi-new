import {Component, OnInit} from '@angular/core';
import {Contract} from '../../../../../../../_models/Contract';
import {ContractService} from '../../../../../../../_services/contract.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Project} from '../../../../../../../_models/Project';
import {Client} from '../../../../../../../_models/Client';
import {ProjectService} from '../../../../../../../_services/project.service';
import {ClientService} from '../../../../../../../_services/client.service';

@Component({
  selector: 'app-contract-edit',
  templateUrl: './contract-edit.component.html',
  styleUrls: ['./contract-edit.component.scss']
})
export class ContractEditComponent implements OnInit {

  contract: Contract;
  project: Project;
  client: Client;
  form: FormGroup;
  ckConfig: any;

  constructor(
    private fb: FormBuilder,
    private contractService: ContractService,
    private projectService: ProjectService,
    private clientService: ClientService,
    private router: Router
  ) {
    this.project = this.projectService.project;
    this.contract = this.contractService.contract;
    this.client = this.clientService.updateClient;
  }

  ngOnInit() {
    this.form = this.fb.group({
      content: [this.contract ? this.contract.content : null, [Validators.required]]
    });
    this.ckConfig = {
      plugins: ' preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks' +
        ' visualchars fullscreen  link  template codesample table charmap hr pagebreak nonbreaking' +
        ' anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker ' +
        ' mediaembed  linkchecker contextmenu colorpicker textpattern help',
      toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link |' +
        ' alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
    };
  }


  myCreate() {
    if (this.contract) {
      this.contractService.updateContract(this.contract.id, this.form.value).subscribe(data => {
        this.router.navigate([`admin/client/${this.client.nikName}/${this.project.id}/contract`]);
      })

    } else {
      this.contractService.addContract(this.form.value, this.project.id).subscribe(data => {
        this.router.navigate([`admin/client/${this.client.nikName}/${this.project.id}/contract`]);
      }, e => {
        console.log(e);
      });
    }
  }

  back() {
    this.router.navigate([`admin/client/${this.client.nikName}/${this.project.id}/contract`]);
  }

}
