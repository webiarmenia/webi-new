import {Component, OnInit} from '@angular/core';
import {InvoiceService} from '../../../../../../../_services/invoice.service';
import {Project} from '../../../../../../../_models/Project';
import {ProjectService} from '../../../../../../../_services/project.service';
import {Router} from '@angular/router';
import {Invoice} from '../../../../../../../_models/Invoice';
import {Client} from '../../../../../../../_models/Client';
import {ClientService} from '../../../../../../../_services/client.service';
import {InvoiceInfo} from '../../../../../../../_models/InvoiceInfo';

@Component({
  selector: 'app-invoice-list',
  templateUrl: './invoice-list.component.html',
  styleUrls: ['./invoice-list.component.scss']
})
export class InvoiceListComponent implements OnInit {
  project: Project;
  invoices: Invoice[] = [];
  invoiceInfos: InvoiceInfo[];
  client: Client;
  done = false;

  constructor(
    private invoiceService: InvoiceService,
    private projectService: ProjectService,
    private clientService: ClientService,
    private router: Router
  ) {
    // if (!this.projectService.project) {
    //   this.router.navigate([`admin/client`]);
    // }
    this.project = this.projectService.project;
    this.client = this.clientService.updateClient;
  }

  ngOnInit() {
    this.invoiceService.getInvoices(this.project.id).subscribe(data => {
      this.invoiceInfos = data['invoiceInfos'];
      if (this.invoiceInfos) {
        this.invoiceInfos.forEach(i => {
          this.invoices.push(i.invoice);
        });
      }
      console.log(this.invoices)
      this.done = true;
    }, e => {
      console.log(e);
    });
  }


  updateInvoice(invoice) {
    this.invoiceService.invoice = invoice;
    this.router.navigate([`admin/client/${this.client.nikName}/${this.project.id}/invoice/edit`]);
  }

  viewInvoice(invoiceInfo) {
    this.invoiceService.invoiceInfo = invoiceInfo;
    this.invoiceService.invoice = invoiceInfo.invoice;
    this.router.navigate([`admin/client/${this.client.nikName}/${this.project.id}/invoice/${invoiceInfo.invoice.id}`]);

  }



  createInvooice() {
    this.invoiceService.invoice = null;
    this.router.navigate([`admin/client/${this.client.nikName}/${this.project.id}/invoice/edit`]);
  }

}
