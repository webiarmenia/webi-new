import {Component, OnInit} from '@angular/core';
import {ProjectService} from '../../../../../../_services/project.service';
import {Project} from '../../../../../../_models/Project';
import {ActivatedRoute, Router} from '@angular/router';
import {ClientService} from '../../../../../../_services/client.service';
import {Client} from '../../../../../../_models/Client';
import {InvoiceService} from '../../../../../../_services/invoice.service';

@Component({
  selector: 'app-project-view',
  templateUrl: './project-view.component.html',
  styleUrls: ['./project-view.component.css']
})
export class ProjectViewComponent implements OnInit {
  project: Project;
  client: Client;
  invoices;
  done = false;

  constructor(
    private service: ProjectService,
    private router: Router,
    private clientService: ClientService,
    private route: ActivatedRoute,
    private invoiceService: InvoiceService
  ) {
    if (!this.service.project) {
      this.service.getOne(this.route.params['_value'].id).subscribe(data => {
        this.project = data['project'];
        this.service.project = this.project
        this.done = true;
      }, e => console.log(e))
    } else {
      this.project = this.service.project;
      this.done = true;

    }
  }

  ngOnInit() {

  }

}
