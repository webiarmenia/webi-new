import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ClientComponent} from './client.component';
import {CreateAccountComponent} from './create-account/create-account.component';
import {EditAccountComponent} from './edit-account/edit-account.component';
import {ViewAccountComponent} from './view-account/view-account.component';


const children: Routes = [
  {path: 'projects', loadChildren: () => import('./view-account/project/project.module').then(m => m.ProjectModule)},
  {path: '', redirectTo : 'projects'},
];

const routes: Routes = [
  {path: '', component: ClientComponent},
  {path: 'create', component: CreateAccountComponent},
  {path: 'edit', component: EditAccountComponent},
  {path: ':nikName', component: ViewAccountComponent, children : children},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule {
}
