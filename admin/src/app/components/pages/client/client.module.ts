import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {NgxTreeDndModule} from 'ngx-tree-dnd';
import {ClientRoutingModule} from './client-routing.module';
import {ClientComponent} from './client.component';
import {CreateAccountComponent} from './create-account/create-account.component';
import { EditAccountComponent } from './edit-account/edit-account.component';
import { ViewAccountComponent } from './view-account/view-account.component';


@NgModule({
  declarations: [
    ClientComponent,
    CreateAccountComponent,
    EditAccountComponent,
    ViewAccountComponent,

  ],
  imports: [
    CommonModule,
    ClientRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxTreeDndModule,

  ],
})
export class ClientModule {
}
