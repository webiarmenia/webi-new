import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../../../../_services/data.service';
import {Router} from '@angular/router';
import {Globals} from '../../../../app.globals';

@Component({
  selector: 'app-card-create',
  templateUrl: './card-create.component.html',
  styleUrls: ['./card-create.component.css']
})
export class CardCreateComponent implements OnInit {

  myForm: FormGroup;
  language: String = 'en';
  url;
  bgColor = '#5582ca';
  textColor = '#000';
  titleMaxLength = 15;
  descriptionMaxLength = 150;

  constructor(
    private formBuilder: FormBuilder,
    private dataService: DataService,
    private router: Router,
    private globals: Globals,
  ) {
    this.url = this.globals.queryUrl;
  }

  ngOnInit() {
    this.myForm = this.formBuilder.group({
      amTitle: [null],
      ruTitle: [null],
      enTitle: [null, Validators.required],
      amDescription: [null],
      ruDescription: [null],
      enDescription: [null, Validators.required],
      url: [null],
      bgColor: [null],
      textColor: [null],
      img: ['', Validators.required],

    });
  }

  myNews() {
    const fd: any = new FormData();

    console.log(this.myForm.value)
    const form = {};
    const title: {} = {
      am: this.myForm.get('amTitle').value,
      ru: this.myForm.get('ruTitle').value,
      en: this.myForm.get('enTitle').value
    };
    const description: {} = {
      am: this.myForm.get('amDescription').value,
      ru: this.myForm.get('ruDescription').value,
      en: this.myForm.get('enDescription').value
    };

    fd.append('img', this.myForm.get('img').value);
    fd.append('url', this.myForm.get('url').value);
    fd.append('background', this.bgColor);
    fd.append('textColor', this.textColor);
    fd.append('title', JSON.stringify(title));
    fd.append('description', JSON.stringify(description));



    this.dataService.sendData(fd, 'cards-type-one').subscribe(data => {
        if (data['success']) {
            this.router.navigate(['admin/card']);
        }
    }, (err) => {
        console.log(err);
    });
  }

  onFileChange(event) {

    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.myForm.get('img').setValue(file);
    }
  }

  changeLanguage(language) {
    this.language = language;
  }
}
