import {Component, OnInit} from '@angular/core';
import {StylesService} from '../../../_services/styles.service';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Globals} from '../../../app.globals';
import {AdminService} from '../../../_services/admin.service';
import {Admin} from '../../../_models/Admin';


@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    currentUser = null;
    email: String;
    admin: Admin;
    languages = [
        {
            name: 'en',
            image: 'assets/images/language/en.png'
        },
        {
            name: 'ru',
            image: 'assets/images/language/ru.png'
        },
        {
            name: 'am',
            image: 'assets/images/language/am.png'
        }
    ];
    final;
    lan = {
        image: 'assets/images/language/en.png',
        name: ''
    };

    constructor(
        private stylesService: StylesService,
        private router: Router,
        private translate: TranslateService,
        private adminService: AdminService
    ) {
    }

    ngOnInit() {
        this.final = this.languages.filter(item => {
            return item.name !== 'en';
        });
        // this.email = localStorage.getItem('email');
        // this.email = localStorage.getItem('email');
        this.admin = this.adminService.getAdmin();
        // console.log(this.admin);
    }

    switchLanguage(l) {

        this.translate.use(l.name);
        this.lan = l;

        this.final = this.languages.filter(item => {
            return item.name !== l.name;
        });
    }

    menu_collapse() {
        this.stylesService.setCollase();
    }

    myLogout() {
        localStorage.clear();
        this.router.navigate(['login']);
    }

}
