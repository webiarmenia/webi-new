import {Component, OnInit, OnDestroy} from '@angular/core';
// import PerfectScrollbar from 'perfect-scrollbar';
import {Subscription} from 'rxjs';
import {StylesService} from '../../../_services/styles.service';
import {AdminService} from '../../../_services/admin.service';
import {Admin} from '../../../_models/Admin';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit, OnDestroy {
    elements;
    dropdown = false;
    collapse = false;
    collapseSubscription: Subscription;
    admin: Admin;
    constructor(private stylesService: StylesService, private adminService: AdminService) {
        this.admin = this.adminService.admin;
        this.collapseSubscription = this.stylesService.getCollapse().subscribe(collapse => this.collapse = collapse);
    }

    ngOnDestroy() {
        this.collapseSubscription.unsubscribe();
    }


    ngOnInit() {
        // const ps = new PerfectScrollbar('.ps');
        // // ps.update();
    }

}
