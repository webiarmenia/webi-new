// const mongoose = require('mongoose');
import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const servicePageSchema = new Schema({
    category : {
        type: String,
        required: true,
    },
    slug: {
        type: String,
        required: true,
    },
    grStart: {
        type: String,
        required: true,
    },
    grEnd: {
        type: String,
        required: true,
    },
    image: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: new Date()
    },
    updated: {
        type: Date,
        required: true,
        default: new Date()
    },
    description: {
        type: Object,
        default: {en: '', ru: '', am: ''}
    },
    title: {
        type: Object,
        default: {en: '', ru: '', am: ''}
    },
    details: {
        type: Object
    }
});


mongoose.model('servicePage', servicePageSchema);

const ServicePage = mongoose.model('servicePage');

module.exports = ServicePage;