import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const InvoiceInfo = new Schema({
    project: {
        type: mongoose.Schema.ObjectId,
        ref: 'Project',
        default: null
    },
    invoice: {
        type: mongoose.Schema.ObjectId,
        ref: 'Invoice',
        default: null
    },
    clientNotes: {
        type: String,
        default: null
    },
    clientAccepted: {
        type: Boolean,
        default: false
    },
    seen: {
        type: Boolean,
        default: false
    },
    show: {
        type: Boolean,
        default: false
    },
    invoicePdf : {
        type: String,
        default: null
    },
    hasUpdate : {
        type: Boolean,
        default : false
    },
    created: {
        type: Date,
        required: true,
        default: new Date()
    },
    updated: {
        type: Date,
        required: true,
        default: new Date()
    },
    refused : {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('InvoiceInfo', InvoiceInfo);