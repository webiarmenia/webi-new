import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const ContractInfo = new Schema({
    project: {
        type: mongoose.Schema.ObjectId,
        ref: 'Project',
        default: null
    },
    contract: {
        type: mongoose.Schema.ObjectId,
        ref: 'Contract',
        default: null
    },
    notes: {
        type: String,
        default: null
    },
    accepted: {
        type: Boolean,
        default: false
    },
    seen: {
        type: Boolean,
        default: false
    },
    pdf : {
        type: String,
        default: null
    },
    hasUpdate : {
        type: Boolean,
        default : false
    },
    show: {
        type: Boolean,
        default: false
    },
    created: {
        type: Date,
        required: true,
        default: new Date()
    },
    updated: {
        type: Date,
        required: true,
        default: new Date()
    },
});

module.exports = mongoose.model('ContractInfo', ContractInfo);