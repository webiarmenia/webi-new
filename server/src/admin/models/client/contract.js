import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const Contract = new Schema({
    content: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Contract', Contract);