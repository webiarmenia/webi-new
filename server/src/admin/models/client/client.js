import mongoose from 'mongoose';
import jwt from "jsonwebtoken";
import config from '../../../../config'

const Schema = mongoose.Schema;

const client = new Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        match: /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.(([0-9]{1,3})|([a-zA-Z]{2,3})|(aero|coop|info|museum|name))$/
    },
    companyName: {type: String, required: false},
    firstName: {type: String, required: true},
    lastName: {type: String, required: true},
    nikName: {type: String, required: true, unique: true},
    status: {type: String, default: null},
    password: {type: String, default: null},
    hasPassword: {type: Boolean, default: false},
    created : {type: Date, default: Date.now()},
    updated : {type: Date, default: Date.now()}
});

module.exports = mongoose.model('Client', client);

module.exports.getToken = function (client) {
    return jwt.sign({
            id: client._id,
            firstName: client.firstName,
            lastName: client.lastName,
            nikName: client.nikName,
            email: client.email,
            status: client.status
        },
        config.jwt_key, {expiresIn: '4h'});
};
