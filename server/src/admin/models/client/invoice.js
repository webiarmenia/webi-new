import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const Invoice = new Schema({
    fromName: {
        type: String,
        required: true
    },
    fromEmail: {
        type: String,
        required: true

    },
    fromAddress: {
        type: String,
        required: true
    },
    fromPhone: {
        type: String
    },
    fromBusiness: {
        type: String
    },
    toName: {
        type: String,
        required: true
    },
    toEmail: {
        type: String,
        required: true
    },
    toAddress: {
        type: String,
        required: true
    },
    toPhone: {
        type: String
    },
    number: {
        type: String
    },
    date: {
        type: String,
        required: true
    },
    due: {
        type: String,
        required: true
    },
    notes: {
        type: String,
    },
    fee: {
        type: Object,
        value: {
            name: {type: String},
            type: {type: String},
            value: {type: Number}
        }
    },
    details: {
        type: [Object],
        default: []
    },
    total : {
        type: Number
    },
    balance: {
        type: Number
    },
    currency : {
        type: Number,
        default: 0
    }
});

module.exports = mongoose.model('Invoice', Invoice);