import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const Project = new Schema({
    name: {type: String, required: true},
    slug: {type: String, required: true, unique : true},
    description: {type: String, required: true},
    priority: {type: Number, required: true},
    clientId: {type: mongoose.Schema.ObjectId, ref: 'Client', default: null},
    created: {type: Date, default: Date.now()},
    updated: {type: Date, default: Date.now()},
    notice: {type: Number, default: 1}
});

module.exports = mongoose.model('Project', Project);
