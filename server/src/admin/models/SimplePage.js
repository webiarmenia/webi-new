// const mongoose = require('mongoose');
import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const simplePageSchema = new Schema({
    slug: {
        type: String,
        required: true,
    },
    key: {
        type: String,
        default: null
    },
    title: {
        type: Object,
        default: {en: '', ru: '', am: ''}
    },
    shortDescription: {
        type: Object,
        default: {en: '', ru: '', am: ''}
    },

    content: {
        type: String
    },
    dirName: {
        type: String,
        default: null
    },
    created: {
        type: Date,
        default: new Date()
    },
    updated: {
        type: Date,
        required: true,
        default: new Date()
    }
});


const SimplePage = mongoose.model('servicePage');

module.exports = mongoose.model('simplePage', simplePageSchema);


