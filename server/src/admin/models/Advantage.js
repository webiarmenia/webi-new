import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const Advantage = new Schema({
    title: {
        en: {type: String, required: true,},
        ru: {type: String},
        am: {type: String}
    },
    description: {
        en: {type: String, required: true,},
        ru: {type: String},
        am: {type: String}

    },
    order: {type: Number, default: null},
});

module.exports = mongoose.model('advantages', Advantage);

