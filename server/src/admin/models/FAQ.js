import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const faqSchema = new Schema({
    shortDescription: {
        type: Object,
        default: {en: '', ru: '', am: ''}
    },
    talk: {
        type: Object
    },
    created: {
        type: Date,
        default: new Date()
    },
    updated: {
        type: Date,
        required: true,
        default: new Date()
    }

});


const FAQ = mongoose.model('faq', faqSchema);

module.exports = FAQ;