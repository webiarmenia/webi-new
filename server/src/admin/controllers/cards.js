import Card from '../models/Card';
const fs = require('fs');
import config from '../../../config'

module.exports = {
    getAll: (req, res) => {
        Card.find()
            .then(result => {
                res.status(200).json({
                    success: true,
                    data: result
                })
            })
            .catch(err => {
                return res.status(500).send({
                    success: false,
                    error: err.message,
                });
            })
    },
    getOne: (req, res) => {
        Card.findOne({_id: req.params.id})
            .then(result => {
                if (!result) {
                    res.status(404).json({
                        success: false,
                        msg: "Card not found"
                    })
                } else {
                    res.status(200).json({
                        success: true,
                        card: result
                    })
                }
            })
            .catch(err => {
                return res.status(500).send({
                    success: false,
                    error: err.message,
                });
            });
    },
    create: (req, res) => {
        if (!req.file) {
            return res.status(500).json({
                error: 'file required'
            })
        }

        // const card = {
        //     title: req.body.title,
        //     description: req.body.description,
        //     background: req.body.background,
        //     textColor: req.body.textColor,
        //     url: req.body.url
        // };

        const card = new Card({
            title: JSON.parse(req.body.title),
            description: JSON.parse(req.body.description),
            image: req.file.filename,
            textColor: req.body.textColor,
            background: req.body.background,
            url: req.body.url
        });

        card.save()
            .then(result => {
                res.status(200).json({
                    success: true,
                    card: result
                })
            })
            .catch(err => {
                return res.status(500).send({
                    success: false,
                    error: err.message,
                });
            });
    },
    update: (req, res) => {
        let update = req.body;

        if (!req.body) {
            return res.status(400).send({
                msg: "Card content can not be empty"
            });
        }else{
            if (req.file) {
                // console.log(1)
                update.image = req.file.filename;
            }
            if (req.body.description) {
                // console.log(2)
                update.description = JSON.parse(req.body.description)
            }
            if (req.body.title) {
                // console.log(3)
                update.title = JSON.parse(req.body.title)
            }
        }

        Card.findByIdAndUpdate({_id: req.params.id}, update)
            .then(result => {
                if (!result) {
                    res.status(404).json({
                        success: false,
                        error: "Card not found"
                    })
                } else {
                    if(req.file){
                        fs.unlink(`${config.upload_directory}/card/${result.image}`, (err) => {
                            if (err) {
                                console.log(err)
                            }
                        });
                    }
                    res.status(200).json({
                        success: true,
                        msg: "Card updated successfully!"
                    });
                }
            })
            .catch(err => {
                return res.status(500).send({
                    success: false,
                    error: err.message,
                });
            });
    },
    delete: (req, res) => {

        console.log(req.params.id)
        Card.findByIdAndRemove({_id: req.params.id})
            .then(result => {
                if (!result) {

                    res.status(404).json({
                        success: false,
                        msg: "Card not found"
                    })
                } else {
                    fs.unlink(`${config.upload_directory}/card/${result.image}`, (err) => {
                        if (err) {
                            console.log(err)
                        }
                    });
                    res.status(200).json({
                        success: true,
                        msg: "Card deleted successfully!",
                    });
                }
            })
            .catch(err => {
                return res.status(500).send({
                    success: false,
                    error: err.message,
                });
            });

    }
};
