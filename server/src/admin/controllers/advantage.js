// const Portfolio = require('../models/Portfolio');
const fs = require('fs');
import Advantage from '../models/Advantage'


module.exports.getAll = (req, res) => {
    Advantage.find({})
        .then(result => {
            res.status(200).json({
                success: true,
                advantages: result
            })
        })
        .catch(e => console.log(e))
};

module.exports.getOne = (req, res) => {
    Advantage.findOne({_id: req.params.id})
        .then(result => {
            if (!result) {
                res.status(404).json({
                    success: false,
                    msg: "Advantage not found with id " + req.params.id
                })
            } else {
                res.status(200).json({
                    success: false,
                    advantage: result
                })
            }
        })
        .catch(e => {
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });
};


module.exports.create = (req, res) => {

    const advantage = new Advantage({
        order: req.body.order,
        title: req.body.title,
        description: req.body.description,
    });
    advantage.save()
        .then(result => {
            res.status(201).json({
                success: true,
                portfolio: result
            })
        })
        .catch(err => {
            console.log('error');
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });
};

module.exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            msg: "Advantage content can not be empty"
        });
    }
    let update = req.body;

    Advantage.findByIdAndUpdate({_id: req.params.id}, update)
        .then(result => {
            if (!result) {
                res.status(404).json({
                    success: false,
                    error: "Advantage not found with id " + req.params.id
                })
            } else {

                res.status(200).json({
                    success: true,
                    msg: "Advantage deleted successfully!"
                });
            }
        })
        .catch(err => {
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });
};

module.exports.delete = (req, res) => {
    Advantage.findByIdAndRemove({_id: req.params.id})
        .then(result => {
            if (!result) {

                res.status(404).json({
                    success: false,
                    msg: "Advantage not found with id " + req.params.id
                })
            } else {
                res.status(200).json({
                    success: true,
                    msg: "Advantage deleted successfully!",
                    result: result
                });
            }
        })
        .catch(err => {
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });


};

