// const Portfolio = require('../models/Portfolio');
const fs = require('fs');
import Portfolio from '../models/Portfolio'
import config from '../../../config'

const rimraf = require("rimraf");


module.exports.getAll = (req, res) => {
    Portfolio.find({})
        .then(result => {
            res.status(200).json({
                success: true,
                portfolios: result
            })
        })
        .catch(e => console.log(e))
};

module.exports.getOne = (req, res) => {
    Portfolio.findOne({_id: req.params.id})
        .then(result => {
            if (!result) {
                res.status(404).json({
                    success: false,
                    msg: "Portfolio not found with id " + req.params.id
                })
            } else {
                res.status(200).json({
                    success: false,
                    portfolio: result
                })
            }
        })
        .catch(e => {
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });
};


module.exports.create = (req, res) => {
    // if (!req.files) {
    //     return res.status(500).json({
    //         success: false,
    //         msg: "error"
    //     })
    // }
    // let filenames = req.files.map(i => {
    //     return {
    //         filename: i.filename,
    //         type: i.fieldname,
    //         id: i.originalname
    //     }
    // });
    // console.log(filenames)
    // const image = filenames[filenames.length - 1].filename;

    // const details = JSON.parse(req.body.details);


    // for (let i = 0; i < filenames.length; ++i) {
    //     if (details.length !== 0 && details[filenames[i].id]) {
    //         if (details[filenames[i].id].type === 'cover') {
    //             details[filenames[i].id].img = filenames[i].filename
    //         } else if (details[filenames[i].id].type === 'imageWithText') {
    //             details[filenames[i].id].img = filenames[i].filename
    //         } else if (details[filenames[i].id].type === 'imageWithImage') {
    //
    //             if (filenames[i].type === 'img1') {
    //                 details[filenames[i].id].img1 = filenames[i].filename
    //             }
    //             if (filenames[i].type === 'img2') {
    //                 details[filenames[i].id].img2 = filenames[i].filename
    //             }
    //         }
    //     }
    // }

    // const portfolio = new Portfolio({
    //     url: req.body.url,
    //     slug: req.body.slug,
    //     sliderPosition: req.body.sliderPosition,
    //     title: JSON.parse(req.body.title),
    //     image: image,
    //     // description: JSON.parse(req.body.description),
    //     hover: JSON.parse(req.body.hover),
    //     shortDescription: JSON.parse(req.body.shortDescription),
    //     random: req.body.random,
    //     details: details
    // });
    // portfolio.save()
    //     .then(result => {
    //         res.status(201).json({
    //             success: true,
    //             portfolio: result
    //         })
    //     })
    //     .catch(err => {
    //         console.log('error');
    //         // res.end()
    //         return res.status(500).send({
    //             success: false,
    //             error: err.message,
    //         });
    //     });

    const portfolio = new Portfolio({
        // description: JSON.parse(req.body.description),
        url: req.body.url,
        slug: req.body.slug,
        sliderPosition: req.body.sliderPosition,
        title: req.body.title,
        image: req.body.imgURL,
        hover: req.body.hover,
        shortDescription: req.body.shortDescription,
        random: req.body.random,
        details: req.body.details
    });

    portfolio.save()
        .then(result => {
            res.status(201).json({
                success: true,
                portfolio: result
            })
        })
        .catch(err => {
            console.log('error');
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });

};

module.exports.update = (req, res) => {
    // console.log(req.files)
    // let filenames = req.files.map(i => {
    //     return {
    //         filename: i.filename,
    //         type: i.fieldname,
    //         id: i.originalname
    //     }
    // });
    // console.log(filenames)
    let update = req.body;

    // console.log(req.body)

    // const image = filenames[filenames.length - 1].filename;

    // const details = JSON.parse(req.body.details);

    // for (let i = 0; i < filenames.length; ++i) {
    //     if (details.length !== 0 && details[filenames[i].id]) {
    //         if (details[filenames[i].id].type === 'cover') {
    //             details[filenames[i].id].img = filenames[i].filename
    //         } else if (details[filenames[i].id].type === 'imageWithText') {
    //             details[filenames[i].id].img = filenames[i].filename
    //         } else if (details[filenames[i].id].type === 'imageWithImage') {
    //
    //             if (filenames[i].type === 'img1') {
    //                 details[filenames[i].id].img1 = filenames[i].filename
    //             }
    //             if (filenames[i].type === 'img2') {
    //                 details[filenames[i].id].img2 = filenames[i].filename
    //             }
    //         }
    //     }
    // }


    if (!req.body) {
        return res.status(400).send({
            msg: "Portfolio content can not be empty"
        });
    } else {
        // if (filenames.length > 0) {
        //     if (filenames[filenames.length - 1].id === '999') {
        //         update.image = filenames[filenames.length - 1].filename
        //
        //     }
        // }
        // if (req.body.description) {
        //     // console.log(2)
        //     update.description = JSON.parse(req.body.description)
        // }
        if (req.body.title) {
            // console.log(3)
            update.title = req.body.title
        }
        if (req.body.hover) {
            // console.log(3)
            update.hover = req.body.hover
        }
        if (req.body.shortDescription) {
            // console.log(3)
            update.shortDescription = req.body.shortDescription
        }
        if (req.body.imgURL) {
            // console.log(3)
            update.image = req.body.imgURL
        }

        if (req.body.details) {
            // console.log(3)
            update.details = req.body.details
        }

    }


    Portfolio.findOneAndUpdate({_id: req.params.id}, update)
        .then(result => {
            if (!result) {
                res.status(404).json({
                    success: false,
                    error: "Portfolio not found with id " + req.params.id
                })
            } else {

                // if (req.files) {
                //     const details = result.details;
                //
                //     if (details.length > 0) {
                //         for (let i = 0; i < filenames.length; ++i) {
                //             if (details[filenames[i].id]) {
                //                 if (details[filenames[i].id].type === 'imageWithImage') {
                //
                //                     if (filenames[i].type === 'img1') {
                //                         fs.unlink(`${config.upload_directory}/portfolio/${details[filenames[i].id].img1}`, (err) => {
                //                             if (err) {
                //                                 console.log(err)
                //                             }
                //                         });
                //                     }
                //                     if (filenames[i].type === 'img2') {
                //                         fs.unlink(`${config.upload_directory}/portfolio/${details[filenames[i].id].img2}`, (err) => {
                //                             if (err) {
                //                                 console.log(err)
                //                             }
                //                         });
                //                     }
                //                 } else if (details[filenames[i].id].type === 'cover') {
                //                     fs.unlink(`${config.upload_directory}/portfolio/${details[filenames[i].id].img}`, (err) => {
                //                         if (err) {
                //                             console.log(err)
                //                         }
                //                     });
                //                 } else if (details[filenames[i].id].type === 'imageWithText') {
                //                     fs.unlink(`${config.upload_directory}/portfolio/${details[filenames[i].id].img}`, (err) => {
                //                         if (err) {
                //                             console.log(err)
                //                         }
                //                     });
                //                 }
                //             }
                //         }
                //     }
                //     if (filenames[filenames.length - 1].id === '999') {
                //         fs.unlink(`${config.upload_directory}/portfolio/${result.image}`, (err) => {
                //             if (err) {
                //                 console.log(err)
                //             }
                //         });
                //     }
                // }
                res.status(200).json({
                    success: true,
                    msg: "Portfolio updated successfully!"
                });
            }
        })
        .catch(err => {
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });
};

module.exports.delete = (req, res) => {
    Portfolio.findOneAndDelete({_id: req.params.id})
        .then(result => {
            if (!result) {
                res.status(404).json({
                    success: false,
                    msg: "Portfolio not found with id " + req.params.id
                })
            } else {
                rimraf.sync(`${config.upload_directory}/portfolio/ckeditor/${result.random}`);
                res.status(200).json({
                    success: true,
                    msg: "Portfolio deleted successfully!",
                    result: result
                });
                // const details = result.details;
                // for (let i = 0; i < details.length; ++i) {
                //     if (details[i].type !== 'text') {
                //         if (details[i].img) {
                //             fs.unlink(`${config.upload_directory}/portfolio/${details[i].img}`, (err) => {
                //                 if (err) {
                //                     console.log(err)
                //                 }
                //             });
                //
                //         } else if (details[i].img1) {
                //             fs.unlink(`${config.upload_directory}/portfolio/${details[i].img1}`, (err) => {
                //                 if (err) {
                //                     console.log(err)
                //                 }
                //             });
                //             fs.unlink(`${config.upload_directory}/portfolio/${details[i].img2}`, (err) => {
                //                 if (err) {
                //                     console.log(err)
                //                 }
                //             });
                //         }
                //     }
                // }
                // fs.unlink(`${config.upload_directory}/portfolio/${result.image}`, (err) => {
                //     if (err) {
                //         console.log(err)
                //     }
                // });

            }
        })
        .catch(err => {
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });


};


module.exports.ckEditorAddImage = (req, res) => {
    res.status(201).json({
        filename: req.file.filename
    })
};

module.exports.ckEditorDeleteImage = (req, res) => {
    let name = req.query.name;
    fs.unlinkSync(`${config.upload_directory}/portfolio/ckeditor/${name}`);
    res.status(201).json({
        msg: 'CkImage has been removed'
    })
};
module.exports.deleteNoEmptyDir = (req, res) => {
    rimraf.sync(`${config.upload_directory}/portfolio/ckeditor/${req.params.dir}`);
}

