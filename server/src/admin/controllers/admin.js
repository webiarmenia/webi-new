import Admin from '../models/admin';
import bcrypt from 'bcryptjs';
import helper from '@helpers/functions';
import config from "../../../config";

const fs = require('fs');

module.exports = {
    getAll: (req, res, next) => {
        Admin.find({role: {$ne: 'superAdmin'}})
            .then(admins => {
                if (admins.length === 0) {
                    res.status(200).json({
                        success: true,
                        admins: []
                    })
                } else {
                    res.status(200).json({
                        success: true,
                        admins: admins.map(a => {
                            return {
                                id: a._id,
                                firstName: a.firstName,
                                lastName: a.lastName,
                                email: a.email,
                                role: a.role,
                                permissions: a.permissions,
                                avatar: a.avatar,
                            }
                        })
                    })
                }
            })
            .catch()
    },

    getOne: (req, res, next) => {
        Admin.findOne({_id: req.params.id})
            .then(admin => {
                if (!admin) {
                    res.status(409).json({
                        message: 'Not found with this id'
                    });
                } else {
                    res.status(200).json({
                        success: true,
                        admin: {
                            id: admin._id,
                            firstName: admin.firstName,
                            lastName: admin.lastName,
                            email: admin.email,
                            role: admin.role,
                            permissions: admin.permissions,
                            avatar: admin.avatar,
                        }
                    })
                }


            })
            .catch(e => {
                helper.errorHandler(res, e)

            })
    },
    create: (req, res, next) => {
        console.log('create');
        Admin.findOne({email: req.body.email})
            .then(admin => {
                if (admin) {
                    return res.status(409).json({
                        message: 'Admin already exist !'
                    });
                } else {
                    bcrypt.hash(req.body.password, 10, (err, hash) => {
                        if (err) {
                            console.log('else err');
                            res.status(500).json({error: "Incorrect Password"});
                        } else {
                            const admin = new Admin({
                                firstName: req.body.firstName,
                                lastName: req.body.lastName,
                                email: req.body.email,
                                password: hash,
                                role: req.body.role,
                                permissions: JSON.parse(req.body.permissions),
                                avatar: req.file.filename
                            });
                            admin.save()
                                .then(result => {
                                    res.status(201).json({message: 'admin created !'})
                                })
                                .catch(e => {
                                    helper.errorHandler(res, e)
                                });
                        }
                    })

                }
            })
            .catch(e => {
                helper.errorHandler(res, e)
            });
    },
    login: function (req, res, next) {
        Admin.find({email: req.body.email})
            .exec()
            .then(admin => {

                if (admin.length < 1) {
                    return res.status(401).json({
                        error: 'Auth failed'
                    });
                }
                bcrypt.compare(req.body.password, admin[0].password, (err, result) => {
                    if (err) {
                        return res.status(401).json({
                            error: 'Auth failed'
                        });
                    }
                    if (result) {
                        const token = Admin.getToken(admin[0]);
                        return res.status(200).json({success: true, token: token});
                    }
                    res.status(401).json({
                        error: 'Auth failed'
                    });
                })
            })
            .catch(e => {
                helper.errorHandler(res, e)
            });
    },
    update: (req, res, next) => {
        let updateObj = req.body;
        updateObj.permissions = JSON.parse(req.body.permissions);
        if (req.file) {
            updateObj.avatar = req.file.filename;
        }
        if (req.body.password) {
            updateObj.password = bcrypt.hashSync(req.body.password, 10);
        }
        if (updateObj.avatar === 'null') {
            updateObj.avatar = null;
        }

        Admin.findOneAndUpdate({_id: req.params.adminId}, updateObj)
            .exec()
            .then(result => {
                if (!result) {
                    res.status(404).json({
                        error: 'Not found'
                    })
                } else {
                    if (req.file) {
                        fs.unlink(`${config.upload_directory}/admin/${result.avatar}`, (err) => {
                            if (err) {
                                console.log(err)
                            }
                        });
                    }
                    res.status(200).json({
                        success: true,
                        msg: "Admin updated successfully!"
                    });
                }
            })
            .catch(e => {
                helper.errorHandler(res, e)
            })
    },
    delete: (req, res, next) => {

        Admin.findByIdAndDelete({_id: req.params.adminId})
            .exec()
            .then(r => {
                res.status(200).json({message: 'Admin deleted !'});
            })
            .catch(e => helper.errorHandler(res, e));


    }
};

