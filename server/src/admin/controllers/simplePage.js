// const Portfolio = require('../models/Portfolio');
const fs = require('fs');
import SimplePage from '../models/SimplePage'
import config from '../../../config'

const rimraf = require("rimraf");


module.exports.getAll = (req, res) => {
    SimplePage.find({})
        .then(result => {
            res.status(200).json({
                success: true,
                pages: result
            })
        })
        .catch(e => console.log(e))
};
//
// module.exports.getOne = (req, res) => {
//     ServicePage.findOne({_id: req.params.id})
//         .then(result => {
//             if (!result) {
//                 res.status(404).json({
//                     success: false,
//                     msg: "Page not found with id " + req.params.id
//                 })
//             } else {
//                 res.status(200).json({
//                     success: false,
//                     page: result
//                 })
//             }
//         })
//         .catch(e => {
//             return res.status(500).send({
//                 success: false,
//                 error: err.message,
//             });
//         });
// };


module.exports.create = (req, res) => {
    const simplePage = new SimplePage({
        slug: req.body.slug,
        key: req.body.key,
        title: req.body.title,
        shortDescription: req.body.shortDescription,
        dirName: req.body.dirName,
        content: req.body.content,
        created: Date.now(),
        updated: Date.now(),
    });
    // console.log(simplePage)


    simplePage.save()
        .then(result => {
            res.status(201).json({
                success: true,
                simple: result
            })
        })
        .catch(err => {
            console.log('error');
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });

};

module.exports.update = (req, res) => {
    let update = req.body;
    if (!req.body) {
        return res.status(400).send({
            msg: "SimplePage content can not be empty"
        });
    }

    SimplePage.findOneAndUpdate({_id: req.params.id}, update)
        .then(result => {
            if (!result) {
                res.status(404).json({
                    success: false,
                    error: "SimplePage not found with id " + req.params.id
                })
            } else {
                res.status(200).json({
                    success: true,
                    msg: "SimplePage updated successfully!"
                });
            }
        })
        .catch(err => {
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });
};

module.exports.delete = (req, res) => {


    // fs.unlink(`${config.upload_directory}/simplePage/oltibknwirmceu_88276532521568883946782.png`, (err) => {
    //     if (err) {
    //         console.log(err)
    //     }
    // });
    // res()

    // fxupqvnuydmceu_42212492531568884051187.png

    SimplePage.findOneAndDelete({_id: req.params.id})
        .then(result => {
            if (!result) {
                res.status(404).json({
                    success: false,
                    msg: "SimplePage not found with id " + req.params.id
                })
            } else {

                if (fs.existsSync(`${config.upload_directory}/page/editor/${result.dirName}`)) {
                    rimraf.sync(`${config.upload_directory}/page/editor/${result.dirName}`);
                }

                res.status(200).json({
                    success: true,
                    msg: "SimplePage deleted successfully!",
                    result: result
                });
            }
        })
        .catch(err => {
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });


};

