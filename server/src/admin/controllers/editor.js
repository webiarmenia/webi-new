// const Category = require('../models/Category');

const fs = require('fs');
import config from '../../../config'

// module.exports.create = (req, res) => {
//
//
//     res.status(200).json({
//         success: true,
//         fileName: req.body.random + req.file.originalname
//     })
// };
//
//
// module.exports.delete = (req, res) => {
//
// };

module.exports = {
    create: (req, res) => {

        res.status(200).json({
            success: true,
            fileName: req.file.filename
        })
    },
    delete: (req, res) => {
        console.log(req.body)

        if (req.body.length > 0) {

            req.body.forEach(name => {
                fs.unlink(`${config.upload_directory}/simplePage/${name}`, (err) => {

                    if (err) {
                        console.log(err)
                    }
                });
            })

        }

        res.status(200).json({
            success: true,
            // fileName: req.body.random + req.file.originalname
        })
    }
};
