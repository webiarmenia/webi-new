// const Portfolio = require('../models/Portfolio');
const fs = require('fs');
import ServicePage from '../models/ServicePage'
import config from '../../../config'

const rimraf = require("rimraf");


module.exports.getAll = (req, res) => {
    ServicePage.find({})
        .then(result => {
            res.status(200).json({
                success: true,
                pages: result
            })
        })
        .catch(e => console.log(e))
};

module.exports.getOne = (req, res) => {
    ServicePage.findOne({_id: req.params.id})
        .then(result => {
            if (!result) {
                res.status(404).json({
                    success: false,
                    msg: "Page not found with id " + req.params.id
                })
            } else {
                res.status(200).json({
                    success: false,
                    page: result
                })
            }
        })
        .catch(e => {
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });
};


module.exports.create = (req, res) => {

    const page = new ServicePage({
        grStart: req.body.grStart,
        grEnd: req.body.grEnd,
        slug: req.body.slug,
        category: req.body.category,
        title: req.body.title,
        image: req.body.imgURL,
        description: req.body.description,
        details: req.body.details,
        created: Date.now(),
        updated: Date.now(),
    });
    page.save()
        .then(result => {
            res.status(201).json({
                success: true,
                page: result
            })
        })
        .catch(err => {
            console.log('error');
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });

};

module.exports.update = (req, res) => {
    let update = req.body;
    if (!req.body) {
        return res.status(400).send({
            msg: "SimplePage content can not be empty"
        });
    }

    ServicePage.findOneAndUpdate({_id: req.params.id}, update)
        .then(result => {
            if (!result) {
                res.status(404).json({
                    success: false,
                    error: "ServicePage not found with id " + req.params.id
                })
            } else {
                res.status(200).json({
                    success: true,
                    msg: "ServicePage updated successfully!"
                });
            }
        })
        .catch(err => {
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });
};

module.exports.delete = (req, res) => {
    ServicePage.findOneAndDelete({_id: req.params.id})
        .then(result => {
            if (!result) {
                res.status(404).json({
                    success: false,
                    msg: "ServicePage not found with id " + req.params.id
                })
            } else {
                res.status(200).json({
                    success: true,
                    msg: "ServicePage deleted successfully!",
                    result: result
                });
            }
        })
        .catch(err => {
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });


};

