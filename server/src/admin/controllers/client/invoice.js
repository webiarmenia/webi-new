import Invoice from '../../models/client/invoice';
import InvoiceInfo from '../../models/client/invoiceInfo';
import Project from '../../models/client/project';
import helper from '@helpers/functions';
import bcrypt from 'bcryptjs';
import randomsPass from 'randomstring';


module.exports = {

    createInvoice(req, res, next) {
        new Invoice({
            fromName: req.body.fromName,
            fromEmail: req.body.fromEmail,
            fromAddress: req.body.fromAddress,
            fromPhone: req.body.fromPhone,
            fromBusiness: req.body.fromBusiness,
            toName: req.body.toName,
            toEmail: req.body.toEmail,
            toAddress: req.body.toAddress,
            toPhone: req.body.toPhone,
            number: req.body.number,
            date: req.body.date,
            due: req.body.due,
            notes: req.body.notes ? req.body.notes : null,
            fee: req.body.fee ? req.body.fee : null,
            total: req.body.total,
            balance: req.body.balance,
            details: req.body.details,
            currency: req.body.currency
        }).save()
            .then(invoice => {
                console.log(1111);
                new InvoiceInfo({
                    project: req.params.projectId,
                    invoice: invoice._id,
                    created: Date.now(),
                    updated: Date.now(),
                }).save()
                    .then(result => {
                        console.log(222);
                        res.status(201).json({success: true, message: 'Created'})
                    })
                    .catch(e => {
                        console.log(333);
                        helper.errorHandler(res, e)
                    })
            })
            .catch(e => {
                console.log(4444);
                helper.errorHandler(res, e)
            });
    },

    updateInvoice(req, res, next) {
        let obj = req.body;
        Invoice.findOneAndUpdate({_id: req.params.invoiceId}, obj)
            .exec()
            .then(invoice => {
                let updateObj = {
                    hasUpdate: true,
                    updated: Date.now()
                };
                InvoiceInfo.findOneAndUpdate({invoice: invoice._id}, updateObj)
                    .exec()
                    .then(
                        res.status(200).json({
                            success: true
                        })
                    )
                    .catch(e => helper.errorHandler(res, e))


            })
            .catch(e => helper.errorHandler(res, e))
    },

    updateInvoicePdf(req, res, next) {
        let updateObj = req.body;
        updateObj.hasUpdate = false;
        updateObj.seen = false;
        InvoiceInfo.findOneAndUpdate({invoice: req.params.invoiceId}, updateObj)
            .exec()
            .then(inv => {
                if (inv.seen) {
                    Project.findOne({_id: inv.project}).then(proj => {
                        proj.notice = proj.notice + 1;
                        proj.save();
                    });
                }
                res.status(200).json({
                    success: true
                })
            })
            .catch(e => helper.errorHandler(res, e))
    },

    getAll(req, res, next) {
        InvoiceInfo.find({project: req.params.projectId})
            .populate('invoice')
            .exec()
            .then(invoiceInfos => {
                if (invoiceInfos.length < 1) {
                    res.status(200).json({success: true, invoices: []})
                } else {
                    res.status(200).json({
                        success: true,
                        invoiceInfos: invoiceInfos.map(i => {
                            return {
                                id: i._id,
                                seen: i.seen,
                                clientNotes: i.clientNotes,
                                clientAccepted: i.clientAccepted,
                                invoicePdf: i.invoicePdf,
                                hasUpdate: i.hasUpdate,
                                refused: i.refused,
                                created: i.created,
                                updated: i.updated,
                                invoice: {
                                    id: i.invoice._id,
                                    fromName: i.invoice.fromName,
                                    fromEmail: i.invoice.fromEmail,
                                    fromAddress: i.invoice.fromAddress,
                                    fromPhone: i.invoice.fromPhone,
                                    fromBusiness: i.invoice.fromBusiness,
                                    toName: i.invoice.toName,
                                    toEmail: i.invoice.toEmail,
                                    toAddress: i.invoice.toAddress,
                                    toPhone: i.invoice.toPhone,
                                    number: i.invoice.number,
                                    date: i.invoice.date,
                                    due: i.invoice.due,
                                    notes: i.invoice.notes,
                                    fee: i.invoice.fee,
                                    details: i.invoice.details,
                                    balance: i.invoice.balance,
                                    total: i.invoice.total,
                                    pdfInvoice: i.invoice.pdfInvoice,
                                    currency: i.invoice.currency
                                }
                            }
                        })
                    })
                }
            })
            .catch(e => helper.errorHandler(res, e))
    },

    getOne(req, res, next) {
        InvoiceInfo.findOne({invoice: req.params.invoiceId})
            .populate('invoice')
            .exec()
            .then(invoiceInfo => {
                if (!invoiceInfo) {
                    res.status(200).json({success: true, invoice: []})
                } else {
                    res.status(200).json({
                        success: true,
                        invoiceInfo: {
                            id: invoiceInfo._id,
                            seen: invoiceInfo.seen,
                            clientNotes: invoiceInfo.clientNotes,
                            clientAccepted: invoiceInfo.clientAccepted,
                            invoicePdf: invoiceInfo.invoicePdf,
                            hasUpdate: invoiceInfo.hasUpdate,
                            refused: invoiceInfo.refused,
                            created: invoiceInfo.created,
                            updated: invoiceInfo.updated,
                        },
                        invoice: {
                            id: invoiceInfo.invoice._id,
                            fromName: invoiceInfo.invoice.fromName,
                            fromEmail: invoiceInfo.invoice.fromEmail,
                            fromAddress: invoiceInfo.invoice.fromAddress,
                            fromPhone: invoiceInfo.invoice.fromPhone,
                            fromBusiness: invoiceInfo.invoice.fromBusiness,
                            toName: invoiceInfo.invoice.toName,
                            toEmail: invoiceInfo.invoice.toEmail,
                            toAddress: invoiceInfo.invoice.toAddress,
                            toPhone: invoiceInfo.invoice.toPhone,
                            number: invoiceInfo.invoice.number,
                            date: invoiceInfo.invoice.date,
                            due: invoiceInfo.invoice.due,
                            notes: invoiceInfo.invoice.notes,
                            fee: invoiceInfo.invoice.fee,
                            details: invoiceInfo.invoice.details,
                            balance: invoiceInfo.invoice.balance,
                            total: invoiceInfo.invoice.total,
                            pdfInvoice: invoiceInfo.invoice.pdfInvoice,
                            currency: invoiceInfo.invoice.currency
                        }
                    })
                }
            })
            .catch(e => helper.errorHandler(res, e))
    },

    deleteInvoice(req, res, next) {

    },
    sendMail(req, res, next) {
        // var base64Data = req.rawBody.replace(/^data:image\/png;base64,/, "");


        // require("fs").writeFile("out.png", base64Data, 'base64', function(err) {
        //     console.log(err);
        // });
    }

};
