import Contract from '../../models/client/contract';
import ContractInfo from '../../models/client/contractInfo';
import helper from '@helpers/functions';
import bcrypt from 'bcryptjs';
import randomsPass from 'randomstring';
import Project from "../../models/client/project";


module.exports = {

    createContract(req, res, next) {
        new Contract({
            content: req.body.content,

        }).save()
            .then(contract => {
                new ContractInfo({
                    project: req.params.projectId,
                    contract: contract._id,
                    created: Date.now(),
                    updated: Date.now(),
                }).save()
                    .then(result => {
                        console.log(222);
                        res.status(201).json({success: true, message: 'Created'})
                    })
                    .catch(e => {
                        console.log(333);
                        helper.errorHandler(res, e)
                    })
            })
            .catch(e => {
                console.log(4444);
                helper.errorHandler(res, e)
            });
    },

    updateContract(req, res, next) {
        let obj = req.body;
        Contract.findOneAndUpdate({_id: req.params.contractId}, obj)
            .exec()
            .then(contract => {
                let updateObj = {
                    hasUpdate: true,
                    created: Date.now()
                };
                ContractInfo.findOneAndUpdate({contract: contract._id}, updateObj)
                    .exec()
                    .then(
                        res.status(200).json({
                            success: true
                        })
                    )
                    .catch(e => helper.errorHandler(res, e))


            })
            .catch(e => helper.errorHandler(res, e))
    },

    updateContractPdf(req, res, next) {
        let updateObj = req.body;
        updateObj.hasUpdate = false;
        updateObj.seen = false;
        ContractInfo.findOneAndUpdate({contract: req.params.contractId}, updateObj)
            .exec()
            .then( cont => {
                if ( cont.seen ) {
                    Project.findOne({_id: cont.project}).then(proj => {
                        proj.notice = proj.notice + 1;
                        proj.save();
                    });
                }
                res.status(200).json({
                    success: true
                })
            })
            .catch(e => helper.errorHandler(res, e))
    },

    getAll(req, res, next) {
        ContractInfo.find({project: req.params.projectId})
            .populate('contract')
            .exec()
            .then(contractInfos => {
                if (contractInfos.length < 1) {
                    res.status(200).json({success: true, contractInfos: []})
                } else {
                    res.status(200).json({
                        success: true,
                        contractInfos: contractInfos.map(i => {
                            return {
                                id: i._id,
                                seen: i.seen,
                                notes: i.notes,
                                accepted: i.accepted,
                                pdf: i.pdf,
                                hasUpdate: i.hasUpdate,
                                created: i.created,
                                updated: i.updated,
                                contract: {
                                    id: i.contract._id,
                                    content: i.contract.content,
                                }
                            }
                        })
                    })
                }
            })
            .catch(e => helper.errorHandler(res, e))
    },

    getOne(req, res, next) {
        ContractInfo.findOne({contract: req.params.contractId})
            .populate('contract')
            .exec()
            .then(contractInfo => {
                if (!contractInfo) {
                    res.status(200).json({success: true, contract: null})
                } else {
                    res.status(200).json({
                        success: true,
                        contractInfo: {
                            id: contractInfo._id,
                            seen: contractInfo.seen,
                            notes: contractInfo.notes,
                            accepted: contractInfo.accepted,
                            invoicePdf: contractInfo.invoicePdf,
                            hasUpdate: contractInfo.hasUpdate,
                            created: contractInfo.created,
                            updated: contractInfo.updated,
                            pdf: contractInfo.pdf,

                        },
                        contract: {
                            id: contractInfo.contract._id,
                            content: contractInfo.contract.content,
                        }
                    })
                }
            })
            .catch(e => helper.errorHandler(res, e))
    },

    deleteContract(req, res, next) {

    },
    sendMail(req, res, next) {
        // var base64Data = req.rawBody.replace(/^data:image\/png;base64,/, "");


        // require("fs").writeFile("out.png", base64Data, 'base64', function(err) {
        //     console.log(err);
        // });
    }

};
