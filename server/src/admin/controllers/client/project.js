import Project from '../../models/client/project';
import helper from '@helpers/functions';
import bcrypt from 'bcryptjs';
import randomsPass from 'randomstring';


module.exports = {

    createProject(req, res, next) {
        console.log(req.body)
        new Project({
            name: req.body.name,
            description: req.body.description,
            priority: req.body.priority,
            clientId: req.body.clientId,
            slug: req.body.slug,
            created: Date.now(),
            updated: Date.now()
        }).save()
            .then(result => {
                console.log(222222)
                res.status(201).json({success: true, message: 'Created'})
            })
            .catch(e => {
                console.log(33333)
                helper.errorHandler(res, e)
            });
    },

    updateProject(req, res, next) {
        let obj = req.body;
        obj.updated = Date.now();
        Project.findOneAndUpdate({_id: req.params.projectId}, obj)
            .exec()
            .then(project => {
                res.status(200).json({
                    success: true
                })
            })
            .catch(e => helper.errorHandler(res, e))
    },

    getAll(req, res, next) {
        Project.find({clientId: req.params.clientId})
            .exec()
            .then(projects => {
                if (projects.length < 1) {
                    res.status(200).json({success: true, projects: []})
                } else {
                    res.status(200).json({
                        success: true,
                        projects: projects.map(p => {
                            return {
                                id: p._id,
                                name: p.name,
                                description: p.description,
                                priority: p.priority,
                                slug: p.slug
                            }
                        })
                    })
                }
            })
            .catch(e => helper.errorHandler(res, e))
    },

    getOne(req, res, next) {
        Project.findOne({_id: req.params.projectId})
            .exec()
            .then(project => {
                if (!project) {
                    res.status(200).json({success: true, project: null})
                } else {
                    res.status(200).json({
                        success: true,
                        project: {
                            id: project._id,
                            name: project.name,
                            description: project.description,
                            priority: project.priority,
                            slug: project.slug,
                        }
                    })
                }
            })
            .catch(e => helper.errorHandler(res, e))
    },

    deleteProject(req, res, next) {

    }

};
