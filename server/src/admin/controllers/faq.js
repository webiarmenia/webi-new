// const Portfolio = require('../models/Portfolio');
const fs = require('fs');
import FAQ from '../models/FAQ'
import config from '../../../config'

const rimraf = require("rimraf");


module.exports.getAll = (req, res) => {
    FAQ.find({})
        .then(result => {
            res.status(200).json({
                success: true,
                faqs: result
            })
        })
        .catch(e => console.log(e))
};
//
// module.exports.getOne = (req, res) => {
//     ServicePage.findOne({_id: req.params.id})
//         .then(result => {
//             if (!result) {
//                 res.status(404).json({
//                     success: false,
//                     msg: "Page not found with id " + req.params.id
//                 })
//             } else {
//                 res.status(200).json({
//                     success: false,
//                     page: result
//                 })
//             }
//         })
//         .catch(e => {
//             return res.status(500).send({
//                 success: false,
//                 error: err.message,
//             });
//         });
// };


module.exports.create = (req, res) => {
    const faq = new FAQ({
        shortDescription: req.body.shortDescription,
        talk: req.body.talk,
        created: Date.now(),
        updated: Date.now(),
    });
    // console.log(simplePage)

    faq.save()
        .then(result => {
            res.status(201).json({
                success: true,
                faq: result
            })
        })
        .catch(err => {
            console.log('error');
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });

};

module.exports.update = (req, res) => {
    let update = req.body;
    if (!req.body) {
        return res.status(400).send({
            msg: "FAQ content can not be empty"
        });
    }

    FAQ.findOneAndUpdate({_id: req.params.id}, update)
        .then(result => {
            if (!result) {
                res.status(404).json({
                    success: false,
                    error: "FAQ not found with id " + req.params.id
                })
            } else {
                res.status(200).json({
                    success: true,
                    msg: "FAQ updated successfully!"
                });
            }
        })
        .catch(err => {
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });
};

module.exports.delete = (req, res) => {

    FAQ.findOneAndDelete({_id: req.params.id})
        .then(result => {
            if (!result) {
                res.status(404).json({
                    success: false,
                    msg: "FAQ not found with id " + req.params.id
                })
            } else {

                res.status(200).json({
                    success: true,
                    msg: "FAQ deleted successfully!",
                    result: result
                });
            }
        })
        .catch(err => {
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });


};

