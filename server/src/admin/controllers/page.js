// const Page = require('../models/Page');
const fs = require('fs');
import Page from '../models/Page'
const rimraf = require("rimraf");
import config from '../../../config'

module.exports.getAll = (req, res) => {
    const pages = Page.find({})
        .then(result => {
            res.status(200).json({
                success: true,
                data: result
            })
        })
        .catch(e => console.log(e))
};

module.exports.getOne = (req, res) => {
    Page.findOne({_id: req.params.id})
        .then(result => {
            if (!result) {
                res.status(404).json({
                    success: false,
                    msg: "Page not found with id " + req.params.id
                })
            } else {
                res.status(200).json({
                    success: false,
                    portfolio: result
                })
            }
        })
        .catch(e => {
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });
};

module.exports.create = (req, res) => {

    const comps = JSON.parse(req.body.components).map((c, i) => {
        return {
            order: i,
            type: c.type,
            content: {}
        }
    });

    const page = new Page({
        title: JSON.parse(req.body.title),
        description: JSON.parse(req.body.description),
        components: comps,
        // banner: req.file.filename,
        random: req.body.random,
        slug: req.body.slug
    });
    page.save()
        .then(result => {
            res.status(201).json({
                success: true,
                page: result
            })
        })
        .catch(err => {
            console.log('error');
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });
};


module.exports.update = async (req, res) => {
    let update = req.body;

    if (!req.body) {
        return res.status(400).send({
            msg: "Portfolio content can not be empty"
        });
    } else {
        if (req.file) {
            // console.log(1)
            update.banner = req.file.filename;
        }
        if (req.body.description) {
            // console.log(2)
            update.description = JSON.parse(req.body.description)
        }
        if (req.body.title) {
            // console.log(3)
            update.title = JSON.parse(req.body.title)
        }
        if (req.body.content) {
            // console.log(3)
            update.content = JSON.parse(req.body.content)
        }
    }

    Page.findByIdAndUpdate({_id: req.params.id}, update)
        .then(result => {
            if (!result) {
                res.status(404).json({
                    success: false,
                    error: "Page not found with id " + req.params.id
                })
            } else {
                if (req.file) {
                    fs.unlink(`${config.upload_directory}/page/${result.banner}`, (err) => {
                        if (err) {
                            console.log(err)
                        }
                    });
                }
                res.status(200).json({
                    success: true,
                    msg: "Page deleted successfully!"
                });
            }
        })
        .catch(err => {
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });
};

module.exports.delete = (req, res) => {
    Page.findByIdAndRemove({_id: req.params.id})
        .then(result => {
            if (!result) {

                res.status(404).json({
                    success: false,
                    msg: "Page not found with id " + req.params.id
                })
            } else {
                fs.unlink(`${config.upload_directory}/page/${result.banner}`, (err) => {
                    if (err) {
                        console.log(err)
                    }
                });
                rimraf.sync(`${config.upload_directory}/page/ckeditor/${result.random}`);

                res.status(200).json({
                    success: true,
                    msg: "Page deleted successfully!",
                    result: result
                });
            }
        })
        .catch(err => {
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });


};

module.exports.ckEditorAddImage = (req, res) => {
    res.status(201).json({
        filename: req.file.filename
    })
}

module.exports.ckEditorDeleteImage = (req, res) => {
    let name = req.query.name;
    fs.unlinkSync(`${config.upload_directory}/page/ckeditor/${name}`);
    res.status(201).json({
        msg: 'CkImage has been removed'
    })
};
module.exports.deleteNoEmptyDir = (req, res) => {
    rimraf.sync(`${config.upload_directory}/page/ckeditor/${req.params.dir}`);
};

