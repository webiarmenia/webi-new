import express from 'express';
import servicePage from '@admin/controllers/servicePage';
import upload from '@admin/middleware/multer';


const router = express.Router();

router.get('/:id', servicePage.getOne);
router.get('/', servicePage.getAll);
router.post('/', servicePage.create);
router.put('/:id', servicePage.update);
router.delete('/:id', servicePage.delete);


module.exports = router;
