import express from 'express';
import simplePage from '@admin/controllers/simplePage';
import upload from '@admin/middleware/multer';


const router = express.Router();

// router.get('/:id', simplePage.getOne);
router.get('/', simplePage.getAll);
router.post('/', simplePage.create);
router.put('/:id', simplePage.update);
router.delete('/:id', simplePage.delete);


module.exports = router;
