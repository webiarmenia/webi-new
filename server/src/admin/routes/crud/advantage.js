import express from 'express';
import advantage from '@admin/controllers/advantage';


const router = express.Router();


router.get('/', advantage.getAll);
router.get('/:id', advantage.getOne);
router.post('/', advantage.create);
router.put('/:id', advantage.update);
router.delete('/:id', advantage.delete);


module.exports = router;