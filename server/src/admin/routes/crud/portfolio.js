import express from 'express';
import portfolio from '@admin/controllers/portfolio';
import upload from '@admin/middleware/multer';


const router = express.Router();

router.get('/', portfolio.getAll);
router.get('/:id', portfolio.getOne);
router.post('/ckeditor',upload.single('image'), portfolio.ckEditorAddImage);
// router.post('/',upload.any(), portfolio.create);
// router.put('/:id', upload.any(), portfolio.update);

router.post('/', portfolio.create);

router.put('/:id', portfolio.update);
router.delete('/ckeditor',portfolio.ckEditorDeleteImage);
router.delete('/ck/:dir', portfolio.deleteNoEmptyDir);
router.delete('/:id', portfolio.delete);


module.exports = router;