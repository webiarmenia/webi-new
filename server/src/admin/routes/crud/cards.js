import express from 'express';
// import cards from '@admin/controllers/cards-type-one';
import cards from '@admin/controllers/cards';
import upload from '@admin/middleware/multer';


const router = express.Router();


router.get('/', cards.getAll);
router.get('/:id', cards.getOne);
router.post('/',upload.single('img'), cards.create);

router.put('/:id',upload.single('img'), cards.update);
router.delete('/:id', cards.delete);


module.exports = router;