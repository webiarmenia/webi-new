import express from 'express';
import faq from '@admin/controllers/faq';
import upload from '@admin/middleware/multer';


const router = express.Router();

// router.get('/:id', simplePage.getOne);
router.get('/', faq.getAll);
router.post('/', faq.create);
router.put('/:id', faq.update);
router.delete('/:id', faq.delete);


module.exports = router;
