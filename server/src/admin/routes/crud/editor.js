import express from 'express';
import editor from '@admin/controllers/editor';
import upload from '@admin/middleware/multer';


const router = express.Router();

// router.get('/:id', simplePage.getOne);
// router.get('/', simplePage.getAll);
router.post('/:type', upload.single('image'), editor.create);
// router.put('/:id', simplePage.update);
router.delete('/', editor.delete);


module.exports = router;
