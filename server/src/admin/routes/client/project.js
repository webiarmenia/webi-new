import express from "express";
import project from '../../controllers/client/project'

const router = express.Router();

router.post('/', project.createProject);
router.get('/:clientId', project.getAll);
// router.get('/one/:projectId', project.getOne);
router.get('/one/:projectId', project.getOne);
router.put('/:projectId', project.updateProject);

module.exports = router;