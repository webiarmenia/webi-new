import express from "express";
import invoice from '../../controllers/client/invoice'

const router = express.Router();

// router.post('/mail', invoice.sendMail);
router.post('/:projectId', invoice.createInvoice);
router.get('/:projectId', invoice.getAll);
router.get('/one/:invoiceId', invoice.getOne);
router.put('/:invoiceId', invoice.updateInvoice);
router.put('/pdf/:invoiceId', invoice.updateInvoicePdf);
router.delete('/:invoiceId', invoice.deleteInvoice);


module.exports = router;