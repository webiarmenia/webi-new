import express from "express";

import client from '../../controllers/client/client'

import projectRoutes from './project';
import invoiceRoutes from './invoice';
import contractRoutes from './contract';

const router = express.Router();


router.post('/', client.createAccount);
router.get('/', client.getAll);
router.get('/:nikName', client.getByNikName);
router.put('/:id', client.updateClient);
router.get('/password/:id', client.createPasswordAndSend);


router.use('/project', projectRoutes);
router.use('/invoice', invoiceRoutes);
router.use('/contract', contractRoutes);






module.exports = router;