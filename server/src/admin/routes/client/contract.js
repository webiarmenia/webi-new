import express from "express";
import contract from '../../controllers/client/contract'

const router = express.Router();

// router.post('/mail', invoice.sendMail);
router.post('/:projectId', contract.createContract);
router.get('/:projectId', contract.getAll);
router.get('/one/:contractId', contract.getOne);
router.put('/:contractId', contract.updateContract);
router.put('/pdf/:contractId', contract.updateContractPdf);
router.delete('/:contractId', contract.deleteContract);


module.exports = router;