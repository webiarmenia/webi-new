import express from 'express';

import checkAuth from '../middleware/check-auth';

import controller from '../controllers/admin';
import media from './crud/media';
import advantage from './crud/advantage';
import page from './crud/page';
import portfolio from './crud/portfolio';
import servicePage from './crud/servicePage';
import simplePage from './crud/simplePage';
import faq from './crud/faq';
import editor from './crud/editor';
import setting from './crud/setting';
import team from './crud/team';
import menu from './crud/menu';
import category from './crud/category';
import language from './crud/language';
import news from './crud/news';
import serviceCards from './crud/cards';
import client from './client/client';
// import multer from "multer";

import upload from '@admin/middleware/multer';

const router = express.Router();

// const storage = multer.diskStorage({
//     destination: function (req, file, cb) {
//         if (req.url.indexOf('invitation') < 0)
//             cb(null, './src/_uploads/users');
//         else
//             cb(null, './src/_uploads/organization');
//     },
//     filename: function (req, file, cb) {
//         cb(null, (`${Date.now()}-${file.originalname}`))
//     }
// });
// const upload = multer({storage: storage});

//==========================================================================
//=======================  Admin CRUD  =====================================
//==========================================================================

router.post('/login', controller.login);
router.use('/', checkAuth);
// Grouping routes
router.post('/create', upload.single('avatar'), controller.create);
router.get('/', controller.getAll);
router.get('/one/:id', controller.getOne);
router.put('/update/:adminId', upload.single('avatar'), controller.update);
router.delete('/delete/:adminId', controller.delete);


router.use('/media', media);
router.use('/advantage', advantage);
router.use('/page', page);
router.use('/portfolio', portfolio);
router.use('/service-page', servicePage);
router.use('/simple-page', simplePage);
router.use('/faq', faq);
router.use('/editor', editor);
router.use('/setting', setting);
router.use('/team', team);
router.use('/menu', menu);
router.use('/category', category);
router.use('/language', language);
router.use('/news', news);
router.use('/cards-type-one', serviceCards);
router.use('/client', client);


module.exports = router;
