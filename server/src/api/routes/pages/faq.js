import express from 'express';
import faq from '../../controllers/pages/faq'

const router = express.Router();

router.get('/', faq.getAll);

module.exports = router;