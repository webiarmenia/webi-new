import express from 'express';
import simple from '../../controllers/pages/simple'

const router = express.Router();

router.get('/', simple.getAll);
router.get('/:slug', simple.getOne);

module.exports = router;