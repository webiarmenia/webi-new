import express from 'express';
import service from '../../controllers/pages/service'

const router = express.Router();

router.get('/', service.getAll);
router.get('/:slug', service.getOne);

module.exports = router;