import FAQ from '../../../admin/models/FAQ'


module.exports.getAll = (req, res) => {
    FAQ.find({})
        .then(result => {
            // res.status(200).json({
            //     success: true,
            //     faqs: result.filter(f => {
            //         f.talk.
            //     })
            // })

            result.forEach(faq => {
                faq.talk = faq.talk.filter(t => {
                    return t.show === true;
                })
            });

            res.json({
                success: true,
                pages: result
            })
        })
        .catch(e => console.log(e))
};

