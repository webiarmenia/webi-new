import SimplePage from '../../../admin/models/SimplePage'



module.exports.getAll = (req, res) => {
    SimplePage.find({})
        .then(result => {
            res.status(200).json({
                success: true,
                pages: result
            })
        })
        .catch(e => console.log(e))
};

module.exports.getOne = (req, res) => {
    SimplePage.findOne({slug: req.params.slug})
        .then(result => {
            if (!result) {
                res.status(404).json({
                    success: false,
                    msg: "Simple Page not found with id " + req.params.slug
                })
            } else {
                res.status(200).json({
                    success: false,
                    page: result
                })
            }
        })
        .catch(e => {
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });z
};