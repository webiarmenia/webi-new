import ServicePage from '../../../admin/models/ServicePage'



module.exports.getAll = (req, res) => {
    ServicePage.find({})
        .then(result => {
            res.status(200).json({
                success: true,
                pages: result
            })
        })
        .catch(e => console.log(e))
};

module.exports.getOne = (req, res) => {
    ServicePage.findOne({slug: req.params.slug})
        .then(result => {
            if (!result) {
                res.status(404).json({
                    success: false,
                    msg: "Page not found with id " + req.params.slug
                })
            } else {
                res.status(200).json({
                    success: false,
                    page: result
                })
            }
        })
        .catch(e => {
            return res.status(500).send({
                success: false,
                error: err.message,
            });
        });
};