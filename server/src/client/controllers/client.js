import Client from '../../admin/models/client/client'
import Project from '../../admin/models/client/project'
import Invoice from '../../admin/models/client/invoiceInfo'
import Contract from '../../admin/models/client/contractInfo'
import helper from '@helpers/functions';
import bcrypt from 'bcryptjs';


module.exports = {

    login(req, res, next) {
        Client.findOne({email: req.body.email, nikName: req.body.nikName})
            .exec()
            .then(client => {
                if (!client) {
                    return res.status(404).json({
                        message: 'No such client'
                    });
                }
                bcrypt.compare(req.body.password, client.password, (err, result) => {
                    if (err) {
                        return res.status(401).json({
                            message: 'Auth failed'
                        });
                    }
                    if (result) {
                        const token = Client.getToken(client);
                        return res.status(200).json({success: true, token: token});
                    }
                    res.status(401).json({
                        message: 'Auth failed'
                    });
                })
            })
            .catch(e => helper.errorHandler(res, e))
    },

    changePass(req, res, next) {
        Client.findOne({_id: req.client.id})
            .exec()
            .then(client => {
                if (client) {
                    bcrypt.compare(req.body.oldPassword, client.password, (err, result) => {
                        if (err) {
                            return res.status(401).json({
                                message: 'Auth failed'
                            });
                        }
                        if (result) {
                            console.log(55);
                            bcrypt.hash(req.body.password, 10, (err, hash) => {
                                if (err) {
                                    res.status(500).json({error: err});
                                } else {
                                    client.password = hash;
                                    client.save()
                                        .then(result => {
                                            if (result) {
                                                return res.status(200).json(result)
                                            }
                                        })
                                        .catch(e => {
                                            helper.errorHandler(res, e)
                                        });
                                }
                            })
                        } else {
                            return res.status(401).json({
                                message: 'Auth failed'
                            });
                        }
                    })

                } else {
                    res.status(500).json({
                        success: false,
                        error: 'something wrong'
                    })
                }
            })
            .catch(e => helper.errorHandler(res, e))
    },

    getProjects(req, res, next) {
        Project.find({clientId: req.params.clientId})
            .then(projects => {
                if (projects.length > 0) {
                    res.status(200).json(projects.map(pr => {
                        return {
                            id: pr._id,
                            slug: pr.slug,
                            name: pr.name,
                            description: pr.description,
                            created: pr.created,
                            updated: pr.updated,
                            notice: pr.notice
                        }
                    }));
                } else {
                    res.status(404).json({
                        message: 'No products'
                    })
                }
            })
            .catch(e => next(e))
    },

    getInvoices(req, res, next) {
        Invoice.find({project: req.params.projectId, show: true})
            .then(invoices => {
                if (invoices.length > 0) {
                    res.status(200).json(invoices.map(inv => {
                        return {
                            id: inv._id,
                            project: inv.project,
                            invoicePdf: inv.invoicePdf,
                            clientAccepted: inv.clientAccepted,
                            refused: inv.refused,
                            clientNotes: inv.clientNotes,
                            hasUpdate: inv.hasUpdate,
                            seen: inv.seen,
                            created: inv.created,
                            updated: inv.updated,
                        }
                    }));
                } else {
                    res.status(404).json({
                        message: 'No invoice'
                    })
                }
            })
            .catch(e => next(e))
    },

    updateInvoice(req, res, next) {
        Invoice.findByIdAndUpdate({_id: req.params.id}, req.body)
            .then( inv=> {
                if (inv) {
                    res.status(200).json({
                        id: inv._id,
                        project: inv.project,
                        invoicePdf: inv.invoicePdf,
                        clientAccepted: inv.clientAccepted,
                        refused: inv.refused,
                        clientNotes: inv.clientNotes,
                        hasUpdate: inv.hasUpdate,
                        seen: inv.seen,
                        created: inv.created,
                        updated: inv.updated,
                    });
                } else {
                    res.status(200).json([]);
                }
            })
            .catch(e => next(e))
    },
    getContracts(req, res, next) {
        console.log('------------', req.params.projectId);
        Contract.find({project: req.params.projectId, show: true})
            .then(contracts => {
                if (contracts.length > 0) {
                    res.status(200).json(contracts.map(contract => {
                        return {
                            id: contract._id,
                            project: contract.project,
                            pdf: contract.pdf,
                            accepted: contract.accepted,
                            // refused: inv.refused,
                            notes: contract.notes,
                            hasUpdate: contract.hasUpdate,
                            seen: contract.seen,
                            created: contract.created,
                            updated: contract.updated,
                        }
                    }));
                } else {
                    res.status(200).json([])
                }
            })
            .catch(e => next(e))
    },

    updateContract(req, res, next) {
        Contract.findByIdAndUpdate({_id: req.params.id}, req.body)
            .then( contract=> {
                if (contract) {
                    res.status(200).json({
                        id: contract._id,
                        project: contract.project,
                        pdf: contract.pdf,
                        accepted: contract.accepted,
                        // refused: inv.refused,
                        notes: contract.notes,
                        hasUpdate: contract.hasUpdate,
                        seen: contract.seen,
                        created: contract.created,
                        updated: contract.updated,
                    });
                } else {
                    res.status(200).json([]);
                }
            })
            .catch(e => next(e))
    },

    async getNotice(projId) {
        try {
            return 5;
        } catch (e) {

        }
    }

};
