import express from 'express';
import client from '../controllers/client';
// import invoice from '../controllers/invoice';
import checkAuth from '../_middleware/client-check-auth';

const router = express.Router();


router.post('/login', client.login);

router.use('/', checkAuth);

router.post('/change-password', client.changePass);

router.get('/projects/:clientId', client.getProjects);

router.get('/projects/invoices/:projectId', client.getInvoices);

router.post('/projects/update-invoice/:id', client.updateInvoice);

router.get('/projects/contracts/:projectId', client.getContracts);

router.post('/projects/update-contract/:id', client.updateContract);

router.post('/change-password', client.changePass);

// router.post('/invoice', invoice.create);



module.exports = router;
