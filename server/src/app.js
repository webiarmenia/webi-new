import express from 'express';
import bodyParser from 'body-parser';

import morgan from 'morgan';
import cors from 'cors';

import api from './api/routes/api';
import adminRoutes from './admin/routes/admin';
import clientRoutes from './client/routes/client';



import setting from './admin/controllers/setting'


var CronJob = require('cron').CronJob;


import './services/mongo'

import debug from 'debug';

const log = debug('app');
const logError = debug('app:error');
const logInfo = debug('app:info');

require('./seed').createDef(); //Creat default tables

const app = express();



app.use(morgan('combined', { stream: {write: logInfo} }));
app.use(cors());

// app.use(bodyParser.urlencoded({extended: false}));
// app.use(bodyParser.json());

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));


app.use('/uploads', express.static(__dirname + '/../_uploads'));
app.use('/admin', adminRoutes);
app.use('/api', api);
app.use('/client', clientRoutes);

//----------- Connect to Angular client

app.use('/admin-panel', express.static(__dirname + '/../../admin/dist/Client'));
app.use('/admin-panel/*', express.static(__dirname + '/../../admin/dist/Client'));

app.use('/', express.static(__dirname + '/../../front/dist/webiFront'));
app.use('/*', express.static(__dirname + '/../../front/dist/webiFront'));

// new CronJob('* *59 *23 * * *', function() {
// new CronJob('*/15 * * * * *', function() {
//     setting.backup()
//     // console.log(1111)
// }, null, true, 'America/Los_Angeles');

app.use((req, res, next) => {
    const err = new Error('Not found');
    err.status = 404;
    res.status(err.status).json({error: err.message});
    next(err);
});
app.use((err, req, res, next) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    // res.locals.error = req.app.get('env') === 'development' ? err : {};
// add this line to include winston logging
//     logError(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
    console.log('appp', err);
    logError(`${err.status || 500} - ${err.message} `);
    res.status(err.statusCode).json({error: err.message});
});

module.exports = app;
