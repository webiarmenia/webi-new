import mongoose from "mongoose";
import {mongodb_url} from '../../config'


mongoose.connect(mongodb_url, {useNewUrlParser: true, useCreateIndex: true})
    .then(_ => {
        console.log('MongoDB has connected ...')
    })
    .catch(err => {
        console.log('Error MongoDB not connected ...')
    });
