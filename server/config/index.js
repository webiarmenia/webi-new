const appRoot = require('app-root-path');
const dotenv = require('dotenv');

dotenv.config();

module.exports = {
    "config_id": "development",
    "app_name": "webi armenia",
    "app_desc": "my app desc",
    "node_port": process.env.NODE_PORT,
    "json_indentation": 4,
    "jwt_key": process.env.JWT_KEY,
    "mongodb_url": process.env.MONGO_DATABASE_URL,
    "upload_directory": `${appRoot}/_uploads`,
    "backup_directory": `${appRoot}/backup`,
    "cron_backup_directory": `${appRoot}/backup/cron`,
    "winston_config": {
        "file": {
            "level": 'warn',
            "filename": `${appRoot}/logs/app.log`,
            "handleExceptions": true,
            "json": true,
            "maxsize": 5242880, // 5MB
            "maxFiles": 5,
            "colorize": false,
        },
        "console": {
            "level": 'warn',
            "handleExceptions": true,
            "json": false,
            "colorize": true,
        }
    }
};
