require('module-alias/register');

const app = require('./src/app');

const app_name = require('./config').app_name;
const node_port = require('./config').node_port;

app.listen(node_port, () => {
    console.log(`${app_name} listening on port ${node_port}`);
});
